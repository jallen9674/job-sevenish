<?php
/**
 * Template Name: Client Reviews
 * - Note: Markup copied from legacy theme
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );
remove_action('genesis_loop', 'genesis_do_loop', 10);

//Adding Team Member
add_action( 'genesis_loop', 'hennessey_list_reviews', 10 );


function hennessey_list_reviews(){
	?>
<p>At Sevenish Law Firm we take pride in providing our client with the best experience possible. From the initial consultation through the court and legal process until the conclusion of your case, we believe in the importance of open communication and preparation. See below for what some of our previous clients and peers have said about us.</p>
<div id="video-testimonials" class="testimonialblock testimonialtop single-section fullwidth">
    <h2>Video Reviews</h2>
        <div class="testimonialvideo nopadding">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                        )
                );
                ?>
                <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                    <?php
                    if (get_field('testimonial_video')):
                        if (get_field('video_thumbnail')) {
                            $thumb = get_field('video_thumbnail');
                        } else {
                            $thumb = '/wp-content/uploads/2017/11/mqdefault-83.jpg';
                        }
                        ?>
                        <div class="single-video-block-wrap">
                            <div class="single-video-block ">
                                <div class="video-wrap hidden-xs">
                                    <a target="_blank" class="fancybox-youtube single-video" rel="group" href="https://youtu.be/<?php the_field('testimonial_video'); ?>">
                                        <img src="<?php echo $thumb; ?>" alt="<? the_title(); ?>">
                                    </a>
                                </div>
                                <div class="visible-xs mobile-videos">
                                    <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                        <!-- <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php the_field('testimonial_video'); ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe> -->
                                        <!-- <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $thumb; ?>);"></button> -->
                                    </div>
                                </div>
                                <div class="below-video-content"><div class="testimonials-title"><?php
                                        if (get_field('video_title')) {
                                            the_field('video_title');
                                        } else {
                                            the_title();
                                        }
                                        ?></div>
                                    <?php if (get_field('below_video_quote')) { ?>
                                        <div class="testimonial-quote">
                                            <?php the_field('below_video_quote'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (get_field('video_testimonial_client_info')) { ?>
                                        <div class="testimonial-client-info">
                                            - <?php the_field('video_testimonial_client_info'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
    </div>
</div>
<div id="written-testimonials" class="testimonialblock testimonialbottom fullwidth single-section">
        <h3>Written Reviews</h3>
            <div class="row">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                        )
                );
                ?>

                <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                    <?php if (get_field('written_testimonial_quote')) : ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote">
                                    <?php the_field('written_testimonial_quote'); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_field('written_testimonial_client_info'); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    elseif (get_the_content()) :
                        ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote">
                                    <?php the_content(); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_title(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                    ?>

                    <?php
                endwhile;
                wp_reset_query();
                ?>
</div>
<div id="social-testimonials" class="testimonialblock fullwidth single-section">
        <?php if (get_field('social_review_title')) { ?>
                <h3><?php the_field('social_review_title'); ?></h3>
            <?php } ?>
            <?php if (get_field('social_review_subtitle')) { ?>
                <div class="social-review-subtitle"><?php the_field('social_review_subtitle'); ?></div>
            <?php } ?>
            <div class="social-review-list">
                <?php
                if (have_rows('social_reviews')):
                    while (have_rows('social_reviews')) : the_row();
                        ?>
                        <div class="single-social-block">
                            <?php if (get_sub_field('review_content')) { ?>
                                <div class="social-review-quote"><?php the_sub_field('review_content'); ?></div>
                            <?php } ?>
                            <?php if (get_sub_field('social_review_title')) { ?>
                                <div class="social-review-quote"><?php the_sub_field('social_review_title'); ?></div>
                            <?php } ?>
                            <div class="review-profile">
                                <?php
                                if (get_sub_field('profile_picture')) {
                                    $profile = get_sub_field('profile_picture');
                                    ?>
                                    <img class="review-profile-img alignleft" src="<?php echo $profile; ?>" alt="<?php echo $profile['alt']; ?>">
                                <?php } else {
                                    ?>
                                    <img class="review-profile-img alignleft" src="/wp-content/uploads/2018/03/thumb-anonymous.png" alt="sevenish-law">
                                <?php } ?>
                                <?php if (get_sub_field('reviewed_by')) { ?>
                                    <div class="social-reviewed-by">by <?php the_sub_field('reviewed_by'); ?></div>
                                <?php } ?>


                                <?php $star = get_sub_field('review_rating'); ?>
                                <span class="stars">
                                    <?php
                                    if ($star == 1) {
                                        echo"★";
                                    } elseif ($star == 2) {
                                        echo"★★";
                                    } elseif ($star == 3) {
                                        echo"★★★";
                                    } elseif ($star == 4) {
                                        echo"★★★★";
                                    } elseif ($star == 5) {
                                        echo"★★★★★";
                                    } else {
                                        
                                    }
                                    ?>
                                </span>
                                <span class="rating">(<?php echo $star; ?>/5)</span>
                                
                                
                                <?php if (get_sub_field('reviewed_at')) { ?>
                                    <p class="reviewed-at">REVIEWED AT<a href="<?php the_sub_field('reviewed_at_link'); ?>" class="social-reviewed-at">
                                            <?php the_sub_field('reviewed_at'); ?></a></p>
                                <?php } ?>

                                </p>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
</div>
</div>
	
	<?php 
}

// Runs the Genesis loop.
genesis();