<?php
/**
 * Template Name: Our Team
 */

//Filter Post Meta Information
//add_filter( 'genesis_post_info', 'hennessey_post_info' );

function hennessey_post_info( $post_info ) {
	$post_info = 'Posted on [post_date]';
	return $post_info;
}

//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

//Adding Team Member
add_action( 'genesis_after_entry_content', 'hennessey_meet_team' );

// Runs the Genesis loop.
genesis();


function hennessey_meet_team(){
	?>

	<div class="our-team">
      <div class="our-team__inner wrapper">

		<?php //Begin Attorney List ?>   
		    
        <div class="our-team__attorney-list">
			
			<?php //Begin Single Attorney?>
			<a href="<?php echo get_the_permalink(664);?>" class="team-member">
				<div class="team-member__left">
					<div class="team-member__image">
						<img src="<?php echo CHILD_URL; ?>/assets/app/img/team/randall-sevenish-attorney.png" alt="Randall Sevenish, Attorney"/>
					</div>
				</div>
				<div class="team-member__right">
					<span class="team-member__name">
						Randall Sevenish
					</span>
					<span class="team-member__designation">
						Attorney/Founder
					</span>			
					<div class="team-member__content">
						<p>
						My entire adult life has been dedicated to serving others. I have a long history of helping, teaching, educating and protecting others in a variety of arenas, including injury law, law enforcement…
						</p>
					</div>
					<span class="team-member__button">Learn More &raquo;</span>
				</div>
			</a>

						
			<?php //Begin Single Attorney?>
			<a href="<?php echo get_the_permalink(693);?>" class="team-member">
				<div class="team-member__left">
					<div class="team-member__image">
						<img src="<?php echo CHILD_URL; ?>/assets/app/img/team/regina-sevenish-marketing.png" alt="Regina Sevenish, Client Advocate"/>
					</div>
				</div>
				<div class="team-member__right">
					<span class="team-member__name">
						Regina Sevenish
					</span>
					<span class="team-member__designation">
						Client Advocate
					</span>			
					<div class="team-member__content">
						<p>
							Regina Sevenish, wife and best friend of the law firm’s founder, Randy, are partners in so many ways. They have and continue to share business ventures, fun activities in and outside of…
						</p>
					</div>
					<span class="team-member__button">Learn More &raquo;</span>
				</div>
			</a>

						
			<?php //Begin Single Attorney?>
			<a href="<?php echo get_the_permalink(688);?>" class="team-member">
				<div class="team-member__left">
					<div class="team-member__image">
						<img src="<?php echo CHILD_URL; ?>/assets/app/img/team/shannon-majors-cos.png" alt="Shannon Majors, Chief of Staff"/>
					</div>
				</div>
				<div class="team-member__right">
					<span class="team-member__name">
						Shannon Majors
					</span>
					<span class="team-member__designation">
						Chief of Staff
					</span>			
					<div class="team-member__content">
						<p>
							Chief of Staff Shannon Majors is an integral member of the Sevenish Law legal team, responsible for a broad range of important duties. As the firm’s principal paralegal…
						</p>
					</div>
					<span class="team-member__button">Learn More &raquo;</span>
				</div>
			</a>
     
        </div>
        <?php //End Attorney feed ?>

      </div>
    </div> 

	<?php 
}