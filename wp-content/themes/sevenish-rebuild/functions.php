<?php

global $hc_settings;

$hc_settings = [
    'location_taxonomy' => 'hc_related',
    'location_widget_title' => '_hc_related_widget_title',
    'faqs_category_taxonomy' => 'faq-cat',
    'state' => 'Indianapolis',
    'stateabbr' => 'IN',
    'phone_number' => '317-743-7971',
    'primary_menu' => "Primary Menu",
    'practice_areas_menu_item' => "menu-item-00",
];

// generate widget title field
if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array (
        'key' => 'group_5dcda98072a23',
        'title' => 'City Widget Title',
        'fields' => array (
            array (
                'key' => 'field_5dcda98c6d1dd',
                'label' => 'Location Widget Title',
                'name' => $hc_settings['location_widget_title'],
                'type' => 'text',
                'value' => NULL,
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));
endif;


if(is_admin()):
// setup plugins
require_once 'plugin-activator/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 *  <snip />
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function my_theme_register_required_plugins() {

    $plugins = array(

        array(
            'name'      => 'Custom Permalinks',
            'slug'      => 'custom-permalinks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Yoast SEO',
            'slug'      => 'wordpress-seo',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Redirection',
            'slug'      => 'redirection',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Contact Form 7',
            'slug'      => 'contact-form-7',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Custom Fields',
            'slug'      => 'advanced-custom-fields',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Simply Show Hooks',
            'slug'      => 'simply-show-hooks',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'reSmush.it',
            'slug'      => 'resmushit-image-optimizer',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Autoptimize',
            'slug'      => 'autoptimize',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Classic Editor',
            'slug'      => 'classic-editor',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'Accessibility by UserWay',
            'slug'      => 'userway-accessibility-widget',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'CallTrackingMetrics',
            'slug'      => 'call-tracking-metrics',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),

        array(
            'name'      => 'WP External Links',
            'slug'      => 'wp-external-links',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),
        
        array(
            'name'      => 'Contact Form 7 Database Addon – CFDB7',
            'slug'      => 'contact-form-cfdb7',
            'required'  => true,
            'force_activation' => true,
            'force_deactivation' => true
        ),
        
        array(
            'name'      => 'WebP Express',
            'slug'      => 'webp-express',
            'required'  => false,
            'force_activation' => false,
            'force_deactivation' => false
        ),

    );

    $config = array(
        'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        /*
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
            'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
            // <snip>...</snip>
            'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
        */
    );

    tgmpa( $plugins, $config );

}
endif;



/*
 *
 * AUTO INCLUDE - BE CAREFUL!
 *
 * */

$autoiclude_folders = [
    '/lib/snippets/',
    '/lib/taxonomy/',
    '/lib/shortcodes/',
    '/lib/widgets/',
    '/lib/post-types/'
];

foreach($autoiclude_folders as $folder) {
    foreach (scandir(dirname(__FILE__) . $folder) as $filename) {
        $path = dirname(__FILE__) . $folder . $filename;
        if (is_file($path)) {
            require_once $path;
        }
    }
}


// include layout
require_once 'lib/layout/layout.php';


// include frontend assets
require_once 'assets/assets.php';

// allow shortcodes in text widgets
add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

function fn_remove_markeup_tags( $open , $argss ){
    return '';
}

/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0-or-later
 * @link    https://www.studiopress.com/
 */

// Starts the engine.
require_once get_template_directory() . '/lib/init.php';

add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
/**
 * Sets localization (do not remove).
 *
 * @since 1.0.0
 */
function genesis_sample_localization_setup() {

    load_child_theme_textdomain( genesis_get_theme_handle(), get_stylesheet_directory() . '/languages' );

}

// Registers the responsive menus.
if ( function_exists( 'genesis_register_responsive_menus' ) ) {
    genesis_register_responsive_menus( genesis_get_config( 'responsive-menus' ) );
}

add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
/**
 * Enqueues scripts and styles.
 *
 * @since 1.0.0
 */
function genesis_sample_enqueue_scripts_styles() {

    $appearance = genesis_get_config( 'appearance' );

    wp_enqueue_style(
        genesis_get_theme_handle() . '-fonts',
        $appearance['fonts-url'],
        [],
        genesis_get_theme_version()
    );

    wp_enqueue_style( 'dashicons' );

    if ( genesis_is_amp() ) {
        wp_enqueue_style(
            genesis_get_theme_handle() . '-amp',
            get_stylesheet_directory_uri() . '/lib/amp/amp.css',
            [ genesis_get_theme_handle() ],
            genesis_get_theme_version()
        );
    }

}

add_action( 'after_setup_theme', 'genesis_sample_theme_support', 9 );
/**
 * Add desired theme supports.
 *
 * See config file at `config/theme-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_theme_support() {

    $theme_supports = genesis_get_config( 'theme-supports' );

    foreach ( $theme_supports as $feature => $args ) {
        add_theme_support( $feature, $args );
    }

}

add_action( 'after_setup_theme', 'genesis_sample_post_type_support', 9 );
/**
 * Add desired post type supports.
 *
 * See config file at `config/post-type-supports.php`.
 *
 * @since 3.0.0
 */
function genesis_sample_post_type_support() {

    $post_type_supports = genesis_get_config( 'post-type-supports' );

    foreach ( $post_type_supports as $post_type => $args ) {
        add_post_type_support( $post_type, $args );
    }

}

// Adds image sizes.
add_image_size( 'sidebar-featured', 75, 75, true );
add_image_size( 'genesis-singular-images', 702, 526, true );

// Removes header right widget area.
unregister_sidebar( 'header-right' );

// Removes secondary sidebar.
unregister_sidebar( 'sidebar-alt' );

// Removes site layouts.
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

// Repositions primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
//add_action( 'genesis_header', 'genesis_do_nav', 12 );

// Repositions the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 10 );

add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
/**
 * Reduces secondary navigation menu to one level depth.
 *
 * @since 2.2.3
 *
 * @param array $args Original menu options.
 * @return array Menu options with depth set to 1.
 */
function genesis_sample_secondary_menu_args( $args ) {

    if ( 'secondary' === $args['theme_location'] ) {
        $args['depth'] = 1;
    }

    return $args;

}

add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
/**
 * Modifies size of the Gravatar in the author box.
 *
 * @since 2.2.3
 *
 * @param int $size Original icon size.
 * @return int Modified icon size.
 */
function genesis_sample_author_box_gravatar( $size ) {

    return 90;

}

add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
/**
 * Modifies size of the Gravatar in the entry comments.
 *
 * @since 2.2.3
 *
 * @param array $args Gravatar settings.
 * @return array Gravatar settings with modified size.
 */
function genesis_sample_comments_gravatar( $args ) {

    $args['avatar_size'] = 60;
    return $args;

}


/*-------------------------------------
Hennessey Consulting Specific Functions
--------------------------------------*/

/*---------------------------------
Cleaning Up WP Head
----------------------------------*/
function removeHeadLinks() {
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
  }
  add_action('init', 'removeHeadLinks');
  remove_action('wp_head', 'wp_generator');
  
if (  !is_user_logged_in() ){
  wp_deregister_style( 'dashicons' ); 
}


/*--------------------------------------------
Favicons
---------------------------------------------*/

//Remove Genesis Favicon
remove_action('wp_head', 'genesis_load_favicon');

//Add Hennessey Favicon
function hennessey_add_favicon() {
    ?>
      <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
      <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/site.webmanifest">
      <meta name="msapplication-TileColor" content="#2b5797">
      <meta name="theme-color" content="#ffffff">
    <?php
  }
  
  add_action('wp_head', 'hennessey_add_favicon');
  


/*---------------------------------
Using Template Parts the Genesis Way
----------------------------------*/

//Remove Genesis Header Content (Logo and Navigation)
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_do_nav' );


//Remove Genesis Footer
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );


//Removing Interior Page Heading
//remove_action( 'genesis_entry_header', 'genesis_do_post_title' );


//JSON-LD Module
//require_once('modules/jsonld/init.php');


//Allow Sidebar Shortcodes
add_filter( 'widget_text', 'do_shortcode' );


/*---------------------------------
Image Sizes
----------------------------------*/

//Image Sizes
add_image_size( 'faq-thumb', 300, 200, true );
add_image_size( 'faq-thumb-sidebar', 400, 250, true );
add_image_size( 'header-banner', 1400, 300, true );


/*---------------------------------
Add Read More Link to Post Excerpts
----------------------------------*/

add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
   return '... <a href="' . get_permalink() . '">Read More</a>';
}



/*--------------------------------------------
Adding Userway
---------------------------------------------*/

add_action('wp_footer', 'hennessey_add_userway');

function hennessey_add_userway(){
  ?>
  <script type="text/javascript">
  var _userway_config = {
   account: 'PgJBLRHmZy'
  };
  </script>
  <script type="text/javascript" src="https://cdn.userway.org/widget.js"></script>
  <?php 
}


/*--------------------------------------------
Adding Google Analytics (& Tag manager)
---------------------------------------------*/

add_action('wp_head', 'hennessey_add_analytics_head');

function hennessey_add_analytics_head(){
  ?>
  
  <meta name="google-site-verification" content="o4qRVEOXcBLUP8cRRpVfdkygMThMGXJmko6vmgcVu6s" />

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-71852357-26"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-71852357-26', {'send_page_view': false, 'optimize_id': 'GTM-NJFW26V', 'linker': {'domains': ['sevenishlaw.com'] } });
  </script>
    

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-P8LJQZT');</script>
  <!-- End Google Tag Manager -->


  <?php 
}

add_action('genesis_before_header', 'hennessey_add_analytics_body', 0);

function hennessey_add_analytics_body() {
  ?>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8LJQZT"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <?php 
}


/*--------------------------------------------
Adding Facebook Pixel 
---------------------------------------------*/

//add_action('wp_head', 'hennessey_add_fb');

function hennessey_add_fb(){
  ?>
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '850926998401561');
    fbq('track', 'PageView');
  </script>
  <noscript><img height="1" width="1" alt="fb track" style="display:none"
    src="https://www.facebook.com/tr?id=850926998401561&ev=PageView&noscript=1"
  /></noscript>

  <?php
}



/*--------------------------------------------
Adding Call Rail Snippet
-- Currently using CTM via plugin
---------------------------------------------*/

//add_action('wp_footer', 'hennessey_add_callrail');

function hennessey_add_callrail() {
  ?>
  <script type="text/javascript" src="//cdn.callrail.com/companies/656659615/90e1d4a90c69f29ee4c5/12/swap.js"></script> 
  <?php
}


/*--------------------------------------------
Adding ngage Chat
---------------------------------------------*/

add_action('wp_footer', 'hennessey_add_ngage');

function hennessey_add_ngage() {  
  //Do Not Show During Local Development
  if ( (!($_SERVER['SERVER_NAME']  == 'sevenish.local')) && (!($_SERVER['SERVER_NAME']  == 'sevenishdev.wpengine.com')) ) {
  ?>
    <div id="nGageLH" style="visibility:hidden; display: block; padding: 0; position: fixed; right: 0px; bottom: 0px; z-index: 5000;"></div>
    <script src="https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=173-8-154-196-139-3-64-63" async="async">
  <?php 
  }
}




/*---------------------------------
Add Tag to Meta Description & Category
----------------------------------*/

add_filter( 'wpseo_metadesc', 'hennessey_description_tag', 100, 1 );

function hennessey_description_tag($s){
  if( is_tag() ){
     $s .= single_tag_title(" Posts Tagged as: ", false);
  }
  if( is_category() ){
     $s .= single_cat_title(" Posts Categorized as: ", false);
  }
  return $s;
 }
 
 
 /*------------------------------------------
 Add page number to meta description
 -------------------------------------------*/

 function hc_add_page_number( $s ) {
    global $page;
    $paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
    ! empty ( $page ) && 1 < $page && $paged = $page;

    $paged > 1 && $s .= ' | ' . sprintf( __( 'Page: %s' ), $paged );

    return $s;
}

add_filter( 'wpseo_title', 'hc_add_page_number', 100, 1 );
add_filter( 'wpseo_metadesc', 'hc_add_page_number', 100, 1 );


 /*------------------------------------------
 Bringing in Legacy Post Types and Taxonomies
 -------------------------------------------*/

 add_action('init', 'sevenish_custom_post');

function sevenish_custom_post() {
  $args = ['public' => true,
      'label' => 'Testimonials',
      'rewrite' => array('with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('client-review', $args);
  $args = ['public' => true,
      'label' => 'Videos',
      'has_archive' => true,
      'rewrite' => array('slug'=>'video','with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('video', $args);
  $args = ['public' => true,
      'label' => 'FAQs',
      'has_archive' => true,
      'rewrite' => array('with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('faq', $args);
  $args = ['public' => true,
      'label' => 'Library',
      'rewrite' => array('with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('library', $args);
  $args = ['public' => true,
      'label' => 'Free Books',
      'rewrite' => array('with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('report', $args);
  $args = ['public' => true,
      'label' => 'Case Results',
      'rewrite' => array('with_front' => false),
      'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
  ];
  register_post_type('case-result', $args);
}

add_action('init', 'create_case_tax');

function create_case_tax() {
  register_taxonomy(
          'video-cat', 'video', array(
      'label' => __('Videos Category'),
      'rewrite' => array('slug' => 'videos/category', 'with_front' => false),
      'hierarchical' => true,
          )
  );
  register_taxonomy(
          'faq-cat', 'faq', array(
      'label' => __('FAQs Category'),
      'rewrite' => array('slug' => 'faqs/category', 'with_front' => false),
      'hierarchical' => true,
          )
  );
  register_taxonomy(
          'library-cat', 'library', array(
      'label' => __('Library Category'),
      'rewrite' => array('slug' => 'library-cat', 'with_front' => 'false'),
      'hierarchical' => true,
          )
  );
  register_taxonomy(
          'case-cat', 'case-result', array(
      'label' => __('Case Results Category'),
      'rewrite' => array('slug' => 'case-results-category', 'with_front' => false),
      'hierarchical' => true,
          )
  );
}


//Add Hennessey Favicon
function hennessey_add_sleeknote() {
    ?>
      <!-- Start of Sleeknote signup and lead generation tool - www.sleeknote.com -->
<script id="sleeknoteScript" type="text/javascript">
(function (){
  var sleeknoteScriptTag=document.createElement("script");
  sleeknoteScriptTag.type="text/javascript";
  sleeknoteScriptTag.charset="utf-8";
  sleeknoteScriptTag.src=("//sleeknotecustomerscripts.sleeknote.com/21072.js");
  var s=document.getElementById("sleeknoteScript");
  s.parentNode.insertBefore(sleeknoteScriptTag, s);
})(); </script>
<!-- End of Sleeknote signup and lead generation tool - www.sleeknote.com -->
<?php
  }
  
  add_action('wp_head', 'hennessey_add_sleeknote');