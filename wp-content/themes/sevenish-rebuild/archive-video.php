<?php
/**
 * Template Name: Video Listing
 */

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Adding Body Class
add_filter( 'body_class', 'genesis_sample_landing_body_class' );
function genesis_sample_landing_body_class( $classes ) {
	$classes[] = 'faq-listing-page fullwidth-template';
	return $classes;
}

//Remove Entry Content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Remove Archive Loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//Adding FAQ Listing
add_action( 'genesis_loop',  'hennessey_video_listing');


/*------------------------------
FAQ Listing
--------------------------------*/

function hennessey_video_listing(){
	?>

			
	<?php		

		$taxes [
			97 => "Frequently Asked Question",
			98 => "Client Testimonials",
			0 => "Other Videos"
		];
	
		foreach($taxes as $tax_id => $tax_name) {
			
		?><h2><?=$tax_name?></h2><div class="video-listing"><?php
	
		$args = array(
			'posts_per_page' => -1,
			'post_type' => 'video',
		);
			
			if($tax_id) {
				$args['video-cat'] = $tax_id;
			}

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

		//Getting Post Category for Display
		global $post;
		$postTerms =  wp_get_object_terms($post->ID, 'video-cat');
		$categoryFilterSlug = '';
		$categoryPrettyName = '';
		if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
			 foreach ( $postTerms as $term ) {
			   $categoryFilterSlug .= ' ' . $term->slug;
			   $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
			 }
		 }

		?>

		<a href="<?php the_permalink(); ?>" class="video-listing single-video-listing">
			<div class="single-video-listing__inner">
				<?php 
					if ( has_post_thumbnail() ){
						the_post_thumbnail('faq-thumb', ['alt'=>get_the_title(), 'class'=>'single-video-listing__image']);
					}
				?>
				<span class="single-video-listing__meta">
					<?php echo $categoryPrettyName; ?>
				</span>
				<span class="single-video-listing__title"><?php the_title(); ?></span>

			</div>
		</a>
		
		<?php endwhile; else : ?>
		<!-- IF NOTHING FOUND CONTENT HERE -->
		<?php endif; ?>

		<?php wp_reset_query(); ?>
        

	<?php 
		} ?>
		   	</div>
		<?php
}

// Runs the Genesis loop.
genesis();