<?php

function custom_footer(){
    global $hc_settings;
    remove_action('genesis_footer', 'genesis_do_footer', 10); ?>

    <div class="footer-review-section">
        <div class="footer-review-section__inner wrapper">

            <h2 class="footer-review-section__title">Client & Professional Reviews</h2>

            <div class="owl-carousel owl-theme footer-review-section__carousel">
                <?php
                $i = 1;
                $parent = new WP_Query(array(
                    'post_type' => 'client-review',
                    'showposts' => 5,
                
                    'meta_query' => array(
                        array(
                            'key' => 'testimonial_video',
                            'value'   => array(''),
                            'compare' => 'NOT IN'
                        )
                    )
                    
                ));
                $count = $parent->post_count;
                if ($parent->have_posts()) :
                    ?>
                    <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                        <div class="item active">
                            
                            <div class="footer-review">
                                <?php /* -- Disabling YT Videos In Footer for Performance Reasons
                                <div class="footer-review__video">
                                    <?php if(get_field('testimonial_video')){ ?>
                                        <iframe width="400" height="250" src="https://www.youtube.com/embed/<?php echo get_field('testimonial_video'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <?php } ?>
                                </div>      
                                */ ?>
                                <span class="footer-review__author">
                                        <?php the_title(); ?>
                                </span>                                                                     
                                <div class="footer-review__content">
                                    <?php echo wp_trim_words(the_field('below_video_quote'), 30); ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div> 
                    <?php
                    unset($parent);
                endif;
                wp_reset_query();
                ?>
            </div> <?php //End __Carousel ?>

            <div class="footer-review-section__disclaimer">
            These reviews do not constitute a guarantee, warranty, or prediction regarding the outcome of your legal matter.
            </div>

        </div>
    </div>

    <footer class="site-footer">
      
        <div class="footer-content">
            <div class="footer-content__inner wrapper">

                <div class="footer-content__logo">

<!--                     <img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/logo-white.png" alt="Sevenish Law Firm"> -->
                    <img src="https://www.sevenishlaw.com/wp-content/uploads/2020/01/logo.png" alt="Sevenish Law Firm">
                    <div class="footer-content__social">
                        <ul>
                            <?php /* Need Justia and Avvo Logos
                            <li>
                                <a href="#" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-avvo"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-justia"></i>
                                </a>
                            </li> */ ?>
                            <li>
                                <a href="https://www.facebook.com/SevenishLaw/" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/sevenishlaw" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/company/sevenish-law" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/user/SevenishLaw/videos" target="_blank" rel="noopener noreferrer">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="footer-content__links">
                    <h3 class="footer-content__heading">
                        Important Links
                    </h3>
                    
                    <ul class="footer-content__link-list">
                        <li><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li><a href="<?php echo site_url(); ?>/about-us/">About Us</a></li>
                        <li><a href="<?php echo site_url(); ?>/case-results/">Results</a></li>
                        <li><a href="<?php echo site_url(); ?>/blog/">Blog</a></li>
                        <li><a href="<?php echo site_url(); ?>/contact-us/">Contact Us</a></li>
                    </ul>

                </div>

                <div class="footer-content__contact">
                    <h3 class="footer-content__heading">
                        Contact Us
                    </h3>
                    <div class="footer-content__contact-info">

                        <span class="footer-content__contact-info-header">
                            <strong>Indianapolis Office</strong>
                        </span>
                    
                        <div class="footer-contact-info">
                            <i class="fa fa-map"></i>
                            <p>
                                101 W Ohio St<br>
                                Suite 1540<br>
                                Indianapolis IN 46204<br>
                                <a target="_blank" href="https://www.google.com/maps/dir//Sevenish+Law+Firm,+101+W+Ohio+St+%231540,+Indianapolis,+IN+46204/@39.7696979,-86.1602258,15z/data=!4m9!4m8!1m0!1m5!1m1!1s0x886b5095c580a4b9:0x1dd4b8f6c60e0b04!2m2!1d-86.1602258!2d39.7696979!3e0"><strong>Get Directions <span>&raquo;</span></strong></a>
                            </p>
                        </div>

                        <div class="footer-contact-info">
                            <i class="fa fa-phone"></i>
                            <p>
                                <a href="tel:<?php echo $hc_settings['phone-number']; ?>"><?php echo $hc_settings['phone-number']; ?></a>
                            </p>
                        </div>
                    
                        <div class="footer-contact-info">
                            <i class="fa fa-fax"></i>
                            <p>
                                317-636-7721
                            </p>
                        </div>
                    </div>

                </div>
            
            </div>
        </div>
        
        <div class="footer-bottom">
            <div class="footer-bottom__inner wrapper">
            
                <div class="footer-bottom__navigation">
                    <ul>
                        <li><a href="<?php echo site_url(); ?>/sitemap/">Sitemap</a></li>&nbsp;&nbsp;|&nbsp;&nbsp;<li><a href="<?php echo site_url(); ?>/disclaimer/">Disclaimer</a></li>&nbsp;&nbsp;|&nbsp;&nbsp;<li><a href="<?php echo site_url(); ?>/privacy-policy/">Privacy Policy</a></li>
                    </ul>
                </div>

                <div class="footer-bottom__copyright">
                    Copyright &copy Sevenish Law Firm <?php echo date('Y'); ?>. All Rights Reserved.
                </div>

            </div>
        </div> <?php //End .footer-bottom ?>

    </footer>

<?php }

add_action('genesis_footer', 'custom_footer', 8);
