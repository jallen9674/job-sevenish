<?php 

//remove genesis header
add_action('after_setup_theme', 'remove_genesis_header');
function remove_genesis_header(){
  remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );
  remove_action( 'genesis_header', 'genesis_do_header' );
  remove_action( 'genesis_header', 'genesis_header_markup_close', 15 );
}

add_action('genesis_header', 'hennessey_global_header', 11);
function hennessey_global_header() {
	global $hc_settings; ?>

	<div class="mobile-navigation-pane" id="mobile-menu">

        <div class="mobile-navigation-pane__inner">
        
            <div class="mobile-navigation-pane__header">
                <span>Navigation</span>
                <i class="fa fa-close navigation-pane-close"></i>
            </div>

            <div class="mobile-navigation-pane__navigation">

                <nav role="navigation">
                    <?php 
                        wp_nav_menu(
                            array(
                                'container' => false,                           
                                'container_class' => 'menu cf',                 
                                'menu' => 'Primary Navigation',  
                                'menu_class' => 'mobile-nav',               
                                'theme_location' => 'main-nav',                 
                                'before' => '',                                 
                                'after' => '',                                  
                                'link_before' => '',                            
                                'link_after' => '',                             
                                'depth' => 0,                                   
                                'fallback_cb' => ''                             
                            )
                        ); 
                    ?>               
                </nav>

            </div>
        
        </div>

    </div>

    <div class="site-header__inner mobile-header">

        <div class="notification-bar">
            <div class="notification-bar__inner">
                <i class="fa fa-phone"></i> &nbsp;&nbsp;Call Us Now!
            </div>
        </div>
                    
        <div class="mobile-header__logo">
            <a href="<?php echo site_url(); ?>">
            <img src="<?= CHILD_URL; ?>/assets/app/img/new-logo.jpg" alt="Sevenish Law Logo" class="mobile-header__logo--default">
            </a>
        </div>

        <div class="mobile-header__phone">
            <a href="tel:<?php echo $hc_settings['phone_number']; ?>"><?php echo $hc_settings['phone_number']; ?></a>
        </div>

        <div class="mobile-header__navigation-toggle navigation-pane-toggle">
            
            <span>Menu</span>
            <span><i class="fa fa-bars"></i></span>

        </div>

    </div>

	<header class="site-header">
		<div class="wrap">
			<div class="site-header__inner desktop-header">
		        <div class="desktop-header__logo">
		            <a href="<?php echo site_url(); ?>">
						<img src="<?= CHILD_URL; ?>/assets/app/img/new-logo.jpg" alt="Logo" class="desktop-header__logo--default"/>
		            </a>
		        </div>
		        <div class="desktop-header__right">
		            <div class="desktop-header__avvo">
						<a href="https://www.thelawyersofdistinction.com/profile/randall-sevenish/" class="header_img"><img class="alignnone wp-image-10762 size-medium" src="https://www.lawyersofdistinction.com/wp-content/uploads/2019/11/800lod-1.png" alt="" width="243" height="300" /></a>
		                <img src="<?php echo CHILD_URL; ?>/assets/app/img/avvo-top-rated-icon.png" alt="Avvo Top Rated" />
						<!--<a target="_blank" href="https://www.respectedlawyers.com/in/car-accident-lawyer-in-indianapolis/#profile-id-585"><img src="/wp-content/uploads/2020/02/respectedlawyersbadge.png"/></a>-->
		            </div>

		            <div class="desktop-header__cta header-cta">
		                <span class="header-cta__heading">
		                    Call Today for a FREE Consultation
		                </span>
		                <a class="header-cta__phone" href="tel:<?php echo $hc_settings['phone_number']; ?>">
		                    <?php echo $hc_settings['phone_number']; ?>
		                </a>
		                <span class="header-cta__subheading">
		                    24 Hours | 7 Days a Week
		                </span>
		            </div>
		        </div>      
		   </div>

		   <div class="desktop-header-navigation">
		        <div class="desktop-header-navigation__inner wrapper">
		            <nav role="navigation">
		                <?php 
		                    wp_nav_menu(
		                        array(
		                            'container' => false,                           
		                            'container_class' => 'menu cf',                 
		                            'menu' => 'Primary Navigation',  
		                            'menu_class' => 'desktop-nav',               
		                            'theme_location' => 'main-nav',                 
		                            'before' => '',                                 
		                            'after' => '',                                  
		                            'link_before' => '',                            
		                            'link_after' => '',                             
		                            'depth' => 0,                                   
		                            'fallback_cb' => ''                             
		                        )
		                    ); 
		                ?>               
		            </nav>
		        </div>
		    </div>
		</div>
	</header>
   
    <?php 
}