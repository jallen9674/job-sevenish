<?php

/*-------------------------------------
Display a list of random FAQs text only
[hc-sidebar-get-help]
--------------------------------------*/

function hcSidebarGetHelp($atts = null) {
    global $hc_settings;
    ob_start();
    //BEGIN OUTPUT
?>

<div class="get-help">
    <div class="get-help__top">
    </div>
    <div class="get-help__bottom">
        <span class="get-help__heading">
            Call Now For Help
        </span>
        <div class="get-help__contact">
            <a href="tel:<?php echo $hc_settings['phone_number']; ?>">
                <strong><?php echo $hc_settings['phone_number']; ?></strong>
            </a>
            <span>24 Hours / 7 Days a Week</span>
        </div>

    </div>
</div>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hc-sidebar-get-help', 'hcSidebarGetHelp');

