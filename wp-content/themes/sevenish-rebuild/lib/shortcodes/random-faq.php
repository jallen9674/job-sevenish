<?php

/*-------------------------------------
Display a list of random FAQs
[hc-random-faq-image amount="#"]
--------------------------------------*/

function hcRandomFaqImage($atts = null) {

    global $post;

    extract(shortcode_atts(array(
      'amount' => '',
   ), $atts));

    $queryAmount = $amount;

    ob_start();
    //BEGIN OUTPUT
?>
<div class="random-faq-loop  faq-filter-portfolio-wrapper">
        <?php $faqTerms = get_terms( 'faq-cat' );
        // convert array of term objects to array of term IDs
        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

        $args = array(
          'posts_per_page' => $queryAmount,
          'post_type' => 'faq',
          'order' => 'DSC',
          'orderby' => 'rand',
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
        <?php //Getting Category for Filtering
            $postTerms =  wp_get_object_terms($post->ID, 'faq-cat');
            $categoryFilterSlug = '';
            $categoryPrettyName = '';
            if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                 foreach ( $postTerms as $term ) {
                   $categoryFilterSlug .= ' ' . $term->slug;
                   $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                 }
             }
         ?>

        <div class="faq-page-listing">

            <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
            <span class="faq-image-text-overlay">Frequently Asked Question</span>
            <?php
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail('faq-thumb', ['alt' => get_the_title()]);
                } else{
                    echo '<img alt="faq thumb" src="' . CHILD_URL . '/assets/app/img/default-faq-thumb.jpg" />';
                }
            ?>
            </a>
           <div class="faq-meta">
                <span class="faq-category"><?php echo $categoryPrettyName; ?></span>
            </div>
            <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

        </div>
          <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>

         </div>

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hc-random-faq-image', 'hcRandomFaqImage');


