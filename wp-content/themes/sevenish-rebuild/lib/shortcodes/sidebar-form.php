<?php 

/*-------------------------------------
The Contact Form Displayed in Sidebar
[hennessey-sidebar-form]
--------------------------------------*/


function hennesseySidebarForm($atts = null) {

   
    $contact_form = do_shortcode( '[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form sidebar-contact-form"]' );

    ob_start();
    //BEGIN OUTPUT

    ?>

       <div class="sidebar-form__form">
          
          <span class="sidebar-form__form-title">
            Free Case Evaluation
          </span>
          
          <span class="sidebar-form__form-subtitle">
            Quick. Easy. Confidential.
          </span>

          <?php echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form"]'); ?>
        </div>
        

    <?php 
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hennessey-sidebar-form', 'hennesseySidebarForm');