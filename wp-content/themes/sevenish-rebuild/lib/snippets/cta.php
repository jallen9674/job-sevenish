<?php
// adding CTA's
add_filter('the_content', function($content){
    global $post;
    global $hc_settings;

    $terms = get_the_terms( $post->ID, $hc_settings['location_taxonomy'] ); // get page location term (city)

    $phoneNumber = $hc_settings['phone_number']; // get phone number

    $cta = '';

    //if no city, its not single blog post and its not FAQ page - then return content without CTA's

    if(!$terms && !is_singular('post') && !has_term('', $hc_settings['faqs_category_taxonomy'])) {
        return $content;
    } else if($terms) {

        $terms = current($terms); //get single term from $terms array

        /** get page name without lawyer word - ****THIS IS ONLY ON LOCATION PAGES****
         * "Chicago Car Accident Lawyer" will be "Chicago Car Accident"
         */
        $page_h1 = str_ireplace('lawyer', '', $terms->name . " " . get_the_title($post->ID));

        // wrapping it in CTA with phone number
        $cta = '<p class="rs-content-cta">'. $page_h1 .' Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';

        // getting location widget title - e.q. "Car Accidents"
        $short = get_post_meta($post->ID, $hc_settings['location_widget_title'], true);

        if($short) {
            //if exists - making it singular and combining with location
            $short = str_replace(['Accidents','Bites'], ['Accident', 'Bite'], $short);
            $cta = '<p class="rs-content-cta">'.$terms->name . ' ' . $short.' Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
        }

    }

    // default first CTA link for non-location pages - like blog, faq
    $cta_third_add = '<a href="/">personal injury lawyers</a>';
    $cta_first_add = ',';

    if($terms && $post->post_parent !== 0) {
        // FOR LOCATION PAGES: get parent page title and link - e.q. "Personal Injury"
        $link = get_permalink($post->post_parent);
        // make lawyer plural
        $title = str_replace('Lawyer', 'Lawyers', get_the_title($post->post_parent));

        // this is for metropolis-specific pages e.q. "Fort Lauderdale, FL"
        if($title == 'Locations') {
            $link = '/';
            $title = $hc_settings['state'].' Personal Injury Lawyers';
        }

        //modify CTA on location pages with parent page correctly set
        $cta_third_add = '<a href="'.$link.'" title="'.$title.'">'.$title.'</a>';
        $cta_first_add = " with a ".strtolower(get_post_meta($post->ID, $hc_settings['location_widget_title'], true))." lawyer serving {$terms->name},";
    } else if($terms && $post->post_parent == 0) {
        $cta_third_add = '<a href="/">personal injury lawyers</a>';
        $cta_first_add = " with a personal injury lawyer serving {$terms->name},";
    }


    /**
     *
     * CTA SECOND ($cta var) EXAMPLES
     * Personal Injury Lawyer NEAR ME 555-555-5555
     * Dallas Car Accident Lawyer NEAR ME 555-555-5555
     * */


    /**
    *
     * CTA FIRST EXAMPLES
     * For a free legal consultation, call 555-555-5555
     * For a free legal consultation with a personal injury lawyer serving City, call 555-555-5555
     * For a free legal consultation with a car accident lawyer serving City, call 555-555-5555
    * */
    $cta_first = '<p class="rs-content-cta rs-content-cta-small">For a free legal consultation'.$cta_first_add.' call <a href="tel:'.$phoneNumber.'">'.$phoneNumber.' </a></p>';
    /**
     *
     * CTA THIRD EXAMPLES
     * Click to contact our personal injury lawyers today
     * Click to contact our car accident lawyers today
     * */
    $cta_third = '<p class="rs-content-cta rs-content-cta-small"><a href="/contact/">Click to contact</a> our '.$cta_third_add.' today</p>';

    /**
     * This is just static:
     * Complete a Free Case Evaluation form now
     */
    $cta_fourth = '<p class="rs-content-cta rs-content-cta-small">Complete a <a href="/contact/">Free Case Evaluation form</a> now</p>';


    // matching all h2 in post_content, getting its offset
    preg_match_all('/<h2>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);

    // if no h2 in content - matching other headers
    if(count($h2_matches[0]) <= 2) {
        preg_match_all('/<h(2|3|4)>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);
    }
    // if no other headers in article - matching <p> tags with 300+ symbols between <p> and </p>
    if(count($h2_matches[0]) <= 1) {
        preg_match_all('/<p>.{300,}<\/p>/', $content, $h2_matches, PREG_OFFSET_CAPTURE);

        // if more than two <p> matched - exclude first match
        if(count($h2_matches[0]) > 2) {
            array_shift($h2_matches[0]);
        }
    }

    // this is for metropolis pages e.q. Fort Lauderdale, FL
    if($terms && !get_post_meta($post->ID, $hc_settings['location_widget_title'], true)) {
        $cta = '<p class="rs-content-cta">Personal Injury Lawyer Near Me <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a></p>';
    }

    // including CTA's just before matched headers or <p>'s
    if(count($h2_matches[0]) > 1) {
        foreach(array_reverse($h2_matches[0]) as $k => $h2_match) {

            $k = count($h2_matches[0]) - $k - 1;

            if($h2_match[1] && $k == 1) {
                $content = substr_replace($content, $cta_first, $h2_match[1], 0);
            }

            if($h2_match[1] && $k == 2 && $cta) {
                $content = substr_replace($content, $cta, $h2_match[1], 0);
            }

            if($h2_match[1] && $k == 3) {
                $content = substr_replace($content, $cta_third, $h2_match[1], 0);
            }

            if($h2_match[1] && $k == 4) {
                $content = substr_replace($content, $cta_fourth, $h2_match[1], 0);
            }
        }
    }

    // last CTA - just static
    $last_cta = '<p class="rs-content-cta rs-content-cta-small">Call or text <a href="tel:'.$phoneNumber.'">'.$phoneNumber.'</a> or complete a <a href="/contact/">Free Case Evaluation form</a></p>';

    $content .= $last_cta;

    return $content;
});