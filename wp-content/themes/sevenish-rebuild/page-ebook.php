<?php
/**
 * Template Name: Ebook List
 * - Note: Markup copied from legacy theme
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

//Adding Team Member
add_action( 'genesis_after_entry_content', 'hennessey_list_ebooks' );

// Runs the Genesis loop.
genesis();


function hennessey_list_ebooks(){
    ?>
    
    <div class="ebook-listings fullwidth">
        <?php
        $i = 1;
        $parent_nav = new WP_Query(array(
            'order' => 'Desc',
            'posts_per_page' => '-1',
            'post_status' => 'publish',
            'post_type' => 'report',
            'paged' => get_query_var('paged')
        ));
            $post = get_post( $post ); 
        ?>
        <?php if ($parent_nav->have_posts()) : while ($parent_nav->have_posts()) : $parent_nav->the_post();     
        global $post;
        $post = get_post( $post );   
        $backgroundimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
        ?>
                <div class="ebook-listing">
                    <a href="<?php the_permalink(); ?>" class="<?php echo $post->post_type;?>-listing ebook-featured bg-cover">
                    <?php
                        if (has_post_thumbnail()) {
                            the_post_thumbnail('medium', ['alt'=>get_the_title()]);
                        }
                    ?>
                    </a>
                    <div class="blog-content half">
                        <h3> <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                        <p> 
                            <?php
                            echo wp_trim_words(get_the_excerpt(), 20, '...');
                            ?>
                        </p>
                        <a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More</a>
                    </div>
                    <hr/>
                </div>
                <?php
                $i++;
            endwhile;
            
            wp_reset_query();
        endif;
        ?>
    </div>
	

	<?php 
}