<?php
/**
 * Single Post Template
 */

//Filter Post Meta Information
//add_filter( 'genesis_post_info', 'hennessey_post_info' );

//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );


remove_action( 'genesis_entry_header', 'genesis_do_post_title',10 );
// Runs the Genesis loop.
genesis();
