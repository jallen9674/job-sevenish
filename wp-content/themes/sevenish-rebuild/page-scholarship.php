<?php
/**
 * Template Name: Scholarship Application
 */

$_markeup_tags = array( 'content' );

foreach ($_markeup_tags as $markup_key => $markup_value) {

    add_filter( 'genesis_markup_'. $markup_value .'_open' , 'fn_remove_markeup_tags' , 10 , 2);
    add_filter( 'genesis_markup_'. $markup_value .'_close' , 'fn_remove_markeup_tags' , 10 , 2);

}

add_filter( 'body_class', 'hennessey_add_body_class' );
function hennessey_add_body_class( $classes ) {
	$classes[] = 'scholarship-application fullwidth-template';
	return $classes;
}

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Adding Results Listing
add_action( 'genesis_before_content',  'hennessey_scholarship_layout');

remove_action('genesis_after_content', 'genesis_get_sidebar', 10);
remove_action('genesis_entry_header', 'genesis_do_post_format_image', 4);
remove_action('genesis_entry_header', 'genesis_entry_header_markup_open', 5);
remove_action('genesis_entry_header', 'genesis_do_post_title', 10);
remove_action('genesis_entry_header', 'genesis_post_info', 12);
remove_action('genesis_entry_header', 'genesis_entry_header_markup_close', 15);

remove_action('genesis_entry_content', 'custom_do_thumbnail', 8);
remove_action('genesis_entry_content', 'genesis_do_post_image', 8);
remove_action('genesis_entry_content', 'genesis_do_singular_image', 8);

// Runs the Genesis loop.
genesis();



//FAQ Loop

function hennessey_scholarship_layout(){
	?>
	
  <div class="scholarship-content">

      <main class="scholarship-content__left content">
        <p>The Sevenish Law Firm provides an annual scholarship of <strong>$2,500</strong> to a student committed to fighting the plight of drunk driving in their community. Students have until <strong>June 15, 2020</strong> to apply using the form on this page. Applicants must include an essay on the topic indicated below.</p>
        <h2>About the Sevenish Students Against Drinking and Driving Scholarship</h2>
        <p>Attorney Randall Sevenish has spent his entire life serving and protecting others. He has seen the carnage that driving while intoxicated has caused people and their families, first as a police officer and for more than three decades as an injury lawyer.</p>
        <p>“It breaks my heart to see people ruin their lives and their futures by drinking and driving,” said Sevenish. “I’m committed to making a difference, no matter how small, to help end the scourge of driving while intoxicated.”</p>
        <p>The Sevenish Law Firm created this scholarship to enable a promising young student to acquire the knowledge and experience that equips them to help others.</p>
        <h2>Eligibility for the Sevenish Law Firm’s Academic Scholarship</h2>
        <p>Applicants for the Sevenish Students Against Drinking and Driving Scholarship must meet these criteria:</p>
        <ul style="margin-bottom: 30px;">
          <li>3.0 GPA (attach your unofficial transcript to the application form)</li>
          <li>Attending a college or university, or admitted to a college or university</li>
          <li>Submission of a 500 to 1,000-word essay (see topic below)</li>
          <li>Agree to the <a href="https://sevenishlaw.local/students-against-drinking-and-driving-scholarship/terms-and-conditions/">Terms and Conditions</a></li>
        </ul>
        <h2>Essay Question</h2>
        <p>Please answer this essay question in 500 to 1,000 words and attach it as a PDF as part of your application.</p>
        <p><em>Why do you think people continue to drink and drive despite the known dangers, and what can a young adult such as yourself do to persuade their peers not to drink and drive?</em></p>
        <h2>Scholarship Application Instructions</h2>
        <p>The Sevenish Law Firm will only consider applications submitted using the form on this page. Applicants must fill out each required field and must attach their unofficial transcript as well as their essay in PDF format. Applicants must also type their full name at the bottom of the application to serve as their electronic signature. The deadline to apply is <strong>June 15, 2020</strong>.</p>
      </main>

      <aside class="scholarship-content__right">
        <?php echo do_shortcode('[contact-form-7 id="1710" title="Scholarship Application Form" html_class="scholarship-application-form"]'); ?>
      </aside>

  </div>

	<?php 
}
