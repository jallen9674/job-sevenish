<?php
/**
 * Blog Category Template
 */


 //Adjust Body Class
add_filter( 'body_class', 'hennessey_add_body_class' );
function hennessey_add_body_class( $classes ) {
	$classes[] = 'blog-category';
	return $classes;
}

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Add back post titles for this page only
add_action( 'genesis_entry_header', 'genesis_do_post_title' );

//Filter Main Page Title
remove_action( 'genesis_archive_title_descriptions', 'genesis_do_archive_headings_headline', 10, 3 );
add_action( 'genesis_archive_title_descriptions', 'filter_cpt_archive_title', 10, 3 );

function filter_cpt_archive_title( $heading = '', $intro_text = '', $context = '' ) {
  return null;
}

//Heading Section
add_action('genesis_before_content_sidebar_wrap', 'hennessey_archive_header');

// Runs the Genesis loop.
genesis();