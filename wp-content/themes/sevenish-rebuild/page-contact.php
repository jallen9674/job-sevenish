<?php
/**
 * Template Name: Contact Us
 */

$_markeup_tags = array( 'site-container' , 'site-inner' , 'content-sidebar-wrap', 'main' );

foreach ($_markeup_tags as $markup_key => $markup_value) {

    add_filter( 'genesis_markup_'. $markup_value .'_open' , 'fn_remove_markeup_tags' , 10 , 2);
    add_filter( 'genesis_markup_'. $markup_value .'_close' , 'fn_remove_markeup_tags' , 10 , 2);

}
//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Remove Default Entry Content
remove_action('genesis_entry_header', 'genesis_do_post_title', 10);
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Add Contact Form Markup
add_action('genesis_before_entry_content', 'hennessey_contact_markup');

 function hennessey_contact_markup(){
	?>

	<div class="contact-page-contact-form">
      <div class="contact-page-contact-form__inner">
      
        <h2 class="contact-page-contact-form__title homepage-section-title">
          Get a Free Case Evaluation
        </h2>
        <div class="contact-page-contact-form__content">
          <?php the_content(); ?>
        </div>

        <div class="contact-page-contact-form__form">
          <?php echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form"]'); ?>
        </div>

      </div>
    </div>

<div class="Address_nap">
	<div class="adress_ph">
		<br>
<b>Sevenish Law Firm</b><br>
101 W Ohio St<br>
Suite 1540<br>
Indianapolis, IN<br>
On Call: 24/7<br>
(317) 636-7777<br>
	<a href="https://www.google.com/maps/dir//Sevenish+Law+Firm,+101+W+Ohio+St+%231540,+Indianapolis,+IN+46204,+United+States/@39.7696979,-86.1624145,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x886b5095c580a4b9:0x1dd4b8f6c60e0b04!2m2!1d-86.1602258!2d39.7696979?hl=en">Get Directions</a>
		</div>
<div class="mapq">	
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3066.66194584092!2d-86.16241448462364!3d39.76969787944563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1dd4b8f6c60e0b04!2sSevenish%20Law%20Firm!5e0!3m2!1sen!2sua!4v1583506618637!5m2!1sen!2sua" width="400" height="270" frameborder="0" style="border:0;" allowfullscreen="">
	</iframe>
	</div>
	</div>
	<?php 
}

// Runs the Genesis loop.
genesis();

