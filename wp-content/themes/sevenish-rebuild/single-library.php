<?php
/**
 * Single Library Template (Newsletter)
 */


//Remove Entry Content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//Add Video Embed, JSON, Meta, & Description
add_action( 'genesis_before_entry_content',  'hennessey_single_video');

//Video Markup
function hennessey_single_video(){

	//Video Information via ACF
	global $post;


	?>

	<div class="single-newsletter-content">
		<div class="single-newsletter-content__inner">

			<div class="single-newsletter-content__image">
				<?php 
				 if (has_post_thumbnail()) {
					the_post_thumbnail('full', ['alt'=>get_the_title()]);
				}
				?>
			</div>
			<div class="single-newsletter-content__content">
				<?php the_content(); ?>
			</div>
		
		</div>	
	</div>

	<?php 
}


// Runs the Genesis loop.
genesis();
