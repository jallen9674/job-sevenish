<?php

add_action( 'wp_enqueue_scripts', 'custom_include_assets' );

function custom_include_assets() {
    $jsArray = [
        CHILD_URL . '/assets/libs/slideout/slideout.js',
        //CHILD_URL . '/assets/libs/isotope/isotope.js',
        CHILD_URL . '/assets/libs/list/list.js',
        CHILD_URL . '/assets/libs/owl-carousel/owl.carousel.js',
        CHILD_URL . '/assets/app/js/main.js'
    ];

    $cssArray = [
//        CHILD_URL . '/assets/libs/izimodal/css/iziModal.min.css',
//        CHILD_URL . '/assets/libs/swiper/css/swiper.min.css'
    ];

    foreach($cssArray as $css_asset) {
        wp_enqueue_style( basename($css_asset), $css_asset );
    }

    foreach($jsArray as $js_asset) {
        wp_enqueue_script( basename($js_asset), $js_asset, array('jquery'), '1.0.0', true );
    }


    // include general theme files
    wp_enqueue_style( 'theme-main', CHILD_URL . '/assets/app/css/main.css' );
    //wp_enqueue_script( 'theme-main', CHILD_URL . '/assets/app/js/main.js', array('jquery'), '1.0.0', true );

}
