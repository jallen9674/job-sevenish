"use strict";

(function ($) {


    // FAQ Sorting Functionality

    $('#filters a').click(function(e){
        e.preventDefault();

        var term = $(this).data('filter')
        $(this).addClass('filter-active').siblings().removeClass('filter-active');

        if(term === 'all') {
            $('#items > div').fadeIn();
        } else {
            $('#items > div').fadeOut(0);
            $('#items > div[data-filter-item="'+term+'"]').fadeIn();

            if($('#items > div[data-filter-item="'+term+'"]').length == 2) {
                $('#items').css('justify-content', 'space-around');
            } else {
                $('#items').css('justify-content', 'space-between');
            }
        }
    });

    /* Activate Mobile menu*/
    //Set the main site container
    var siteContainer = document.getElementsByClassName('main-wrap');
    var menuContainer = document.getElementsByClassName('mobile-menu');
    var slideout = new Slideout({
      'panel': siteContainer[0],
      'menu': menuContainer[0],
      'padding': 256,
      'tolerance': 70,
      'easing': 'ease',
      'side': 'right',
      'touch' : false
    });

    //Activate the menu
    document.querySelector('.menu-toggle').addEventListener('click', function() {
      slideout.toggle();
    });

    /* Workarrounds for a fixed menu */

    //Add to translate Header, Main site wrap and Footer
      slideout.on('translate', function(translated) {
        //Fixed header animation
        jQuery('.site-header').css('transition','-webkit-transform 0ms ease');
        jQuery('.site-header').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.site-header').css('transform','translateX('+translated+'px)');
        //Site inner animation
        jQuery('.main-wrap').css('transition','-webkit-transform 0ms ease');
        jQuery('.main-wrap').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.main-wrap').css('transform','translateX('+translated+'px)');
        //Footer animation
        jQuery('.site-footer').css('transition','-webkit-transform 0ms ease');
        jQuery('.site-footer').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.site-footer').css('transform','translateX('+translated+'px)');
        //Page H1 animation
        jQuery('.homepage-hero-image').css('transition','-webkit-transform 0ms ease');
        jQuery('.homepage-hero-image').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.homepage-hero-image').css('transform','translateX('+translated+'px)');
        //Inner Page H1 animation
        jQuery('.internal-hero-image').css('transition','-webkit-transform 0ms ease');
        jQuery('.internal-hero-image').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.internal-hero-image').css('transform','translateX('+translated+'px)');
        //Copyright animation
        jQuery('.copyright').css('transition','-webkit-transform 0ms ease');
        jQuery('.copyright').css('-webkit-transform','translateX('+translated+'px)');
        jQuery('.copyright').css('transform','translateX('+translated+'px)');
      });

      slideout.on('beforeclose', function() {
        //Fixed header animation
        jQuery('.site-header').css('transition','-webkit-transform 300ms ease');
        jQuery('.site-header').css('-webkit-transform','translateX(0px)');
        jQuery('.site-header').css('transform','translateX(0px)');
        //Site inner animation
        jQuery('.main-wrap').css('transition','-webkit-transform 300ms ease');
        jQuery('.main-wrap').css('-webkit-transform','translateX(0px)');
        jQuery('.main-wrap').css('transform','translateX(0px)');
        //Footer animation
        jQuery('.site-footer').css('transition','-webkit-transform 300ms ease');
        jQuery('.site-footer').css('-webkit-transform','translateX(0px)');
        jQuery('.site-footer').css('transform','translateX(0px)');
        //Page H1 animation
        jQuery('.homepage-hero-image').css('transition','-webkit-transform 300ms ease');
        jQuery('.homepage-hero-image').css('-webkit-transform','translateX(0px)');
        jQuery('.homepage-hero-image').css('transform','translateX(0px)');
        //Inner Page H1 animation
        jQuery('.internal-hero-image').css('transition','-webkit-transform 300ms ease');
        jQuery('.internal-hero-image').css('-webkit-transform','translateX(0px)');
        jQuery('.internal-hero-image').css('transform','translateX(0px)');
        //Copyright animation
        jQuery('.copyright').css('transition','-webkit-transform 300ms ease');
        jQuery('.copyright').css('-webkit-transform','translateX(0px)');
        jQuery('.copyright').css('transform','translateX(0px)');
    });

    slideout.on('beforeopen', function() {
        //Fixed header animation
        jQuery('.site-header').css('transition','-webkit-transform 300ms ease');
        jQuery('.site-header').css('-webkit-transform','translateX(-255px)');
        jQuery('.site-header').css('transform','translateX(-255px)');
        //Site inner animation
        jQuery('.main-wrap').css('transition','-webkit-transform 300ms ease');
        jQuery('.main-wrap').css('-webkit-transform','translateX(-255px)');
        jQuery('.main-wrap').css('transform','translateX(-255px)');
        //Footer animation
        jQuery('.site-footer').css('transition','-webkit-transform 300ms ease');
        jQuery('.site-footer').css('-webkit-transform','translateX(-255px)');
        jQuery('.site-footer').css('transform','translateX(-255px)');
        //Page H1 animation
        jQuery('.homepage-hero-image').css('transition','-webkit-transform 300ms ease');
        jQuery('.homepage-hero-image').css('-webkit-transform','translateX(-255px)');
        jQuery('.homepage-hero-image').css('transform','translateX(-255px)');
        //Inner Page H1 animation
        jQuery('.internal-hero-image').css('transition','-webkit-transform 300ms ease');
        jQuery('.internal-hero-image').css('-webkit-transform','translateX(-255px)');
        jQuery('.internal-hero-image').css('transform','translateX(-255px)');
        //Copyright animation
        jQuery('.copyright').css('transition','-webkit-transform 300ms ease');
        jQuery('.copyright').css('-webkit-transform','translateX(-255px)');
        jQuery('.copyright').css('transform','translateX(-255px)');
    });


    /* Navigation Sub-Menus */
    //Add dropdown arrow
    jQuery('.mobile-menu .menu-item-has-children').append('<span class="mobile-menu-oppener"></span>')

    jQuery('.mobile-menu .menu-item-has-children a').click(function (e) {
      var hrefVal = jQuery(this).attr('href');
      if (hrefVal == '#') {
        e.preventDefault();
      }
      jQuery(this).parent().find('open').removeClass('open');
      jQuery(this).parent().toggleClass('open');
    });

    //Add functionality for the mobile menu oppener
    jQuery('.mobile-menu .mobile-menu-oppener').click(function (e) {
      e.preventDefault();
      jQuery(this).parent().find('open').removeClass('open');
      jQuery(this).parent().toggleClass('open');
    });

    jQuery(document).ready(function(){
    	jQuery('#burger').click(function(){
    		jQuery(this).toggleClass('open');
    	});
    });

})(jQuery);
