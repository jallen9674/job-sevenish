<?php
function jsonldBlog()
{
	//Start Blog Settings
  	global $post;
	$thePostID = $post->ID;
  $post_type = get_post_type($thePostID);

	if (get_field('schema_type_json', $thePostID)) {
		$schema_type = get_field('schema_type_json', $thePostID);
	}

  if (get_field('assign_blogposting_schema_to_all_posts', 'options')) {
    $all_pages = get_field('assign_blogposting_schema_to_all_posts', 'options');
  }
  if (get_field('select_pages_to_display_blogposting_schema', 'options')) {
    $show_on_pages = get_field('select_pages_to_display_blogposting_schema', 'options');
  }

	//if ($schema_type == 'blog' || get_field('schema_type', 'options')) TODO: make this functionality working with overwrite if cpt is selected
	if ($schema_type == 'blog' || ( (is_single($thePostID) && ($post_type == 'post')) && $all_pages === true) || in_array($thePostID, $show_on_pages) ) {
		$description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
		$short_description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
		$short_description = wp_strip_all_tags($short_description);
		$short_description = json_encode($short_description);
		$description = wp_strip_all_tags($description);
		$description = json_encode($description);
		$page_url = get_the_permalink($thePostID);
		$the_title = json_encode(get_the_title($thePostID));
		$date_published = get_the_date('d-m-Y',$thePostID);
		$date_modified = get_the_modified_date('d-m-Y',$thePostID);
		$image = get_the_post_thumbnail_url($thePostID, 'large');

		if( empty(get_field('author_json', 'options')) ) {
			$author = json_encode(get_the_author_meta('display_name'));
		} else {
			$author = json_encode(get_field('author_json', 'options'));
		}

		if(empty($image)) {
			ob_start();
			ob_end_clean();
			$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
			$image = $matches[1][0];
		}
		if(empty($image)) {
			$image = get_field('default_post_thumbnail', 'options');
		}

		if(!empty($image)) {
			$image_size = getimagesize($image);

			$image_html = '"image": {
				"@type": "ImageObject",
				"height": "'. $image_size[0] .'",
				"width": "'. $image_size[1] .'",
				"url": "'. $image .'"
			  },';
		}

		$html = '<script type="application/ld+json">
			{
			  "@context": "http://schema.org/",
			  "@type": "BlogPosting",
			  "mainEntityOfPage": {
					 "@type": "WebPage",
					 "@id": "'. $page_url .'"
				  },
			  "headline": '. $the_title .',
			  "datePublished": "'. $date_published .'",
			  "dateModified": "'. $date_modified .'",
			  "description": '. $short_description .',
			  '. $image_html .'
			  "author": '. $author .',
			  "publisher": {
				"@type": "Organization",
				"logo": {
				  "@type": "ImageObject",
				  "url": "'. get_field('logo_json', 'options') .'"
				},
				"name": '. json_encode(get_field('publisher_json', 'options')) .'
			  },
			  "articleBody": '.$description.'
			}
			</script>';

		echo $html;
	}
}
//End Blog Settings