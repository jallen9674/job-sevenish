<?php
function jsonldWebpage()
{ 
	global $post;
	
	$description = hc_strip_shortcodes(wpautop( get_the_content( $thePostID )));
	//var_dump($description);
	$description = substr( $description, 0, strpos( $description, '</p>' ) + 4 );
	$description = wp_strip_all_tags($description);
	$description = json_encode($description);
	$page_url = get_the_permalink( $post->ID );
	$the_title = json_encode(get_the_title( $post->ID ));
	
	if( empty(get_field('author_json', 'options')) ) {
		$author = json_encode(get_the_author_meta('display_name'));
	} else {
		$author = json_encode(get_field('author_json', 'options'));
	}
	
	$html = '<script type="application/ld+json">
	{ 
	  "@context": "http://schema.org", 
	  "@type": "WebSite", 
	  "url": "'. $page_url .'", 
	  "name": '. $the_title.',
	   "author": {
		  "@type": "Person",
		  "name": '. $author .'
		},
	  "description": '. $description .',
	  "publisher": '. json_encode(get_field('publisher_json', 'options')) .'
	} 
	</script>';
	
	echo $html;
}
