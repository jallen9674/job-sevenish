<?php
function jsonldNav()
{

	if (!empty(get_field('select_menu_to_be_used_for_navigation_schema', 'options'))) {
		$menu = get_field('select_menu_to_be_used_for_navigation_schema', 'options');
    $menuObj = wp_get_nav_menu_items($menu->ID);
    $post_html = '';

    $menuObj = array_filter($menuObj, function($page){
      return strpos($page->url, home_url()) !== false;
    });

    $i = 1;
    foreach ($menuObj as $page) {
      $page_ID = url_to_postid($page->url);
  		$page_url = $page->url;
  		$the_title = json_encode($page->title);

      $post_html .= '{
        "@type": "SiteNavigationElement",
        "position":'. $i .',
        "name": '. $the_title .',
        "url": "'. $page_url .'"
      }';

      if ($i < count($menuObj)) {
			  $post_html .= ',';
			}

			$i++;
    }

    $html = '<script type="application/ld+json">{
      "@context":"http://schema.org",
      "@type":"ItemList",
      "itemListElement":[
        '. $post_html .'
      ]
    }</script>';

    echo $html;
	}
}
