<?php
//Adding top CTA Accepts "top" and "bottom"

/*-----------------------
General Page CTA
------------------------*/

function content_cta($placement) {

	global $post;
	$is_top_level = '';
  $this_page_keyword = '';
	$html = '';
  $city_name = '';
	$hit_it = false;
	$post_id = $post->ID;
  $site_url = get_site_url();

  if ($post_id) {
    $this_page_keyword = get_post_meta( $post_id, '_hc_related_widget_title', true );
  }
	if ($post->post_parent) {
		$is_top_level = get_post_meta( $post->post_parent, '_hc_related_widget_title', true );
  }
  
	switch ($is_top_level) {
		case 'Personal Injury':
			$hit_it = false;
			break;
		case 'Birth Injuries':
		case 'Birth Injury':
		case 'Car Accident':
		case 'Car Accidents':
		case 'Defective Drug':
		case 'Defective Tires':
		case 'Medical Malpractice':
		case 'Slip and Fall':
		case 'Slip and Fall Accidents':
		case 'Slip & Fall Injury':
		case 'Tire Accident':
		case 'Truck Accident':
    case 'Truck Accidents':
    case 'Motorcycle Accidents':
    case 'Motorcycle Accident':
    case 'Workers Comp.':
    case 'Workers\' Compensation':
    case 'Construction Accidents':
    case 'Pedestrian Accidents':
		case 'Motorcycle Accidents':
    case 'Work Injury':
    case 'Work Injuries':
			$hit_it = true;
			break;

		default:
			$hit_it = false;
			break;
	}

  
    $phone_num_html =  PHONE_NUMBER;
    $phone_num_anchor = PHONE_NUMBER;


	if ($is_top_level == 'Personal Injury') { //Check if page is Personal Injury
    if ($placement == 'top') {
      $html = '<div class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' Personal Injury Lawyer">'.$city_name.' Personal Injury Lawyer</a>.</div>';
    }
    if ($placement == 'bottom' && $is_top_level != '') {
      $html = '<div class="bottomcta">Our team of dedicated <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyers">'.$city_name.' '.$is_top_level.' Lawyers</a> can help with your '.$is_top_level.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a>.</div>';
    }
	}

	if ($hit_it == true) {

		if ($is_top_level == 'Workers Comp.') {
			$is_top_level = 'Workers Compensation';
		}

		if ( ($is_top_level == 'Car Accidents') || ($is_top_level == 'Car Accident') ) {
			$is_top_level = 'Car Accident';
		}

		if ($is_top_level == 'Slip and Fall Accidents') {
			$is_top_level = 'Slip and Fall';
		}
		if ( ($is_top_level == 'Slip & Fall Injury') || ($is_top_level == 'Slip and Falls')) {
			$is_top_level = 'Slip and Fall';
		}

    if ($is_top_level == 'Truck Accidents') {
			$is_top_level = 'Truck Accident';
		}
    if ($is_top_level == 'Construction Accidents') {
			$is_top_level = 'Construction Accident';
    }
    
    //All Non Personal Injury Pages
    if ($placement == 'top') {
      $html = '<div class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyer">'.$city_name.' '.$is_top_level.' Lawyer</a>.</div>';
    }
    if ($placement == 'bottom') {
      $html = '<div class="bottomcta">Our team of dedicated <a href="'.get_permalink($post->post_parent).'" title="'.$city_name.' '.$is_top_level.' Lawyers">'.$city_name.' '.$is_top_level.' Lawyers</a> can help with your '.$is_top_level.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a>.</div>';
    }
	}
	return $html;
}


/*-----------------------
FAQ CTA
------------------------*/

function faq_cta($placement) {

  global $post;

	$post_id = $post->ID;
  $hit_it = false;
  $category_title = '';
  $cta_url = '';  
  $phone_num_html =  PHONE_NUMBER;
  $phone_num_anchor = PHONE_NUMBER;
  $site_url = get_site_url();

  if (!has_term('', 'faq-cat')) {
    return;
  }

  $terms = get_the_terms($post_id, 'faq-cat');
  $master_term_name = $terms[0]->name;

  switch ($master_term_name) {
    case 'Bicycle Accidents':
      $hit_it = true;
      $category_title = 'Bicycle Accident';
      $cta_url = get_the_permalink(30);
      break;
    case 'Bus Accidents':
      $hit_it = true;
      $category_title = 'Bus Accident';
      $cta_url = get_the_permalink(33);
      break;
    case 'Car Accidents':
      $hit_it = true;
      $category_title = 'Car Accident';
      $cta_url = get_the_permalink(34);
      break;
    case 'Child Injury':
      $hit_it = true;
      $category_title = 'Child Injury';
      $cta_url = get_the_permalink(35);
      break;
    case 'Motorcycle Accidents':
      $hit_it = true;
      $category_title = 'Motorcycle Accident';
      $cta_url = get_the_permalink(5);
      break;
    case 'Slip and Fall':
      $hit_it = true;
      $category_title = 'Slip and Fall';
      $cta_url = get_the_permalink(404);
      break;
    case 'Truck Accidents':
      $hit_it = true;
      $category_title = 'Truck Accident';
      $cta_url = get_the_permalink(41);
      break;

    default:
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url;
      break;
  }

    if ($placement == 'top') {
      $html = '<p class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.$cta_url.'" title="Indianapolis '.$category_title.' Lawyer">Indianapolis  '.$category_title.' Lawyer</a>.</p>';
    }
    if ($placement == 'bottom') {
      $html = '<p class="bottomcta">Our team of dedicated <a href="'.$site_url.'" title="Indianapolis Personal Injury Lawyers">Indianapolis Personal Injury Lawyers</a> can help with your '.$category_title.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> with a '.$category_title.' Lawyer in Indianapolis.</p>';
    }
    return $html;
}


/*-----------------------
Blog CTAs
------------------------*/

function blog_cta($placement)
{
  global $post;

	$post_id = $post->ID;
  $hit_it = false;
  $category_title = '';
  $cta_url = '';
  $site_url = get_site_url();

  $phone_num_html =  PHONE_NUMBER;
  $phone_num_anchor = PHONE_NUMBER;

  
  if (!has_term('', 'category')) {
    return;
  }

  $terms = get_the_terms($post_id, 'category');
  $master_term_name = $terms[0]->name;

  switch ($master_term_name) {
    
    case 'Bicycle Accidents':
      $hit_it = true;
      $category_title = 'Bicycle Accident';
      $cta_url = get_the_permalink(30);
    break;
    case 'Car Accident':
      $hit_it = true;
      $category_title = 'Car Accident';
      $cta_url = get_the_permalink(34);
    break;
    case 'Children Injury':
    $hit_it = true;
    $category_title = 'Child Injury';
    $cta_url = get_the_permalink(35);
    break;
    case 'Construction Site Accident':
    $hit_it = true;
    $category_title = 'Construction Site Accident';
    $cta_url = get_the_permalink(36);
    break;
    case 'Dog Bite Injury':
    $hit_it = true;
    $category_title = 'Animal & Dog Bite Injury';
    $cta_url = get_the_permalink(394);
    break;
    case 'DUI':
    case 'Drunk Driving':
    $hit_it = true;
    $category_title = 'Drunk Driving';
    $cta_url = get_the_permalink(274);
    break;
    case 'First Responders':
    $hit_it = true;
    $category_title = 'First Responder';
    $cta_url = get_the_permalink(42);
    break;
    case 'Motorcycle Accidents':
    $hit_it = true;
    $category_title = 'Motorcycle Accident';
    $cta_url = get_the_permalink(5);
    break;
    case 'Pedestrian Accidents':
    $hit_it = true;
    $category_title = 'Pedestrian Accident';
    $cta_url = get_the_permalink(4);
    break;
    case 'Premises Liability':
    $hit_it = true;
    $category_title = 'Slip and Fall';
    $cta_url = get_the_permalink(404);
    break;
    case 'TBI':
    $hit_it = true;
    $category_title = 'Brain Injury';
    $cta_url = get_the_permalink(31);
    break;
    case 'Truck Accident':
    $hit_it = true;
    $category_title = 'Truck Accident';
    $cta_url = get_the_permalink(41);
    break;
    case 'Workers Compensation':
    $hit_it = true;
    $category_title = 'Workers\' Compensation';
    $cta_url = get_the_permalink(39);
    break;
    

    default:
      $hit_it = true;
      $category_title = 'Personal Injury';
      $cta_url = $site_url;
    break;
  }
    if ($placement == 'top') {
      $html = '<div class="topcta">If you have been injured in an accident, call <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> to get a free consultation from a <a href="'.$cta_url.'" title="Indianapolis '.$category_title.' Lawyer">Indianapolis '.$category_title.' Lawyer</a>.</div>';
    }
    if ($placement == 'bottom') {
      $html = '<div class="bottomcta">Our team of dedicated <a href="'.$site_url.'" title="Indianapolis Personal Injury Lawyers">Indianapolis Personal Injury Lawyers</a> can help with your '.$category_title.' case today. Get Free Case Review at <a href="tel:'.$phone_num_html.'">'.$phone_num_anchor.'</a> with a '.$category_title.' Lawyer in Indianapolis.</div>';
    }
    return $html;
}

function localize_us_number($phone) {
  $numbers_only = preg_replace("/[^\d]/", "", $phone);
  return preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $numbers_only);
}
