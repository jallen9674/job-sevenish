/*------------------------------------------------
General JS (Most of this is from original theme)
-------------------------------------------------*/

jQuery(document).ready(function($) {
  



    //Sorting Alphabetically
    $.fn.sortList = function() {
        var mylist = $(this);
        var listitems = $('li', mylist).get();
        listitems.sort(function(a, b) {
            var compA = $(a).text().toUpperCase();
            var compB = $(b).text().toUpperCase();
            return (compA < compB) ? -1 : 1;
        });
        $.each(listitems, function(i, itm) {
            mylist.append(itm);
        });
    }


    //Sort Related Areas List
    $("ul.related-listing").sortList();
    //$("ul.location-listing").sortList();


    //Practice Area Filter Functionality

    var options = {
      valueNames: [ 'parent', 'child' ]
    };

    var practiceAreaList = new List('pa-list', options);

    practiceAreaList.on('updated', function(){
      console.log('filter-fire!' + practiceAreaList.visibleItems.length);

      if ( !$('#pa-list .list').hasClass('filtered') ) {
        $('#pa-list .list').addClass('filtered');
      }

      if ( (practiceAreaList.searched === false) ) {
        $('#pa-list .list').removeClass('filtered');
        console.log('default');
      }

      if ( (practiceAreaList.visibleItems.length <= 7) ) {
        $('#pa-list .list').addClass('filtered');
      } else {
        $('#pa-list .list').removeClass('filtered');
      }


    });

    /*Location Specific Menu Functiona lity
    var locationMenu = $('.taxonomy-menu-wrapper .menu').html();
    console.log(locationMenu);
    if (locationMenu){
      $('#childrenof100 .links-list').html(locationMenu);
    }*/
    


    $('.btn-load-more-videos').click(function(e){
      e.preventDefault();

      $('.video-box-hidden').each(function(i){
        if(i < 6) {
          $(this).removeClass('video-box-hidden')
          $(this).find('iframe').attr('src', $(this).find('iframe').data('src'));
        }
      });
    })


    /*-------------------------------
    Adjust Menu Practice Area List
    - Will only adjust city location menus
    - Depends on the population of the widget in related-widget.php
    ---------------------------------*/

    /*--------------------------------------------
    Nicer Formatted Dropdown menus if a lot of Children
    - Practice areas only
    ---------------------------------------------*/
    

    var practiceAreaSubmenu = $('.use-in-menu').html();
    if ( practiceAreaSubmenu ) {
    
        $('.practice-area-sub .sub-menu').html(practiceAreaSubmenu);

        if ( $(".practice-area-sub .sub-menu li").length < 14 ){
          $('.practice-area-sub').removeClass('practice-area-sub');
        }
              
    }


    /*-------------------------------
    Add Header Class on Scroll

    -- TODO: Refactor if time
    ---------------------------------*/

   
    function headerScrolledStatus(window, desktop, mobile){
      
      var header = $("body");
      var scroll = $(document).scrollTop();
      
        //Mobile Resolutions
        if ( window < 768) {
          
          if (scroll > 0) {
            header.addClass("scrolled");
            //header.css('top', mobile);
          } else {
              header.removeClass("scrolled");

              setTimeout(function() {
                  var animatedHeight = $('.mobile-header').outerHeight();
                  header.css('top', animatedHeight );
              }, 210);
          }         
         
        }

        //Desktop Resolutions
        if ( window >= 768) {
          if (scroll > 0) {
              header.addClass("scrolled");
              //header.css('top', desktop - 93);
          } else {
              header.removeClass("scrolled");
              header.css('top', 0);
          }
      }

    }


    //Calling 

    var windowWidth =  $(window).width();
    var headerHeight = $('.site-header').outerHeight();
    var mobileHeaderHeight = $('.mobile-header').outerHeight();

    headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);

    $(window).resize( throttle( function() { 

      var windowWidth =  $(window).width();
      var headerHeight = $('.site-header').outerHeight();
      var mobileHeaderHeight = $('.mobile-header').outerHeight();
      
      headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);
    }, 30));
    
    $(window).scroll( debounce( function() { 
      var windowWidth =  $(window).width();
      var headerHeight = $('.site-header').outerHeight();
      var mobileHeaderHeight = $('.mobile-header').outerHeight();
      var scroll = $(window).scrollTop(); 

      headerScrolledStatus(windowWidth, headerHeight, mobileHeaderHeight);       
       
    }, 100));



    /*--------------------------------------------
    Adding Drop Down Arrows to Menu Items
    ---------------------------------------------*/

    $('.desktop-header-navigation .menu-item-has-children a').append('<span class="desktop-downarrow"></span>');

    
    /*--------------------------------------------
    Mobile Slideout Navigation 
    ---------------------------------------------*/
    

      var slideout = new Slideout({
        'panel':  document.querySelector('.site-container'),
        'menu': document.getElementById('mobile-menu'),
        'padding': 256,
        'tolerance': 70, 
        'side': 'right'
      });
  
      document.querySelector('.navigation-pane-toggle').addEventListener('click', function() {
        slideout.toggle();
      });
      document.querySelector('.navigation-pane-close').addEventListener('click', function() {
        slideout.close();
      });

      var fixed = document.querySelector('.mobile-header');

      slideout.on('translate', function(translated) {
        fixed.style.transform = 'translateX(' + translated + 'px)';
      });

      slideout.on('beforeopen', function () {
        fixed.style.transition = 'transform 300ms ease';
        fixed.style.transform = 'translateX(-256px)';
      });

      slideout.on('beforeclose', function () {
        fixed.style.transition = 'transform 300ms ease';
        fixed.style.transform = 'translateX(0px)';
      });

      slideout.on('open', function () {
        fixed.style.transition = '';
      });

      slideout.on('close', function () {
        fixed.style.transition = '';
      });

    /*--------------------------------------------
    Homepage Claim Process Functionality
    ---------------------------------------------*/

    $('.homepage-claims a[data-toggle="tab"]').on('click',function(e){

      e.preventDefault();

      var tabTarget = $(this).data('target');
      tabTarget =  '#' + tabTarget;
      
      if ( $(this).hasClass('active') ) {
        //Do Nothing
      } else {
        $('.active.in').removeClass('active').removeClass('in');
        $('.homepage-claims .nav-tabs li.active').removeClass('active');
        $(this).parent().addClass('active');
        $(tabTarget).addClass('active').addClass('in');
      }


    });

          
    /*--------------------------------------------
    Carousel Init
    ---------------------------------------------*/

    $('.btnNext').click(function () {
      if ($('.tabs-block .nav.nav-tabs li:last-of-type').hasClass('active')) {
          $('.tabs-block .nav.nav-tabs li:first-of-type a').trigger('click');
      } else {
          $('.nav-tabs > .active').next('li').find('a').trigger('click');
      }

  });

  $('.btnPrevious').click(function () {
      if ($('.tabs-block .nav.nav-tabs li:first-of-type').hasClass('active')) {
          $('.tabs-block .nav.nav-tabs li:last-of-type a').trigger('click');
      } else {
          $('.nav-tabs > .active').prev('li').find('a').trigger('click');
      }
  });

      $('.award-slider-section .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        slideSpeed: 1000,
        autoplaySpeed: 1000,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        autoplay: true,
        dots: true,
        dotsSpeed: 500,
        navigation: true,
        navigationspeed: 1000,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 3
            },
            768: {
                items: 4
            },
            1000: {
                items: 5,
                navigation: false
            }
        }
    });
    $(".owl-prev").html('<span class="fa fa-chevron-left"></span>');
    $(".owl-next").html('<span class="fa fa-chevron-right"></span>');
    $('.footer-review-section .owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        responsiveClass: true,
        autoplaySpeed: 1000,
        dots: true,
        navigation: true,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        autoplayHoverPause: true, 
        autoplay: false,
        
        navSpeed: 1000,
        dotsSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
                
            }
        } 
    });


    /*--------------------------------------------
    Homepage Youtube Load
    ---------------------------------------------*/

    var homepageYouTubeEmbed = '<div class="responsive-iframe-wrapper"><iframe src="https://www.youtube.com/embed/O4TX1FHcBl8?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';

    $('.youtube-placeholder').on('click', function(){
      $('.homepage-difference__left').html(homepageYouTubeEmbed);
    });



  }); //End Document Ready









/*-------------------------------
FAQS
---------------------------------*/

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;

 jQuery(container).each(function() {

   $el = jQuery(this);
   jQuery($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

  var $container = jQuery('.faq-filter-list').isotope({
    itemSelector: '.grid-item',
    layoutMode: 'fitRows',
    getSortData: {
      category: '[data-category]'
    },
    hiddenClass: 'isotope-hidden',
  });

// filter functions
  var filterFns = {

  };

  var itemReveal = Isotope.Item.prototype.reveal;
Isotope.Item.prototype.reveal = function() {
  itemReveal.apply( this, arguments );
  jQuery( this.element ).removeClass('isotope-hidden');
};

var itemHide = Isotope.Item.prototype.hide;
Isotope.Item.prototype.hide = function() {
  itemHide.apply( this, arguments );
  jQuery( this.element ).addClass('isotope-hidden');
};

  // bind filter button click
  jQuery('#filters').on( 'click', 'button', function() {

    var filterValue = jQuery( this ).attr('data-filter');
    // use filterFn if matches value
    filterValue = filterFns[ filterValue ] || filterValue;
    $container.isotope({ filter: filterValue });



  });

  // change is-checked class on buttons
  jQuery('.filters-button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = jQuery( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      jQuery( this ).addClass('is-checked');
    });
  });


  $container.on( 'arrangeComplete',  function( event, filteredItems ) {
    //console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );
    //equalheight('.faq-filter-listing');
  });

  $container.on( 'layoutComplete',  function( event, filteredItems ) {
    console.log( 'Layout Complete Isotope arrange completed on ' + filteredItems.length + ' items' );
    //jQuery('.faq-filter-listing').addClass('sorted');

     if(jQuery('.isotope-link:nth-of-type(1)').hasClass('is-checked')){
      jQuery('.faq-filter-listing').removeClass('sorted');
     }

     equalheight('.faq-filter-listing:not(.isotope-hidden)');

  });


  //Equal Height Columns
  equalheight('.faq-filter-listing:not(.isotope-hidden)');
