<?php
/**
 * Template Name: Newsletters List
 * - Note: Markup copied from legacy theme
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//* Remove the post info function
remove_action( 'genesis_before_post_content', 'genesis_post_info' );

//Adding Team Member
add_action( 'genesis_after_entry_content', 'hennessey_list_newsletters' );

// Runs the Genesis loop.
genesis();


function hennessey_list_newsletters(){
    
	?>

<div class="newsletter-list fullwidth">
        <?php
        $i = 1;
        $args = array(
            'post_type' => 'library',
            'posts_per_page' => '-1',
            'paged' => get_query_var('paged'),
            'tax_query' => array(
                array(
                    'taxonomy' => 'library-cat',
                    'field' => 'slug',
                    'terms' => array('newsletters')
                )
            )
        );
        $parent_nav = new WP_Query($args);
        ?> 
        <?php
        if ($parent_nav->have_posts()) : while ($parent_nav->have_posts()) : $parent_nav->the_post();
                global $post;        
                $post = get_post($post);
                $backgroundimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                ?>
                <div class="single-list-newsletter blog-listing-<?php echo $i; ?>">
                    <a target="_blank" href="<?php
                    if (get_field('select_newsletter_pdf')) {
                        the_field('select_newsletter_pdf');
                    } else {
                        the_permalink();
                    }
                    ?>" class=" <?php echo $post->post_type; ?>-listing blog-featured bg-cover <?php
                        if (has_post_thumbnail()) {
                            echo 'featured-bg';
                        }
                        ?>" style="background-image: url(<?php echo $backgroundimage; ?>)">
                    </a>
                    <div class="blog-content half newsletter-content">
                        <h3><a target="_blank" title="<?php the_title(); ?>" href="<?php
                            if (get_field('select_newsletter_pdf')) {
                                the_field('select_newsletter_pdf');
                            } else {
                                the_permalink();
                            }
                            ?>"><?php the_title(); ?></a></h3>
                        <p> 
                            <?php
                            echo wp_trim_words(get_the_excerpt(), 20, '...');
                            ?>
                        </p>
                        <a class="read-more" target="_blank" href="<?php
                    if (get_field('select_newsletter_pdf')) {
                        the_field('select_newsletter_pdf');
                    } else {
                        the_permalink();
                    }
                    ?>" title="<?php the_title(); ?>">Read More</a>
                    </div>
                    <hr/>
                </div>
                <?php
                $i++;
            endwhile;
            
            wp_reset_query();
        endif;
        ?>
    </div>
</div>

	<?php 
}