<?php
/**
 * Template Name: Contact Us
 */


//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Remove Default Entry Content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Add Contact Form Markup
add_action('genesis_before_entry_content', 'hennessey_contact_markup');

 function hennessey_contact_markup(){
	?>

	<div class="contact-page-contact-form">
      <div class="contact-page-contact-form__inner">
      
        <h2 class="contact-page-contact-form__title homepage-section-title">
          Get a Free Case Evaluation
        </h2>
        <div class="contact-page-contact-form__content">
          <?php the_content(); ?>
        </div>

        <div class="contact-page-contact-form__form">
          <?php echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form"]'); ?>
        </div>

      </div>
    </div>

	<?php 
}

// Runs the Genesis loop.
genesis();

