<?php
/**
 * Template Name: Homepage Template
 */


// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Remove Default the_content()
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Adding Homepage Layout
add_action( 'genesis_before_content',  'hennessey_homepage_layout');

//Adjusting Body Class
add_filter( 'body_class', 'hennessey_body_class' );
function hennessey_body_class( $classes ) {
	$classes[] = 'homepage-template';
	return $classes;
}

// Runs the Genesis loop.
genesis();


/*------------------------------
Homepage Layout
--------------------------------*/

function hennessey_homepage_layout(){
	?>

    <?php
    /* HERO SECTION */
    ?>

    <div class="homepage-hero">
      <div class="homepage-hero__inner wrapper">

        <div class="homepage-hero__content">

          <div class="homepage-hero__content-box">
            <div class="homepage-hero__content-bg">
              <h1 class="homepage-hero__content-box-title">
                Your Indiana <br>Personal Injury Lawyer
              </h1>
              <span class="homepage-hero__content-box-subtitle">
                <strong>Fierce Protectors</strong> Of the Injured&trade;
              </span>
            </div>

            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/hero-logos.png" alt="Sevenish Law Trust Logos" class="homepage-hero__content-box-logos">
          </div>

        </div>

        <div class="homepage-hero__form">
          
          <span class="homepage-hero__form-title">
            Free Case Evaluation
          </span>
          
          <span class="homepage-hero__form-subtitle">
            Quick. Easy. Confidential.
          </span>

          <?php echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form"]'); ?>
        </div>
      
      </div>
    </div>

    <?php
    /* THE DIFFERENCE */
    ?>

    <div class="homepage-difference homepage-section">
      <div class="homepage-difference__inner wrapper">
      
        <div class="homepage-difference__left">
          <div class="youtube-placeholder">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/homepage-youtube-thumb.jpg" alt="Randall Sevenish Intro Video" />
          </div>
        </div>

        <div class="homepage-difference__right">
          <h2 class="homepage-difference__title homepage-section-title">
            The Sevenish Law Difference
          </h2>
          <div class="homepage-seperator-bar"></div>
          <div class="homepage-difference__content">
            <p>We have experienced, dedicated Indianapolis personal injury attorneys and staff, who will work hard to get you all the cash and benefits you deserve for your injuries. Over the years, we’ve gained a reputation with insurance companies for being aggressive and fighting for the rights of our clients. We are there for you every step of the way. We will meet with you where and when it’s convenient for you and ensure that you understand what legal options you have available. You’ll feel comfortable knowing our Indianapolis accident lawyers are working for you. There is no fee unless we win your case. You can trust Sevenish Law just as thousands of other accident victims have trusted us.</p>
            <p>Our practice has come a long way since the beginning, but it has only gotten stronger. Here, find out more about our personal injury law firm in Indianapolis, including our core values, our law firm’s history, and other details about our practice.</p>
          </div>
        </div>

      </div>
    </div>

    <?php
    /* CONTACT FORM */
    ?>

    <div class="homepage-contact-form homepage-section">
      <div class="homepage-contact-form__inner">
      
        <h2 class="homepage-contact-form__title homepage-section-title">
          Get a Free Case Evaluation
        </h2>
        <p class="homepage-contact-form__content">
          Contact one of our legal experts and get a prompt review of your case.<br>
          24 Hours | 7 days a week
        </p>

        <div class="homepage-contact-form__form">
          <?php echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form" html_class="standard-form"]'); ?>
        </div>

      </div>
    </div>
		
    <?php
    /* PRACTICE AREAS */ 
    ?>

    <div class="homepage-practice-areas homepage-section">
      <div class="homepage-practice-areas__inner wrapper">

        <h2 class="homepage-practice-areas__title homepage-section-title">
          Practice Areas
        </h2>
        <p class="homepage-practice-areas__content">
          Accidents occur daily in Indiana and throughout the country. For people seriously injured in these accidents, life can seem to turn on a dime. One minute they’re going about their business just like any other day and the next, they’re injured and facing a long recovery—and probably a mountain of unexpected medical expenses. With this in mind, our Indianapolis personal injury attorneys can assist with a variety of different types of cases including:
        </p>

        <div class="homepage-practice-areas__grid">

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(41); ?>" class="homepage-practice-area pa-truck-accidents" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-truck-accidents-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Truck Accidents
                </span>
                <p class="homepage-practice-area__content">
                  Accidents on the highways can be lethal events.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(30); ?>" class="homepage-practice-area pa-bicycle-accidents" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-bike-accidents-bg.jpg);">
       
                <span class="homepage-practice-area__title">
                  Bicycle Accidents
                </span>
                <p class="homepage-practice-area__content">
                  Riding a bicycle should not be unsafe.
                </p>
              </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(34); ?>" class="homepage-practice-area pa-car-accidents" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-car-accidents-bg.jpg);">               
                <span class="homepage-practice-area__title">
                  Car Accidents
                </span>
                <p class="homepage-practice-area__content">
                  If you have been seriously injured, call our firm.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(5); ?>" class="homepage-practice-area pa-motorcycle-accidents" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-motorcycle-accidents-bg.jpg);">                
                <span class="homepage-practice-area__title">
                  Motorcycle Accidents
                </span>
                <p class="homepage-practice-area__content">
                  Motorcycle accidents can result in serious permanent injuries.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(1577); ?>" class="homepage-practice-area pa-nursing-home-abuse" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-nursing-home-bg.jpg);">               
                <span class="homepage-practice-area__title">
                  Nursing Home Abuse
                </span>
                <p class="homepage-practice-area__content">
                  Our elderly loved ones deserve respect.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(40); ?>" class="homepage-practice-area pa-wrongful-death" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-wrongful-death-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Wrongful Death
                </span>
                <p class="homepage-practice-area__content">
                  When negligence results in the death of a loved one.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(32); ?>" class="homepage-practice-area pa-burn-injury" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-burn-injury-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Burn Injury
                </span>
                <p class="homepage-practice-area__content">
                  Accidents can result in severe burn injuries.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(31); ?>" class="homepage-practice-area pa-brain-injury" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-brain-injury-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Brain Injury
                </span>
                <p class="homepage-practice-area__content">
                  Brain injuries can have long term severe effects.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(404); ?>" class="homepage-practice-area pa-slip-and-fall" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-slip-and-fall-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Slip and Fall
                </span>
                <p class="homepage-practice-area__content">
                  When hurt in a location, someone must take responsibility.
                </p>
            </a>

            <?php //Begin Single Practice Area?>
            <a href="<?php the_permalink(394); ?>" class="homepage-practice-area pa-dog-bites" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/homepage/pa-dog-bite-bg.jpg);">
                <span class="homepage-practice-area__title">
                  Animal & Dog Bites
                </span>
                <p class="homepage-practice-area__content">
                  If you have been seriously injured, call our firm.
                </p>
            </a>

        </div>
      
      </div>
    </div>

    <?php
    /* READY TO LISTEN */ 
    ?>
    
    <div class="homepage-listen-section">
      <div class="homepage-listen-section__inner wrapper">
      
        <div class="homepage-listen-section__left"></div>

        <div class="homepage-listen-section__right">

          <h2 class="homepage-section-title homepage-listen-section__title">
            Ready to Listen.<br>Ready to Serve.
          </h2>
          <p class="homepage-listen-section__content">
            If you or a loved one is struggling due to another’s negligence, the Sevenish Law Firm is here to help. Our Indianapolis personal injury lawyers can immediately review your case, explain your rights and options, and aggressively pursue the full amount of compensation you deserve for your losses. Please contact our Indiana personal injury lawyers by phone or through our convenient online form to learn more. Our initial consultations are free and confidential, and we always respond to calls within 24 hours.
          </p>

        </div>

      </div>
    </div>

    <?php
    /* PERSONAL INJURY CLAIMS PROCESS */ 
    ?>
    <div class="homepage-claims homepage-section">
      <div class="homepage-claims__inner">
        <div class="homepage-claims__title homepage-section-title">
          Personal Injury Claims Process  
        </div>
        <div class="homepage-seperator-bar homepage-claims__seperator"></div>
        <div class="tabs-block text-center">
            <ul class="nav nav-tabs">
                <?php
                if (have_rows('tabs_section')):
                    $i = 1;
                    while (have_rows('tabs_section')) : the_row();
                        ?>
                        <li class="<?php
                        if ($i == 1) {
                            echo 'active';
                        }
                        ?>">
                            <a data-toggle="tab" href="#tab-content-<?php echo $i; ?>" data-target="tab-content-<?php echo $i; ?>">
                                <span class="tab-number"><?php echo $i; ?></span>
                                <span class="tab-title"><?php the_sub_field('tab_title'); ?></span>
                            </a>
                        </li>
                        <?php
                        $i++;
                    endwhile;
                else :
                endif;
                ?>
            </ul>
            <div class="tab-content">
                <?php
                if (have_rows('tabs_section')):
                    $i = 1;
                    while (have_rows('tabs_section')) : the_row();
                        ?>
                        <div id="tab-content-<?php echo $i; ?>" class="single-tab bg-cover tab-pane fade <?php
                        if ($i == 1) {
                            echo 'in active';
                        }
                        ?>"><div class="wrapper">
                              <div class="col-sm-6 col-sm-offset-6 text-justify tab-main">
                                  <div class="tab-main-content">
                                      <?php the_sub_field('tab_content'); ?>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <?php $i++; ?>

                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>

      </div> 
    </div>  <?php //End CLAIMS?>

    <?php
    /* MEET OUR TEAM */ 
    ?>

    <div class="homepage-team homepage-section">
      <div class="homepage-team__inner wrapper">
      
        <h2 class="homepage-team__title homepage-section-title">
          Meet Our Core Team
        </h2>
        <div class="homepage-seperator-bar"></div>

        <?php //Begin Attorney feed ?>       
        <div class="homepage-team__attorney-loop">
          <?php
            if (have_rows('attorneys_section')): while (have_rows('attorneys_section')) : the_row();
            $postObj = get_sub_field('attorney_page_link');
            $pageLink = get_permalink($postObj);
            $attor_img = get_sub_field('add_attorney_image');
          ?>

            <div class="homepage-attorney">
              <div class="homepage-attorney__image">
                <a href="<?php echo $pageLink ?>">
                  <img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>
                </a>
              </div>
              <span class="homepage-attorney__designation"><?php the_sub_field('add_attorney_designation'); ?></span>
              <span class="homepage-attorney__title"><a href="<?php echo $pageLink ?>"><?php the_sub_field('add_attorney_name'); ?></a></span>
            </div>

          <?php endwhile; endif; ?>
          </div>
        <?php //End Attorney feed ?>

      </div>
    </div> <?php //End MEET OUR TEAM?>
   
	<?php 
}
