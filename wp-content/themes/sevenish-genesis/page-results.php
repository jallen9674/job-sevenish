<?php
/**
 * Template Name: Reults 
 *  Note: reults markup copied from legacy theme
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//Adding Team Member
add_action( 'genesis_after_entry_content', 'hennessey_list_results' );

// Runs the Genesis loop.
genesis();


function hennessey_list_results(){
	?>
	<div class="post-listing fullwidth">
		<?php
		$i = 1;
		$parent_nav = new WP_Query(array(
			'order' => 'desc',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'post_type' => 'case-result',
			'paged' => get_query_var('paged')
		));
			$post = get_post( $post ); 
		?>
		<?php if ($parent_nav->have_posts()) : while ($parent_nav->have_posts()) : $parent_nav->the_post();     $post = get_post( $post );   ?>
				<div class="report-listing-<?php echo $i;?> result-listing">

					<div class="blog-content half">
						<h3> <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<span class="result-listing__meta"><?php
							$terms = get_the_terms(get_the_ID(), 'case-cat');
							if ($terms && !is_wp_error($terms)) :
								$category_links = array();
								foreach ($terms as $term) {
									$category_links[] = $term->name;
								}
								$category_link = join(", ", $category_links);
								?>
								<?php
							endif;
							echo $category_link;
							?>
						</span>
						<p> 
							<?php echo wp_trim_words(get_the_excerpt(), 22, '...');	?>
						</p>
						<a class="result-listing__more" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More &raquo;</a>
					</div>
				</div>
				<?php
				$i++;
			endwhile;
			
			wp_reset_query();
		endif;
		?>
	</div>
</div>
	
	<?php 
}