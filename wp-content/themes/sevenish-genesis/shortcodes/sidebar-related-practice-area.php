<?php
/*
Sidebar Shortcode/Widget to Display Same Practice Area Located in 
Different locations

[hc-related-practice-areas]
*/ 



function hc_related_locations($atts = null) {

    global $post;

    $queryAmount = $amount;
    $locationCurrentPost = $post->ID;

    /*
    Get Current Location Taxonomy On Page
    */

    $postTerms =  wp_get_object_terms($post->ID, 'hc_location');
    $categoryFilterSlug = '';
    $categoryPrettyName = '';
    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
         foreach ( $postTerms as $term ) {
           $categoryFilterSlug .= '' . $term->slug;
           $categoryPrettyName .= ' ' . $term->name;
         }
    }

    //Widget Toggle
    if(($postTerms || 1) && strpos(get_the_title(), 'Personal Injury') === false) :


    //BEGIN FRONTEND OUTPUT
    ob_start();

?>


    <div class="location-widget-outer other-locations">
        <div class="location-widget-inner">

              <?php
              //Display Practice Area in Title
              $postTerms =  wp_get_object_terms($post->ID, 'hc_related');
              
              ?>
              <div class="location-widget-title" style="padding-top: 25px;">
                    <span class="location-widget-title__line-2" style="padding-bottom: 15px;">
                        Other
                    </span> 
                    <span class="location-widget-title__line-1">
                        Areas We Serve
                    </span>
                </div>
            
            <ul class="location-widget-links">

            <?php //BEGIN LOOP FOR MATCHING PAGES ?>
            <?php

                $tit = get_the_title();
                $pos = strpos(strtolower($tit), ' in ');
                $linkTitle = substr($tit, 0, $pos);              

                global $post;
               
                
                 $args = array(
                  'posts_per_page' => -1,
                  'post_type' => 'page',
                  'meta_query' => array(
                        array(
                            'key'     => '_hc_related_widget_title',
                            'value'   => get_field('_hc_related_widget_title', $post->ID),
                            'compare' => '=',
                        ),
                   ),
                  'post__not_in' => array($locationCurrentPost),
                  'meta_key'   => '_hc_related_widget_title',
                  'orderby' => 'rand',
                  'order' => 'ASC'


                );	
                        

            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();


            $city = get_the_terms($post->ID, 'hc_related');
            
            //Updating Title For Current Link
            if ( get_post_meta( $post->ID, '_hc_related_widget_title', true ) ){
                $linkTitle = get_post_meta( $post->ID, '_hc_related_widget_title', true );
            }

            //If Indianapolis Page with Children - Force Indianapolis to prevent things like "Car Accident Car Accident Lawyer"
            $post_slug = $post->post_name;
            //print_r($post);
            if ( strpos($linkTitle, $city[0]->name) !== false ){
                $linkTitle = $city[0]->name;
                $city[0]->name = "Indianapolis";
            }



            ?>

            <?php //BEGIN OUTPUT ?>
                <?php
                    
					
                
                ?>
                <li class="single-location-link">
                    <a href="<?php the_permalink(); ?>"><?=$city[0]->name .' '. $linkTitle . ' Lawyer';?></a>
                </li>
            <?php //END OUTPUT ?>

            <?php endwhile; else : ?>
            <style>.other-locations{display: none;}</style>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php //END LOOP FOR MATCHING PAGES ?>

            </ul>
        </div>
    </div>


<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
	
	endif;
}


add_shortcode('hc-related-practice-areas', 'hc_related_locations');