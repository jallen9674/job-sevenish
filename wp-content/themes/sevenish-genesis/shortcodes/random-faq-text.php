<?php

/*-------------------------------------
Display a list of random FAQs text only
[hc-random-faq-text amount="#"]
--------------------------------------*/

function hcRandomFAQText($atts = null) {

    global $post;

    extract(shortcode_atts(array(
      'amount' => '',
   ), $atts));

    $queryAmount = $amount;

    ob_start();
    //BEGIN OUTPUT
?>
<h2>Additional Frequently Asked Questions</h2>
<ul class="random-faq-loop-text">
        <?php $faqTerms = get_terms( 'faq-cat' );
        // convert array of term objects to array of term IDs
        $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

        $args = array(
          'posts_per_page' => $queryAmount,
          'post_type' => 'faq',
          'order' => 'DSC',
          'orderby' => 'rand',
        );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
      ?>
        <?php //Getting Category for Filtering
            $postTerms =  wp_get_object_terms($post->ID, 'hc_faqs');
            $categoryFilterSlug = '';
            $categoryPrettyName = '';
            if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                 foreach ( $postTerms as $term ) {
                   $categoryFilterSlug .= ' ' . $term->slug;
                   $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                 }
             }
         ?>

            <li><a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a></li>



          <?php endwhile; else : ?>
            <!-- IF NOTHING FOUND CONTENT HERE -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>


</ul> <!-- end .random-faq-loop -->

<?php
    //END OUTPUT (And actually output it!)
    $output = ob_get_contents();
    ob_end_clean();
    return  $output;
}


add_shortcode('hc-random-faq-text', 'hcRandomFAQText');

