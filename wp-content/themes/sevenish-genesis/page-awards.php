<?php
/**
 * Template Name: Awards 
 *  Note: Award markup copied from legacy theme
 */


//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//Adding Team Member
add_action( 'genesis_after_entry_content', 'hennessey_list_awards' );

// Runs the Genesis loop.
genesis();


function hennessey_list_awards(){
	?>

	<div class="awards-section">
		<?php
		if (have_rows('awards_memberships')):
		
			while (have_rows('awards_memberships')) : the_row();
				$award_img = get_sub_field('add_award_image');				
					?>
					<div class="single-award">
					
						<?php if ($award_img): ?>
							<div class="award-img-block">
								<img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
							</div>
						<?php endif; ?>
						<div class="award-main right-img-block <?php
								if ($award_img) {
									echo 'half-award';
								}
							?>">
							<?php the_sub_field('add_award_description'); ?>
						</div>
						
						
					</div>
				<?php
				
				
			endwhile; endif;
		?>
	</div>

	<?php 
}