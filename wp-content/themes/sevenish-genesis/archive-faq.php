<?php
/**
 * Template Name: FAQ Listing
 */

// Removes Skip Links.
remove_action( 'genesis_before_header', 'genesis_skip_links', 5 );

//Full Width Layou
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//Adding Body Class
add_filter( 'body_class', 'genesis_sample_landing_body_class' );
function genesis_sample_landing_body_class( $classes ) {
	$classes[] = 'faq-listing-page fullwidth-template';
	return $classes;
}

//Remove Entry Content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Remove Archive Loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//Adding FAQ Listing
add_action( 'genesis_before_content',  'hennessey_faq_listing');


// Runs the Genesis loop.
genesis();


/*------------------------------
FAQ Listing
--------------------------------*/

function hennessey_faq_listing(){
	?>
		<div class="faq-list-information">
			<p>
				Please select a category below to filter our frequently asked questions
			</p>
		</div>

        <div class="faq-filter-portfolio-wrapper">

            <div id="filters" class="faq-filter-category-list filters-button-group">
                <?php //Output all Categories
                echo '<button class="isotope-link is-checked" data-filter="*">All</button>';
                 $terms = get_terms( 'faq-cat');
                 if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){

                     foreach ( $terms as $term ) {
                       echo '<button class="isotope-link" data-filter=".' . $term->slug . '"><span>' . $term->name . '</span></button>';

                     }

                 }
                ?>
            </div>

            <div class="faq-filter-list">
                <?php
                    $faqTerms = get_terms( 'faq-cat' );
                    // convert array of term objects to array of term IDs
                    $faqTermIDs = wp_list_pluck( $faqTerms, 'term_id' );

                    $args = array(
                      'posts_per_page' => -1,
                      'post_type' => 'faq',
                      'tax_query' => array(
                            array(
                                'taxonomy' => 'faq-cat',
                                'field' => 'term_id',
                                'terms' => $faqTermIDs
                            ),
                        ),
                      'order' => 'DSC',
                    );

                    $the_query = new WP_Query( $args );
                    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                  ?>
                    <?php //Getting Category for Filtering
                         global $post;
                        $postTerms =  wp_get_object_terms($post->ID, 'faq-cat');
                        $categoryFilterSlug = '';
                        $categoryPrettyName = '';
                        if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                             foreach ( $postTerms as $term ) {
                               $categoryFilterSlug .= ' ' . $term->slug;
                               $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
                             }
                         }
                     ?>

                    <div class="faq-filter-listing  grid-item <?php echo $categoryFilterSlug; ?> ">

                        <a href="<?php the_permalink(); ?>" class="faq-filter-image-link">
                        <?php
                            if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                                the_post_thumbnail('faq-thumb', ['alt' => get_the_title()]);
                            } else{
                                echo '<img alt="faq thumb" src="' . get_stylesheet_directory_uri() . '/images/default-faq-thumb.jpg" />';
                            }
                        ?>
                        <span class="faq-date"><?php echo get_the_date('m-j-Y');  ?></span>
                        </a>
                       <div class="faq-meta">
                            <span class="faq-category"><?php echo $categoryPrettyName; ?></span>
                        </div>
                        <a href="<?php the_permalink(); ?>" class="faq-filter-title-link"><?php the_title(); ?></a>

                    </div>
                  <?php endwhile; else : ?>
                    <!-- IF NOTHING FOUND CONTENT HERE -->
                  <?php endif; ?>
                  <?php wp_reset_query(); ?>
            </div>

        </div>
   
	<?php 
}
