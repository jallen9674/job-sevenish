<?php 

/*----------------------------------
Global Header Content
-----------------------------------*/

function hennessey_global_header() {

    ?>
    <?/* Notification bar disabled
    <div class="site-header__notification-bar notification-bar">
        <div class="notification-bar__inner">
            <span class="notification-bar__left">
                Lorem Ipsum 
            </span>
            <span class="notification-bar__right">
                Dolor Sit 24/7
            </span>
        </div>
    </div> */ ?>

   <div class="site-header__inner desktop-header">

        <div class="desktop-header__logo">

            <a href="<?php echo site_url(); ?>">
<!--                <img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="Logo" class="desktop-header__logo--default"/> -->
				    <img src="https://www.sevenishlaw.com/wp-content/uploads/2020/01/SevenishInjuryLawyers-2-Blue-Horiz-1.png" alt="Logo" class="desktop-header__logo--default"/>
            </a>

        </div>

        <div class="desktop-header__right">
            <div class="desktop-header__avvo">
				<a href="https://www.thelawyersofdistinction.com/profile/randall-sevenish/" class="header_img"><img class="alignnone wp-image-10762 size-medium" src="https://www.lawyersofdistinction.com/wp-content/uploads/2019/11/800lod-1.png" alt="" width="243" height="300" /></a>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/avvo-top-rated-icon.png" alt="Avvo Top Rated" />
				<a target="_blank" href="https://www.respectedlawyers.com/in/car-accident-lawyer-in-indianapolis/#profile-id-585"><img src="/wp-content/uploads/2020/02/respectedlawyersbadge.png"/></a>
            </div>

            <div class="desktop-header__cta header-cta">
                <span class="header-cta__heading">
                    Call Today for a FREE Consultation
                </span>
                <a class="header-cta__phone" href="tel:<?php echo hennessey_phone_display(); ?>">
                    <?php echo hennessey_phone_display(); ?>
                </a>
                <span class="header-cta__subheading">
                    24 Hours | 7 Days a Week
                </span>
            </div>
        </div>      
        

   </div>

   <div class="desktop-header-navigation">
        <div class="desktop-header-navigation__inner wrapper">
            <nav role="navigation">
                <?php 
                    wp_nav_menu(
                        array(
                            'container' => false,                           
                            'container_class' => 'menu cf',                 
                            'menu' => 'Primary Navigation',  
                            'menu_class' => 'desktop-nav',               
                            'theme_location' => 'main-nav',                 
                            'before' => '',                                 
                            'after' => '',                                  
                            'link_before' => '',                            
                            'link_after' => '',                             
                            'depth' => 0,                                   
                            'fallback_cb' => ''                             
                        )
                    ); 
                ?>               
            </nav>
        </div>
    </div>




    <?php 
}