<?php
/**
 * Single Video Template
 */


//Remove Entry Content
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//Remove Post Meta & Info
remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


//Add Video Embed, JSON, Meta, & Description
add_action( 'genesis_before_entry_content',  'hennessey_single_video');

//Video Markup
function hennessey_single_video(){

	//Video Information via ACF
	global $post;

	//Video Information Via ACF
	$youtubeID 		= get_post_meta($post->ID, 'enter_youtube_id');
	$videoSchema 	= get_post_meta($post->ID, 'video_json_ld');

	//General Post Meta
	
	$postTerms =  wp_get_object_terms($post->ID, 'video-cat');
	$categoryFilterSlug = '';
	$categoryPrettyName = '';

	if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
		foreach ( $postTerms as $term ) {
		  $categoryFilterSlug .= ' ' . $term->slug;
		  $categoryPrettyName .= ' ' . $term->name . '<span class="divider">, </span>';
		}
	}

	$videoMeta = 'Posted on ' . get_the_date('F j, Y') . '<br>Categorized: ' . $categoryPrettyName;

	?>

	<div class="single-video-embed">
		<div class="single-video-embed__inner">

			<div class="single-video-embed__iframe-wrapper">
				<iframe  src="https://www.youtube.com/embed/<?php echo $youtubeID[0]; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<?php if ($videoSchema) {echo $videoSchema[0];} ?>

			<div class="single-video-embed__meta">
				<?php echo $videoMeta; ?>
			</div>
			<div class="single-video-embed__content">
				<?php the_content(); ?>
			</div>
		
		</div>	
	</div>

	<?php 
}


// Runs the Genesis loop.
genesis();
