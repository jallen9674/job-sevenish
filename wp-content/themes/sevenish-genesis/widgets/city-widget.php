<?php

// Creating the widget
class hc_city_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
        // Base ID of your widget
        'hc_city_widget',

        // Widget name will appear in UI
        __('HC Location List', 'hc_city_widget_domain'),

        // Widget description
        array( 'description' => __( 'Widget that displays a rotation of city & other regions.', 'hc_city_widget_domain' ), )
    );
    }


    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );
    // before and after widget arguments are defined by themes
    echo $args['before_widget'];
    echo '<div class="city-widget-box">';
/*----------------------- BEGIN OUTPUT --------------------------*/

    $titleArray = array(
        'Find yourself an <span>Experienced Lawyer Now!</span>',
        'Find yourself a <span>Competent Lawyer Now!</span>',
        'Find yourself a <span>Determined Lawyer Now!</span>',
        'Find yourself a <span>Diligent Lawyer Now!</span>',
        'Find yourself a <span>Friendly Lawyer Now!</span>',
        'Find yourself a <span>Passionate Lawyer Now!</span>',
        'Find yourself a <span>Reliable Lawyer Now!</span>',
        'Find yourself a <span>Persistent Lawyer Now!</span>',
    );
    echo $args['before_title'] . $titleArray[array_rand($titleArray)] . $args['after_title'];
?>



    <ul class="city-widget-list">
				

        <?php //Output a list of 10 random cities ?>

          <?php
             // get all terms in the taxonomy
            $terms = get_terms( 'hc_related' );
            $term_ids = wp_list_pluck( $terms, 'term_id' );
            

            global $post;
            remove_all_filters('posts_orderby');

            $args = array(
                    'post_type' => 'page',
                    'posts_per_page' => 10,
                    'orderby' => 'rand',
                    'order' => 'ASC',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'hc_related',
                            'field'    => 'term_id',
                            'terms'    => $term_ids,
                        ),
                    ),
                    'meta_query' => array(
                       array(
                           'key' => '_hc_related_widget_title',
                           'value' => 'Personal Injury',
                           'compare' => '=',
                       )
                   )

                );
              
            $query = new WP_Query($args);

            if($query->have_posts()):  while($query->have_posts()):$query->the_post();
                $locationCurrentPost = $post->ID;

                    /*
                    Get Current Location Taxonomy On Page
                    */

                    $postTerms =  wp_get_object_terms($post->ID, 'hc_related');

                    $categoryFilterSlug = '';
                    $categoryPrettyName = '';

                    if ( ! empty( $postTerms ) && ! is_wp_error( $postTerms ) ){
                         foreach ( $postTerms as $term ) {
                           $categoryFilterSlug .= '' . $term->slug;
                           $categoryPrettyName .= ' ' . $term->name;
                         }
                     } ?>
               <?php
               global $post;
               $url = get_permalink();
               /* Excludes Spanish Pages, thank you pages, constact page, and blog pages */
               //  if (!($post->post_parent == '1065') && (strpos($url, 'thank-you') == false) && !is_page(867) && is_home() && !is_single())
               if ( !($post->post_parent == '1065') && (strpos($url, 'thank-you') == false) )
               { ?>
                 <li>
                   <a href="<?php the_permalink(); ?>">
                      <?php echo $categoryPrettyName;?> Personal Injury Lawyer Near Me
                   </a>
                 </li>
               <?php } ?>
        <?php endwhile; ?>
        
        <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </ul>

</div> <?php //End .city-widget-box; ?>
</div>


<?php
/*----------------------- END OUTPUT --------------------------*/

}

// Widget Backend
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'New title', 'hc_city_widget_domain' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php
}

// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class hc_city_widget ends here

// Register and load the widget
function hc_load_city_widget() {
    register_widget( 'hc_city_widget' );
}
add_action( 'widgets_init', 'hc_load_city_widget' );