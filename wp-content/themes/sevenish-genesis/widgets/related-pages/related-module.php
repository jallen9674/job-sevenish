<?php
/***************************************************
** BRINGING EVERYTHING IN
** Note: CSS and JS Added With /js/scripts.js * /scss/partials/_related-widget.scss
***************************************************/

//Bringing in Taxonomy
require_once('related-taxonomy.php');

//Bringing in Metaboxes
require_once('related-metaboxes.php');

//Bringing in Widget
require_once('related-widget.php');