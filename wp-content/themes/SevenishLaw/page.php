<?php
/**
 * Theme Page file used to show Pages
 * @package      WordPress
 * @subpackage   Sevenish Law
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap text-justify"><?php /* Page main content section */ ?>
                    <h1><?php the_title(); ?></h1>
                    <?php
                    $video_thumbnail = get_field('page_video_thumbnail');
                    $youtube_video_id = get_field('youtube_video_id');
                    if ($video_thumbnail && $youtube_video_id) {
                        ?>
                        <div class="page-video-section alignright">
                            <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $video_thumbnail; ?>);">Play video</button>
                            </div>
                            <img src="<?php echo $video_thumbnail; ?>" itemprop="thumbnailURL" class="hide" alt="John Fisher"/>
                            <a class="hide" target="_blank" itemprop="embedURL" href="https://youtu.be/<?php echo get_field('youtube_video_id'); ?>">Watch the Video</a>
                        </div>
                    <?php }
                    ?>
                    
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <?php
                    if (is_page('360')):
                        dynamic_sidebar('attorneys-sidebar');
                    else:
                        get_sidebar();
                    endif;
                    ?>
                </div>  
            </div>
        </div>
    </div>
    <?php if (is_page('360')): ?>
        <div id="attorneys-sec" class="attorney-listing single-section white-text bg-cover">
            <div class="container">
                <div class="row">
                    <?php
                    if (have_rows('attorneys_section', 387)):
                        while (have_rows('attorneys_section', 387)) : the_row();
                            $postObj = get_sub_field('attorney_page_link', 387);
                            $pageLink = get_permalink($postObj);
                            ?>
                            <div class="single-attorney attorney-list-block">
                                <?php $attor_img = get_sub_field('add_attorney_image', 387); ?>
                                <div class="attorney-img-block bg-cover" style="background-image: url('<?php echo $attor_img['url']; ?>')">
                                    <a href="<?php echo $pageLink ?>">
                                        <!--<img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>-->
                                    </a>
                                </div>
                                <div class="attorney-list-content">
                                    <span class="attor-title"><a href="<?php echo $pageLink ?>" title="<?php the_sub_field('add_attorney_name', 387); ?>"><?php the_sub_field('add_attorney_name', 387); ?></a></span>
                                    <span class="attor-designation"><?php the_sub_field('add_attorney_designation', 387); ?></span>
                                    <?php $attor_desc = get_sub_field('attorney_description', 387); ?>
                                    <p><?php echo wp_trim_words($attor_desc, 32); ?></p>
                                    <a class="btn" href="<?php echo $pageLink ?>" title="Read More"> Read More</a>
                                </div>
                            </div>
                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
    <?php endif;
    ?>
</div>
<?php
get_footer();

