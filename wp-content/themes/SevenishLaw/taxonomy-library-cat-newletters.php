<?php
/**
 * Newsletter Term - Library Category file used to show posts
 * @package      WordPress
 * @subpackage   Sevenish Law
 */
get_header();
$term_taxonomy = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
$termname = $term_taxonomy->name;
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap blog-page-wrap"><?php /* Page main content section */ ?>
                    <h1>Newsletters  <?php if($paged) {echo '- Page '.$paged;} ?></h1>
                    <?php if (have_posts()) : $j = 1; ?>
                        <div class="post-listing fullwidth">
                            <?php
                            while (have_posts()) : the_post();
                                ?>
                                <div class="blog-listing blog-listing-<?php echo $j; ?>">
                                    <a href="<?php
                                    if (get_field('select_newsletter_pdf')) {
                                        the_field('select_newsletter_pdf');
                                    } else {
                                        the_permalink();
                                    }
                                    ?>" class="blog-featured bg-cover <?php
                                       if (has_post_thumbnail()) {
                                           echo 'featured-bg';
                                       }
                                       ?>">
                                    </a>
                                    <div class="blog-content half">
                                        <h3><a title="<?php the_title(); ?>" href="<?php
                                            if (get_field('select_newsletter_pdf')) {
                                                the_field('select_newsletter_pdf');
                                            } else {
                                                the_permalink();
                                            }
                                            ?>"><?php the_title(); ?></a></h3>
                                        <span class="date">
                                            <?php
                                            $terms = get_the_terms(get_the_ID(), 'library-cat');
                                            if ($terms && !is_wp_error($terms)) :
                                                $category_links = array();
                                                foreach ($terms as $term) {
                                                    $category_links[] = '<a href="/library-cat/' . $term->slug . '/">' . $term->name . '</a>';
                                                }
                                                $category_link = join(", ", $category_links);
                                                ?>
                                                <?php
                                            endif;
                                            echo $category_link;
                                            ?>
                                        </span>
                                        <p> 
                                            <?php
                                            echo wp_trim_words(get_the_excerpt(), 20, '...');
                                            ?>
                                        </p>
                                        <a class="read-more" href="<?php
                                        if (get_field('select_newsletter_pdf')) {
                                            the_field('select_newsletter_pdf');
                                        } else {
                                            the_permalink();
                                        }
                                        ?>" title="<?php the_title(); ?>">Read More</a>
                                    </div>
                                    <hr/>
                                </div>
                                <?php
                                $j++;
                            endwhile;
                            numeric_posts_nav();
                            wp_reset_query();
                            ?>
                        </div>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <div class="widget widget_categories">
                        <h3 class="widgettitle">Recent Newsletters</h3>
                        <ul>
                            <?php
                            $args = array(
                                'post_type' => 'library',
                                'posts_per_page' => '5',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'library-cat',
                                        'field' => 'slug',
                                        'terms' => 'newletters',
                                    ),
                                ),
                            );
                            $parent = new WP_Query($args);
                            if ($parent->have_posts()) :
                                ?>
                                <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                                    <?php
                                endwhile;
                                unset($parent);
                            endif;
                            wp_reset_query();
                            ?>
                        </ul>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
