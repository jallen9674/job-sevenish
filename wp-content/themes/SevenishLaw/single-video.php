<?php
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap text-justify"><?php /* Page main content section */ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php if (get_field('enter_youtube_id')): ?>
                                <div class="featured-div video-cat-div" style="margin-bottom: 40px;">
                                    <p>Category: <?php the_terms($post->ID, 'video-cat'); ?></p>
                                </div>
                                <div class="featured-div hc-youtube-wrapper">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?=get_field('enter_youtube_id')?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <?php if (get_field('video_json_ld')) { echo get_field('video_json_ld'); } ?>
                                </div>
                                <div class="featured-div">

                                    <?php the_content(); ?>
                                </div>
                            <?php endif;
                            ?>

                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <div class="widget widget_categories">
                        <h3 class="widgettitle">Categories</h3>
                        <ul>
                            <?php
                            $taxonomy = array(
                                "video-cat",
                            );
                            $args = array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                // 'parent' => 0,
                                'hierarchical' => true,
                                // 'child_of' => 0,
                                'childless' => false,
                            );
                            $terms = get_terms($taxonomy, $args);
                            foreach ($terms as $term) {
                                ?>
                                <li class="cat-item"><a href="/videos/category/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a></li>
                                <?php }
                                ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
