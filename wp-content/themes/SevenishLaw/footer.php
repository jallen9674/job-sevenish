<?php
/**
 * Theme footer file
 * @package      WordPress
 * @subpackage   Sevenish Law
 */
?>
<?php get_template_part('site', 'schema'); ?>
</main>
<?php /* Footer starts here */ ?>
<footer id="main-footer" class="fullwidth white-text">
    <div id="footer-top" class="fullwidth bg-cover">
        <div class="container">
            <div class="row">
                <div id="footer-col-left" class="col-sm-4 col-xs-12">
                    <?php if (is_front_page()) {
                        ?>
                        <img src="/wp-content/uploads/2017/12/footer-logo.png" alt="<?php bloginfo('name') ?>"/>
                    <?php } else { ?>
                        <a href="<?php bloginfo('url'); ?>">
                            <img src="/wp-content/uploads/2017/12/footer-logo.png" alt="<?php bloginfo('name') ?>" />
                        </a>
                    <?php }
                    ?>
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Left Column')) : endif; ?>
                </div>
                <div id="footer-middle" class="col-sm-4 col-xs-12">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Middle Column')) : endif; ?>
                </div>
                <div id="footer-right" class="col-sm-4 col-xs-12">
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Right Column')) : endif; ?>

                </div>
            </div>
        </div>
    </div>
    <div id="footer-bottom" class="fullwidth text-center">
        <div class="container">
            <div class="row">
                 <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Copyright Column')) : endif; ?>
                  <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer Bottom Column')) : endif; ?>
             </div>
        </div>
    </div>
    <script>
        (function () {
            var css = document.createElement('link');
            css.href = '//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css';
            css.rel = 'stylesheet';
            css.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(css);
            var fonts = document.createElement('link');
            fonts.href = 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,700i,900';
            fonts.rel = 'stylesheet';
            fonts.type = 'text/css';
            document.getElementsByTagName('head')[0].appendChild(fonts);
        })();
    </script>
    <!-- Start Of NGage -->
    <div id="nGageLH" style="visibility:hidden; display: block; padding: 0; position: fixed; right: 0px; bottom: 0px; z-index: 5000;"></div>
    <script src="https://messenger.ngageics.com/ilnksrvr.aspx?websiteid=173-8-154-196-139-3-64-63" async="async">
    </script>
    <!-- End Of NGage -->
	<!-- CallRail -->
	<script type="text/javascript" src="//cdn.callrail.com/companies/656659615/90e1d4a90c69f29ee4c5/12/swap.js"></script> 
	
</footer>
<?php /* End Footer */ ?>
<?php wp_footer(); ?>
</body>
</html>
