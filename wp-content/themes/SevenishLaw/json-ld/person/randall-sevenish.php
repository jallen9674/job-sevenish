<?php
function addJSONLD664()
{
    global $post;

    if ($post->ID == 664) {
        echo '<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Indianapolis",
        "addressRegion": "IN",
        "postalCode": "46204",
        "streetAddress": "101 W Ohio St #1540"
      },
      "colleague": [
    "https://www.sevenishlaw.com/attorneys/regina-sevenish/",
    "https://www.sevenishlaw.com/attorneys/shannon-majors/",
    "https://www.sevenishlaw.com/attorneys/matthew-forsythe/"       
      ],
      "email": "rs@sevenishlaw.com",
      "image": "https://www.sevenishlaw.com/wp-content/uploads/2017/11/img-min.png",
      "jobTitle": "Attorney",
      "name": "Randall Sevenish",
      "alumniOf": "University of Indiana School of Law",
      "gender": "male",
      "nationality": "USA",
      "telephone": "(317) 636-7777",
      "url": "https://www.sevenishlaw.com/attorneys/randall-sevenish/",
      "sameAs" : [
        "https://www.linkedin.com/in/randall-sevenish-84b2271a/",
        "https://www.facebook.com/randy.sevenish"
      ]
    }
    </script>';
    }
}

add_action('wpseo_json_ld', 'addJSONLD664', 21);
