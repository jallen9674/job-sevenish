<?php
function addJSONLD693()
{
    global $post;

    if ($post->ID == 693) {
        echo '<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Indianapolis",
        "addressRegion": "IN",
        "postalCode": "46204",
        "streetAddress": "101 W Ohio St #1540"
      },
      "colleague": [
    "https://www.sevenishlaw.com/attorneys/randall-sevenish/",
    "https://www.sevenishlaw.com/attorneys/shannon-majors/",
    "https://www.sevenishlaw.com/attorneys/matthew-forsythe/"       
      ],
      "email": "regina@sevenishlaw.com",
      "image": "https://www.sevenishlaw.com/wp-content/uploads/2017/11/regina-sevenish-211x300.jpg",
      "jobTitle": "Marketing",
      "name": "Regina Sevenish",
      "alumniOf": "Indiana State University",
      "gender": "female",
      "nationality": "USA",
      "url": "https://www.sevenishlaw.com/attorneys/regina-sevenish/",
      "sameAs" : [
        "https://www.facebook.com/regina.sevenish",
        "https://www.linkedin.com/in/regina-sevenish-20578711/"
      ]
    }
    </script>';
    }
}

add_action('wpseo_json_ld', 'addJSONLD693', 21);
