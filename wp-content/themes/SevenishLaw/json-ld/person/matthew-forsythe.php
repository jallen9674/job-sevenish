<?php
function addJSONLD686()
{
    global $post;

    if ($post->ID == 686) {
        echo '<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Indianapolis",
        "addressRegion": "IN",
        "postalCode": "46204",
        "streetAddress": "101 W Ohio St #1540"
      },
      "colleague": [
    "https://www.sevenishlaw.com/attorneys/randall-sevenish/",
    "https://www.sevenishlaw.com/attorneys/regina-sevenish/",
    "https://www.sevenishlaw.com/attorneys/shannon-majors/"      
      ],
       "email": "Matthew.Forsythe@sevenishlaw.com",
      "image": "https://www.sevenishlaw.com/wp-content/uploads/2017/11/img-5-min.png",
      "jobTitle": "Attorney",
      "name": "Matthew Forsythe",
      "alumniOf": "Salmon P. Chase College of Law",
      "gender": "male",
      "nationality": "USA",
      "url": "https://www.sevenishlaw.com/attorneys/matthew-forsythe/",
      "sameAs" : [
        "https://www.linkedin.com/in/matthew-forsythe-20543841/"
      ]
    }
    </script>';
    }
}

add_action('wpseo_json_ld', 'addJSONLD686', 21);
