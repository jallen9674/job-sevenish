<?php
function addJSONLD688()
{
    global $post;

    if ($post->ID == 688) {
        echo '<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Person",
      "address": {
        "@type": "PostalAddress",
        "addressLocality": "Indianapolis",
        "addressRegion": "IN",
        "postalCode": "46204",
        "streetAddress": "101 W Ohio St #1540"
      },
      "colleague": [
    "https://www.sevenishlaw.com/attorneys/regina-sevenish/",
    "https://www.sevenishlaw.com/attorneys/randall-sevenish/",
    "https://www.sevenishlaw.com/attorneys/matthew-forsythe/"       
      ],
      "email": "shannon@sevenishlaw.com",
      "image": "https://www.sevenishlaw.com/wp-content/uploads/2017/11/img-4-min.png",
      "jobTitle": "Office Manager and Paralegal",
      "name": "Shannon Majors",
      "alumniOf": "Indiana University",
      "gender": "female",
      "nationality": "USA",
      "url": "https://www.sevenishlaw.com/attorneys/shannon-majors/",
      "sameAs" : [
        "https://www.linkedin.com/in/shannon-majors-216a8082/"
      ]
    }
    </script>';
    }
}

add_action('wpseo_json_ld', 'addJSONLD688', 21);
