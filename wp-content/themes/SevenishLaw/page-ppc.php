<?php
/**
 * Template Name: PPC Landing
 */
get_header();
?>
<div class="fullwidth page-content">

    <?php /*    <div id="page-banner" class="bg-cover">
      <div class="banner-content white-text text-center">
      <div class="container">
      <h2><?php echo get_the_title(362); ?></h2>
      </div>
      </div>
      <?php
      if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('
      <div id="crumbs"><div class="container">', '</div></div>');
      }
      ?>
      </div> */ ?>


    <div id="home-banner">
        <div id="banner-video"> <?php /* Banner video */ ?>
            <video autoplay loop muted>
                <?php if (get_field('banner_video_mp4')): ?>
                    <source src="<?php the_field('banner_video_mp4'); ?>" type="video/mp4">
                    <?php
                endif;
                if (get_field('banner_video_ogg')):
                    ?>
                    <source src="<?php the_field('banner_video_ogg'); ?>" type="video/ogg">
                    <?php
                endif;
                if (get_field('banner_video_webm')):
                    ?>
                    <source src="<?php the_field('banner_video_webm'); ?>" type="video/webm">
                <?php endif;
                ?>
            </video>
            <a class="video-mute"><span class="fa fa-volume-off" aria-hidden="true"></span></a>

        </div>
        <div class="banner fullwidth">
            <div class="container">
                <div class="home-banner-text col-sm-7 col-xs-12">
                    <div class="home-banner-title">
                        <h1><?php
                            if (get_field('home_banner_title')) {
                                the_field('home_banner_title');
                            }
                            ?></h1>
                    </div>
                    <div class="home_content">
                        <?php
                        if (get_field('home_content')) {
                            the_field('home_content');
                        }
                        ?>
                    </div>
                    <div class="home-video-thumb">
                        <?php if (get_field('popup_banner_video')): ?>
                            <!--<a href="https://youtu.be/<?php // echo get_field('popup_banner_video'); ?>" class="video-home html5lightbox"></a>-->
                            <a href="https://youtu.be/<?php echo get_field('popup_banner_video'); ?>" class="video-home">	    
                                <span class="fa fa-play-circle" aria-hidden="true"></span>
                            </a>
                        <?php endif;
                        ?>
                    </div>
                </div>
                <div id="home-banner-form" class="col-sm-5 col-xs-12">
                    <?php
                    echo do_shortcode('[contact-form-7 id="1957" title="PPC: Accident Form"]');
                    ?>
                </div>
            </div>
        </div>
        <div id="awards-sec" class="award-slider-section">
            <div class="container">
                <div class="owl-carousel owl-theme">
                    <?php
                    if (have_rows('awards_section')):
                        while (have_rows('awards_section')) : the_row();
                            $award_img = get_sub_field('add_award');
                            ?>
                            <div class="item text-center">
                                <img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
                            </div>
                            <?php
                            the_sub_field('sub_field_name');
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-12 col-xs-12 page-wrap text-center"><?php /* Page main content section */ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <!--                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                <?php
                /* echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form"]'); */

                //echo do_shortcode('[lexicata-contact-form]');
                ?>
                                </div>  -->
            </div>
        </div>
    </div>


<!-- CLAIMS PROCESS MODULE -->
    <div id="tabs-sec" class="single-section white-text text-center">
            <div class="container">
                <h2>Vehicle Accident Claims Process</h2>
            </div>
            <div class="tabs-block text-center">
                <ul class="nav nav-tabs">
                    <?php
                    if (have_rows('tabs_section')):
                        $i = 1;
                        while (have_rows('tabs_section')) : the_row();
                            ?>
                            <li class="<?php
                            if ($i == 1) {
                                echo 'active';
                            }
                            ?>">
                                <a data-toggle="tab" href="#tab-content-<?php echo $i; ?>">
                                    <span class="tab-number"><?php echo $i; ?></span>
                                    <span class="tab-title"><?php the_sub_field('tab_title'); ?></span>
                                </a>
                            </li>
                            <?php
                            $i++;
                        endwhile;
                    else :
                    endif;
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                    if (have_rows('tabs_section')):
                        $i = 1;
                        while (have_rows('tabs_section')) : the_row();
                            ?>
                            <div id="tab-content-<?php echo $i; ?>" class="single-tab bg-cover tab-pane fade <?php
                            if ($i == 1) {
                                echo 'in active';
                            }
                            ?>">
                                <div class="col-sm-6 col-sm-offset-6 text-justify tab-main">
                                    <div class="tab-main-content">
                                        <?php the_sub_field('tab_content'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>

                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>

<!-- END CLAIMS PROCESS MODULE -->

<!-- START TWO COLUMN CONTENT LAYOUT -->        
        <div id="below-tabs" class="single-section text-justify">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <?php
                        $content_col = get_field('below_tabs_section_left_col_content');
                        echo str_replace('rel="noopener"', ' ', $content_col);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        $content_col = get_field('below_tabs_section_right_col_content');
                        echo str_replace('rel="noopener"', ' ', $content_col);
                        ?>
                    </div>
                </div>
            </div>
        </div>
<!-- END TWO COLUMN CONTENT LAYOUT -->

<!-- START VIDEO RESOURCES -->
    <div id="below-practice-areas" class="single-section text-center">
        <div class="container">
            <?php the_field('below_practice_areas_section'); ?>
        </div></div>
<!-- END VIDEO RESOURCES -->

<!-- START CLIENT TESTIMONIALS SECTION -->
        <div id="testimonials-sec" class="single-section text-center bg-cover">
            <div class="container">
                            <?php the_field('client_testimonials_section_content'); ?>

<!-- VIDEO TESTIMONIALS -->
            <div class="testimonialvideo nopadding row">
                <?php
                $videotestimonials = [1856, 1849];
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'post__in' => $videotestimonials
                        )
                );
                ?>
                <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                    <?php
                    if (get_field('testimonial_video')):
                        if (get_field('video_thumbnail')) {
                            $thumb = get_field('video_thumbnail');
                        } else {
                            $thumb = '/wp-content/uploads/2017/11/mqdefault-83.jpg';
                        }
                        ?>
                        <div class="col-sm-6 single-video-block-wrap">
                            <div class="single-video-block " style="background-color:#ece5d3;">
                                <div class="video-wrap hidden-xs">
                                    <a class="fancybox-youtube single-video" rel="group" href="https://youtu.be/<?php the_field('testimonial_video'); ?>">
                                        <img src="<?php echo $thumb; ?>">
                                    </a>
                                </div>
                                <div class="visible-xs mobile-videos">
                                    <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                        <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php the_field('testimonial_video'); ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                        <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $thumb; ?>);"></button>
                                    </div>
                                </div>
                                <div class="below-video-content"><div class="testimonials-title"><?php
                                        if (get_field('video_title')) {
                                            the_field('video_title');
                                        } else {
                                            the_title();
                                        }
                                        ?></div>
                                    <?php if (get_field('below_video_quote')) { ?>
                                        <div class="testimonial-quote">
                                            <?php the_field('below_video_quote'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (get_field('video_testimonial_client_info')) { ?>
                                        <div class="testimonial-client-info">
                                            - <?php the_field('video_testimonial_client_info'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
<!-- END VIDEO TESTIMONIALS -->
<!-- START STATIC TESTIMONIALS  -->
            <div class="row">
                <?php
                $statictestimonials = [1698, 1777, 1689];
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'post__in' => $statictestimonials
                        )
                );
                ?>

                <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                    <?php if (get_field('written_testimonial_quote')) : ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote" style="color:#000;">
                                    <?php the_field('written_testimonial_quote'); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_field('written_testimonial_client_info'); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    elseif (get_the_content()) :
                        ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote" style="color:#000;">
                                    <?php the_content(); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_title(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                    ?>

                    <?php
                endwhile;
                wp_reset_query();
                ?>

            </div>
        </div>
    </div>
<!-- END CLIENT TESTIMONIALS SECTION -->

<!-- START ATTORNEYS LAYOUT -->
        <div id="attorneys-sec" class="single-section text-center white-text bg-cover">
            <div class="container">
                <div class="row">
                    <?php the_field('attorneys_section_content'); ?>
                    <?php
                    if (have_rows('attorneys_section')):
                        while (have_rows('attorneys_section')) : the_row();
                            $postObj = get_sub_field('attorney_page_link');
                            $pageLink = get_permalink($postObj);
                            ?>
                            <div class="single-attorney">
                                <?php $attor_img = get_sub_field('add_attorney_image'); ?>
                                <div class="attorney-img-block">
                                    <a href="<?php echo $pageLink ?>">
                                        <img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>
                                    </a>
                                </div>
                                <span class="attor-designation"><?php the_sub_field('add_attorney_designation'); ?></span>
                                <span class="attor-title"><a href="<?php echo $pageLink ?>"><?php the_sub_field('add_attorney_name'); ?></a></span>
                            </div>
                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
<!-- END ATTORNEYS LAYOUT -->


<!-- START PRACTICE AREA TILES -->
    <div id="practice-areas" class="single-section white-text text-center Practice-Area-Main bg-cover" style="background-image:url(http://sevenishlaw.com/wp-content/uploads/2018/07/headon-collision.jpg);"> 
        <div class="container">
            <?php the_field('practice_areas_content_section'); ?>
        </div>
        <div class="practice-list col-xs-12 nopadding practice-inner">
            <?php
            if (have_rows('practice_areas_sec')):
                $i = 1;
                while (have_rows('practice_areas_sec')): the_row();
                    $postObj = get_sub_field('select_practice_area_page');
                    $postId = $postObj->ID;
                    $PracticeBg = get_field('practice_area_image', $postId);
                    $PracticeIcon = get_field('practice_area_icon', $postId);
                    $pageLink = get_permalink($postId);
                    $postTitle = get_field('practice_area_title', $postId);
                    ?>
                    <a class="practice-<?php echo $i; ?>" href="<?php echo $pageLink ?>" title="<?php echo $postTitle; ?>">
                        <div class="bg-image bg-cover"></div>
                        <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 area-box">
                            <div class="col-xs-12 nopadding area-inner-box">
                                <div class="col-xs-12 nopadding main-box">
                                    <span class="practice-detail-title"> <?php echo $postTitle; ?></span>
                                </div>
                                <div class="overlay-box">
                                    <div>
                                        <?php if ($PracticeIcon): ?>
                                            <img src="<?php echo $PracticeIcon['url']; ?>" alt="<?php echo $PracticeIcon['alt']; ?>"/>
                                        <?php endif;
                                        ?>
                                        <span class="practice-detail-title"><?php echo $postTitle ?></span>
                                        <img src="<?php bloginfo('template_url'); ?>/img/practice-arrow.png" alt="Practice Link"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php
                    $i++;
                endwhile;
            endif;
            wp_reset_query();
            ?>
        </div>
    </div>
<!-- END PRACTICE AREA TILES -->

<!-- START FREE BOOKS -->
<div id="free-books-sec" class="single-section text-center bg-cover">
            <div class="container">
                <?php the_field('free_books_content_section'); ?>
                <?php
                if (have_rows('add_books')):
                    while (have_rows('add_books')) : the_row();
                        $BookObj = get_sub_field('add_book_link');
                        $BookLink = get_permalink($BookObj);
                        $book_img = get_sub_field('add_book_image');
                        ?>
                        <div class="single-book">
                            <a href="<?php echo $BookLink ?>">
                                <?php if ($book_img): ?>
                                    <img src="<?php echo $book_img['url']; ?>" alt="<?php echo $book_img['alt']; ?>"/> 
                                <?php else:
                                    ?>
                                    <span class="fa fa-book"></span>
                                <?php endif;
                                ?>
                            </a>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
<!-- END FREE BOOKS -->


    </div>
</div>
<?php
get_footer();

