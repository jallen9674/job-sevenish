<?php
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
<?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap text-justify"><?php /* Page main content section */ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h1><?php the_title(); ?></h1> 
                            <?php if (has_post_thumbnail()): ?>
                                <div class="featured-div alignleft">
                                    <?php
                                    the_post_thumbnail();
                                    ?>
                                </div>
                            <?php endif;
                            ?>

                            <?php the_content(); ?>
                           
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <?php if (is_singular('case-result')) { ?>
                        <div class="widget widget_categories">
                            <h3 class="widgettitle">Categories</h3>
                            <ul>
                                <?php
                                $taxonomy = array(
                                    "case-cat",
                                );
                                $args = array(
                                    'orderby' => 'name',
                                    'order' => 'ASC',
                                    'hierarchical' => true,
                                    'childless' => false,
                                );
                                $terms = get_terms($taxonomy, $args);
                                foreach ($terms as $term) {
                                    ?>
                                    <li class="cat-item <?php
                            if ($termname == $term->name) {
                                echo 'current-cat';
                            }
                                    ?>"><a href="/case-results-category/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a></li>
                                    <?php }
                                    ?>
                            </ul>
                        </div>
                    <?php
                    } else {
                        if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')) : endif;
                    }
                    ?>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>