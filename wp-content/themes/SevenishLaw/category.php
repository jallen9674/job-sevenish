<?php
/**
 * Theme Category file used to show posts
 * @package      WordPress
 * @subpackage   John H.Fisher
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
<?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap blog-page-wrap"><?php /* Page main content section */ ?>
                       <h1><?php the_archive_title(); ?> <?php if($paged) {echo '- Page '.$paged;} ?></h1>
                    <?php if (have_posts()) : $i = 1; ?>
                        <div class="post-listing fullwidth">
                            <?php
                            while (have_posts()) : the_post();
                                $post_categories = wp_get_post_categories(get_the_ID());
                                $cats = array();
                                foreach ($post_categories as $c) {
                                    $cat = get_category($c);
                                    $cats[] = '<a href="/category/' . $cat->slug . '/">' . $cat->name . '</a>';
                                }
                                $categories_list = join(', ', $cats);
                                ?>
                                <div class="blog-listing blog-listing-<?php echo $i; ?>">
                                    <a href="<?php the_permalink(); ?>" class="blog-featured bg-cover <?php if(has_post_thumbnail()){ echo 'featured-bg';}?>">
                                    </a>
                                    <div class="blog-content half">
                                        <h3> <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <span class="date"><?php echo $categories_list; ?></span>
                                        <p> 
                                            <?php
                                            echo wp_trim_words(get_the_excerpt(), 20, '...');
                                            ?>
                                        </p>
                                        <a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More</a>
                                    </div>
                                    <hr/>
                                </div>
                                <?php
                                $i++;
                            endwhile;
                            numeric_posts_nav();
                            wp_reset_query();
                            ?>
                        </div>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Blog Sidebar')) : endif; ?>
                </div> 
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

