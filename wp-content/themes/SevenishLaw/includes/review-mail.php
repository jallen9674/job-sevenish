<?php

$name = $_POST['fname'];
$lname = $_POST['lname'];
//$email = $_POST['email'];
//$phone = $_POST['phone'];
$review = $_POST['caseinfo'];
$star = $_POST['ratingstar'];
$message = '';
$message .= '<!DOCTYPE html PUBLIC -//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta http-equiv="x-ua-compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Sevenish Law Group</title>
    </head>
    <body>
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" id="bodyTable" style="height:100%;max-width:800px;">
            <tr>
                <td align="center" valign="top" id="bodyCell">
                    <table border="0" cellpadding="0" cellspacing="0" width="600" class="templateContainer">
                        <tr>
                            <td valign="top" align="center" id="templateHeader" style=" background:#e5dec2; padding: 20px 0; text-align: center;">
                                <img src="http://sevenishlaw.staging.wpengine.com/wp-content/themes/SevenishLaw/img/header-logo.png" width="342" height="78" alt="" border="0" style="margin: 0 auto; display: block; padding: 0; text-align: center;" mc:edit="header_image"/>
                            </td> 
                        </tr>
                        <tr>
                            <td valign="top" id="templateBody2" align="left" style="text-align:center;padding:30px 20px; border:1px solid #ccc">
                              <h2 stye="margin-bottom:0px;">Client Review
                              </h2>
                              <table width="100%" style="margin-bottom:20px; background:#ccc; ">
                                <tr>
                                        <td width="50%" style="background:#fff"><strong>Name</strong></td> 
                                        <td style="background:#fff" width="50%">' . $name . ' ' . $lname . '</td>      
                                </tr>
                                  <tr>
                                        <td width="50%" style="background:#fff"><strong>Review Star</strong></td> 
                                        <td style="background:#fff" width="50%">' . $star . '</td>      
                                </tr>
                                   <tr>
                                        <td width="50%" style="background:#fff"><strong>Review</strong></td> 
                                        <td style="background:#fff" width="50%">' . $review . '</td>      
                                </tr>

                              </table>
                           </td>
                        </tr>
  
                        <tr>
                            <td valign="top" id="templateFooter" style="background:#25170e !important; text-align: center">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="background:#25170e !important;">
                                    <tr>
                                        <td valign="top" id="footerContent">
                                            <div mc:edit="footer_content" style="background:#25170e !important; text-align: center">
                                                <p style="color:#fff; text-align: center">Copyright &copy; Sevenish Law Group. All Rights Reserved.</p>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>';
$to = "Brenton@blusharkdigital.com,rs@sevenishlaw.com, info@sevenishlaw.com";
$subject = "Sevenish Law Client Review Submission";
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
$headers .= "From: Sevenish Law Group <wordpress@sevenishlaw.com>" . "\r\n";
mail($to, $subject, $message, $headers);
