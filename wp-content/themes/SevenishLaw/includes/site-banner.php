<?php /*    <div id="page-banner" class="bg-cover">
        <div class="banner-content white-text text-center">
            <div class="container">
                <h2><?php echo get_the_title(362); ?></h2>
            </div>
        </div>
        <?php
    if (function_exists('yoast_breadcrumb')) {
        yoast_breadcrumb('
<div id="crumbs"><div class="container">', '</div></div>');
    }
    ?>
    </div> */ ?>
    
        <div id="page-banner" class="bg-cover">
        <div class="banner-content white-text">
            <div class="container">
                <div class="col-xs-12 col-sm-6 text-left">
                     <h2>A Lifetime Of Service</h2>
                    <h3>Fierce Protectors of the Injured<span>™</span></h3>
                    <p>Police Captain, SWAT Team Founder & Commander, Karate Sensei & Injury Lawyer</p>
                    
                </div>

            </div>
        </div>
        <?php 
        if (function_exists('yoast_breadcrumb')) { ?>        
            <div id="crumbs"><div class="container">
                    <?php echo do_shortcode('[wpseo_breadcrumb]'); ?>  
                </div></div>
        <?php } ?>
    </div>
