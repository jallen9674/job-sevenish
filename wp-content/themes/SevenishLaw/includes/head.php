<!DOCTYPE html>
<html class="site-html" xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P8LJQZT');</script>
<!-- End Google Tag Manager -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <?php if (is_home() || is_front_page()) { ?>
            <title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
        <?php } else { ?>
            <title><?php wp_title("", true); ?> | <?php bloginfo('name'); ?></title>
        <?php }
        ?>
        <link rel="shortcut icon" href="<?php echo bloginfo('template_url') ?>/img/favicon.ico" />
        <?php wp_enqueue_script('jquery'); ?>
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <?php
        global $post;
        if (get_field('common_banner_image', 'options')) {
            $featured_image = get_field('common_banner_image', 'options');
        } 
        /*elseif (get_post_thumbnail_id(get_the_ID())) {
            $backgroundimage = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
            $featured_image = $backgroundimage;
        } */ else {
            $featured_image = get_bloginfo('template_url') . '/img/new-default-banner.jpg';
        }
        ?>
        <style>
            #page-banner{
                background-image:url('<?php echo $featured_image; ?>');
            }

<?php
if (have_rows('practice_areas_sec')) {
    $i = 1;
    while (have_rows('practice_areas_sec')): the_row();
        $postObj = get_sub_field('select_practice_area_page');
        $postId = $postObj->ID;
        $PracticeBg = get_field('practice_area_image', $postId);
        $pageLink = get_permalink($postId);
        $postTitle = get_field('practice_area_title', $postId);
        ?>
                    #practice-areas .practice-<?php echo $i; ?>{
                        background-image:url('<?php echo $PracticeBg; ?>');
                    }
                    #practice-areas .practice-<?php echo $i; ?> .bg-image{
                        background-image:url('<?php echo $PracticeBg; ?>');
                    }
        <?php
        $i++;
    endwhile;
}
?>
            <?php
            if (have_rows('tabs_section')):
                $i = 1;
                while (have_rows('tabs_section')) : the_row();
                    $tab_bg = get_sub_field('tab_background_image');
                    ?>
                    .single-tab#tab-content-<?php echo $i; ?>{
                        background-image:url('<?php echo $tab_bg; ?>');
                    }
        <?php
        $i++;
    endwhile;
else :
endif;
?>
            <?php
            if (!is_page()) {
                if (have_posts()) : $i = 1;
                    while (have_posts()) : the_post();
                        $backgroundimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                        if (has_post_thumbnail()) {
                            ?>
                            .blog-listing-<?php echo $i; ?> .blog-featured{
                                background-image:url('<?php echo $backgroundimage; ?>');
                            }
                <?php
            }
            $i++;
        endwhile;
    endif;
}
?>
            <?php
            $i = 1;
            global $post_type;
            if (is_page('784')) {
                $post_type = 'faq';
            } elseif (is_page('792')) {
                $post_type = 'library';
            } elseif (is_page('361')) {
                $post_type = 'case-result';
            } elseif (is_page('1394')) {
                $post_type = 'library';
            } elseif (is_page('820')) {
                $post_type = 'report';
            }
            $parent_nav = new WP_Query(array(
                'order' => 'Desc',
                'post_status' => 'publish',
                'post_type' => $post_type,
                'paged' => get_query_var('paged')
            ));
            ?>
            <?php
            if ($parent_nav->have_posts()) : while ($parent_nav->have_posts()) : $parent_nav->the_post();
                    $backgroundimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    $post = get_post($post);
                    if ($backgroundimage) {
                        ?>
                        .blog-listing-<?php echo $i; ?> .<?php echo $post->post_type; ?>-listing.blog-featured {
                            background-image:url('<?php echo $backgroundimage; ?>');
                        }
            <?php
        }
        $i++;
    endwhile;
    wp_reset_query();
endif;
?>

        </style>


        <?php wp_head(); ?>
		<meta name="google-site-verification" content="o4qRVEOXcBLUP8cRRpVfdkygMThMGXJmko6vmgcVu6s" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-71852357-26"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-71852357-26', {'send_page_view': false, 'optimize_id': 'GTM-NJFW26V'});
</script>
    </head>
