<?php
/**
 *  Name: Theme Functions 
 *  Descripion: Common function file
 *  @package      WordPress
 *  @subpackage   Sevenish Law
 */
/**
 *  Set Featured Image for pages
 *  @type	function
 *  @return	n/a
 */
add_theme_support('post-thumbnails', array('post', 'page', 'video', 'faq', 'report', 'library', 'case-result'));

/**
 *  Include header meta tag files
 *  @type	function
 *  @return	n/a
 */
function head() {
    include_once 'includes/head.php';
}

/**
 *  This function is use to enqueue scripts and css
 */
function assets() {
    wp_enqueue_style('bootsrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('rateyo', get_stylesheet_directory_uri() . '/css/jquery.rateyo.min.css');
    wp_enqueue_style('owl-css', get_stylesheet_directory_uri() . '/css/owl.carousel.css');
    wp_enqueue_style('owl-theme-css', get_stylesheet_directory_uri() . '/css/owl.theme.default.min.css');
    //wp_enqueue_style('theme-style', get_stylesheet_uri());
    //wp_enqueue_style('main-style', get_stylesheet_directory_uri(). '/style.css', 'false', 'false' );
    wp_enqueue_style('main-style', get_stylesheet_directory_uri(). '/style.css', array(), null, false);
    wp_enqueue_style('responsiveness', get_stylesheet_directory_uri() . '/css/responsive.css', array(), null, false);
    wp_enqueue_script('jquery-validation', get_stylesheet_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), false, true);
    wp_enqueue_script('jqueryrateyo', get_stylesheet_directory_uri() . '/js/jquery.rateyo.min.js', array('jquery'));
    wp_enqueue_script('bootstrap-script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'));
    wp_enqueue_script('jq-js', get_template_directory_uri() . '/js/html5lightbox.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('scrollbar-script', get_stylesheet_directory_uri() . '/js/jquery.custom-scrollbar.js', array('jquery'));
    wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/main.js', array('jquery'));
    wp_enqueue_script('owl-script', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'));
}

add_action('wp_enqueue_scripts', 'assets');

/**
 * This Function use to  display theme logo
 * @type	function
 * @date	01/06/2017
 * @return	n/a
 */
function theme_logo() {
    if (is_front_page()) {
        ?>
        <img src="<?php bloginfo('template_url'); ?>/img/header-logo.png" alt="<?php bloginfo('name') ?>" />
    <?php } else { ?>
        <a href="<?php bloginfo('url'); ?>">
            <img src="<?php bloginfo('template_url'); ?>/img/header-logo.png" alt="<?php bloginfo('name') ?>" />
        </a>
        <?php
    }
}

/**
 *  Custom Menus
 *  @type	function
 *  @date	18/04/2017
 *  @return	n/a
 */
register_nav_menus(array(
    'primary' => __('Primary Navigation', 'main-nav')
));

/**
 *   Widgets
 *  @type	function
 *  @return	n/a
 */
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Header Right',
        'id' => 'header-right',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Header Left',
        'id' => 'header-left',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Footer Left Column',
        'id' => 'footer-left-middle',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Footer Middle Column',
        'id' => 'footer-col-middle',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Footer Right Column',
        'id' => 'footer-col-right',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Footer Bottom Column',
        'id' => 'footer-col-bottom',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Footer Copyright Column',
        'id' => 'footer-col-copyright',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="fullwidth widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="fullwidth widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Page Sidebar',
        'id' => 'page-sidebar',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Blog Sidebar',
        'id' => 'blog-sidebar',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => '404 Sidebar',
        'id' => 'error-sidebar',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));
    register_sidebar(array(
        'name' => 'Attorneys Sidebar',
        'id' => 'attorneys-sidebar',
        'description' => '',
        'class' => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widgettitle">',
        'after_title' => '</h3>'
    ));
}

/**
 *  Theme menu use to show main menu 
 *  @type	function
 *  @date	1/06/2017
 *  @return	n/a
 */
function theme_menu() {
    echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>';
    wp_nav_menu(array(
        'container_class' => 'header-nav collapse navbar-collapse',
        'menu_class' => 'nav navbar-nav',
        'theme_location' => 'primary')
    );
}

/**
 *  Function to control excerpt length 
 *  @type	function
 *  @return	n/a
 */
function excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '..';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

/*
 *  Theme Custom Post Types
 */

function sevenish_custom_post() {
    $args = ['public' => true,
        'label' => 'Testimonials',
        'rewrite' => array('with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('client-review', $args);
    $args = ['public' => true,
        'label' => 'Videos',
        'rewrite' => array('slug'=>'video','with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('video', $args);
    $args = ['public' => true,
        'label' => 'FAQs',
        'rewrite' => array('with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('faq', $args);
    $args = ['public' => true,
        'label' => 'Library',
        'rewrite' => array('with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('library', $args);
    $args = ['public' => true,
        'label' => 'Free Books',
        'rewrite' => array('with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('report', $args);
    $args = ['public' => true,
        'label' => 'Case Results',
        'rewrite' => array('with_front' => false),
        'supports' => array('excerpt', 'thumbnail', 'title', 'editor')
    ];
    register_post_type('case-result', $args);
}

add_action('init', 'sevenish_custom_post');

/* * ********** Register Custom Taxonomies ********* */
add_action('init', 'create_case_tax');

function create_case_tax() {
    register_taxonomy(
            'video-cat', 'video', array(
        'label' => __('Videos Category'),
        'rewrite' => array('slug' => 'videos/category', 'with_front' => false),
        'hierarchical' => true,
            )
    );
    register_taxonomy(
            'faq-cat', 'faq', array(
        'label' => __('FAQs Category'),
        'rewrite' => array('slug' => 'faqs/category', 'with_front' => false),
        'hierarchical' => true,
            )
    );
    register_taxonomy(
            'library-cat', 'library', array(
        'label' => __('Library Category'),
        'rewrite' => array('slug' => 'library-cat', 'with_front' => 'false'),
        'hierarchical' => true,
            )
    );
    register_taxonomy(
            'case-cat', 'case-result', array(
        'label' => __('Case Results Category'),
        'rewrite' => array('slug' => 'case-results-category', 'with_front' => false),
        'hierarchical' => true,
            )
    );
}

/**
 * Parent Child Functionality
 *  @type	function
 */
function wpb_list_child_pages() {
    global $post;
    $currentPage = get_the_id();
    $children = get_pages('child_of=' . $currentPage);
    if (count($children) != 0) {
        $customtitle = get_field('custom_widget_title', $post->ID);
        ?>
        <div class="widget">
            <h3 class="widgettitle childpracticeicon"><?php
                if ($customtitle) {
                    echo $customtitle;
                } else {
                    echo get_the_title($currentPage);
                }
                ?></h3>
            <div class="widget-content">
                <ul class="menu">
                    <?php
                    $pageArgs = array(
                        'sort_order' => 'asc',
                        'sort_column' => 'post_title',
                        'hierarchical' => 1,
                        'child_of' => 0,
                        'parent' => $currentPage,
                        'post_type' => 'page',
                        'post_status' => 'publish'
                    );
                    $pages = get_pages($pageArgs);
                    foreach ($pages as $page_list) {
                        $option = $page_list->post_title;
                        $title2 = get_field('menu_title', $page_list->ID);
                        if ($page_list->id == $currentPage) {
                            $activeClass = "current_page_item";
                        } else {
                            $activeClass = null;
                        }
                        ?>
                        <li class="<?php echo $activeClass ?>">
                            <a href="<?php echo get_page_link($page_list->ID); ?>"><?php
                                if ($title2) {
                                    echo $title2;
                                } else {
                                    echo $option;
                                }
                                ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <script>
            jQuery(document).ready(function ($) {
                jQuery('#nav_menu-6').remove();
            });
        </script>
        <?php
    } else {
        $parentId2 = wp_get_post_parent_id($currentPage);
        if ($parentId2 > 0) {
            $customtitlep = get_field('custom_widget_title', $parentId2);
            ?>

            <div class="widget">
                <h3 class="widgettitle childpracticeicon"><?php
                    if ($customtitlep) {
                        echo $customtitlep;
                    } else {
                        echo get_the_title($parentId2);
                    }
                    ?></h3>
                <div class="widget-content">
                    <ul class="menu">
                        <?php
                        $pageArgs = array(
                            'sort_order' => 'asc',
                            'sort_column' => 'post_title',
                            'hierarchical' => 1,
                            'child_of' => 0,
                            'parent' => $parentId2,
                            'offset' => 0,
                            'post_type' => 'page',
                            'post_status' => 'publish'
                        );
                        $pages = get_pages($pageArgs);
                        foreach ($pages as $page_list) {
                            if ($page_list->ID == $currentPage) {
                                $activeClass = "current_page_item";
                            } else {
                                $activeClass = null;
                            }

                            $page_title = $page_list->post_title;
                            $menuTitle = get_field('menu_title', $page_list->ID);
                            ?>
                            <li class="<?php echo $activeClass; ?>">
                                <a href="<?php echo get_page_link($page_list->ID); ?>"><?php
                                    if ($menuTitle) {
                                        echo $menuTitle;
                                    } else {
                                        echo $page_title;
                                    }
                                    ?></a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <script>
                jQuery(document).ready(function ($) {
                    jQuery('#nav_menu-6').remove();
                });
            </script>
        <?php }
        ?>

        <?php
    }
    ?>

    <?php
}

add_shortcode('wpb_childpages', 'wpb_list_child_pages');
add_filter('widget_text', 'do_shortcode');
remove_action('wp_head', 'rest_output_link_wp_head');
/* * *********** Add Browser Class ************* */

function mv_browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if ($is_lynx)
        $classes[] = 'lynx';
    elseif ($is_gecko)
        $classes[] = 'gecko';
    elseif ($is_opera)
        $classes[] = 'opera';
    elseif ($is_NS4)
        $classes[] = 'ns4';
    elseif ($is_safari)
        $classes[] = 'safari';
    elseif ($is_chrome)
        $classes[] = 'chrome';
    elseif ($is_IE) {
        $classes[] = 'internet-explorer';
        if (preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
            $classes[] = 'ie' . $browser_version[1];
    } else
        $classes[] = 'unknown';
    if ($is_iphone)
        $classes[] = 'iphone';
    if (stristr($_SERVER['HTTP_USER_AGENT'], "mac")) {
        $classes[] = 'osx';
    } elseif (stristr($_SERVER['HTTP_USER_AGENT'], "linux")) {
        $classes[] = 'linux';
    } elseif (stristr($_SERVER['HTTP_USER_AGENT'], "windows")) {
        $classes[] = 'windows';
    }
    return $classes;
}

add_filter('body_class', 'mv_browser_body_class');

add_action('nav_menu_css_class', 'add_current_nav_class', 10, 2);

function add_current_nav_class($classes, $item) {

    // Getting the current post details
    global $post;

    // Getting the post type of the current post
    $current_post_type = get_post_type_object(get_post_type($post->ID));
    $current_post_type_slug = $current_post_type->rewrite['slug'];

    // Getting the URL of the menu item
    $menu_slug = strtolower(trim($item->url));

    // If the menu item URL contains the current post types slug add the current-menu-item class
    if (strpos($menu_slug, $current_post_type_slug) !== false) {

        $classes[] = 'current-menu-item';
    }
    if (( is_post_type_archive('report') || is_singular('report')) || ( is_post_type_archive('case-result') || is_singular('case-result') || is_post_type_archive('faq') || is_singular('faq') || is_singular('library') || (is_post_type_archive('video') || is_singular('video') ) || (is_tax()) && $item->title == 'Blog')
    ) {
        $classes = array_diff($classes, array('current_page_parent'));
    }

    // Return the corrected set of classes to be added to the menu item
    return $classes;
}

add_filter('walker_nav_menu_start_el', 'wpse_226884_replace_hash', 999);

/**
 * Replace # with js
 * @param string $menu_item item HTML
 * @return string item HTML
 */
function wpse_226884_replace_hash($menu_item) {
    if (strpos($menu_item, 'href="#"') !== false) {
        $menu_item = str_replace('href="#"', 'href="javascript:void(0);"', $menu_item);
    }
    return $menu_item;
}

if (function_exists('acf_add_options_page')) {

    acf_add_options_page();
}


/* Remove type attribute*/
add_filter( 'style_loader_tag',  'clean_style_tag'  );
add_filter( 'script_loader_tag', 'clean_script_tag'  );

/**
 * Clean up output of stylesheet <link> tags
 */
function clean_style_tag( $input ) {
    preg_match_all( "!<link rel='stylesheet'\s?(id='[^']+')?\s+href='(.*)' type='text/css' media='(.*)' />!", $input, $matches );
    if ( empty( $matches[2] ) ) {
        return $input;
    }
    // Only display media if it is meaningful
    $media = $matches[3][0] !== '' && $matches[3][0] !== 'all' ? ' media="' . $matches[3][0] . '"' : '';

    return '<link rel="stylesheet" href="' . $matches[2][0] . '"' . $media . '>' . "\n";
}

/**
 * Clean up output of <script> tags
 */
function clean_script_tag( $input ) {
    $input = str_replace( "type='text/javascript' ", '', $input );

    return str_replace( "'", '"', $input );
}

add_filter ('wpseo_breadcrumb_output','bybe_crumb_v_fix');
function bybe_crumb_v_fix ($link_output) {
  $link_output = preg_replace(array('#<span xmlns:v="http://rdf.data-vocabulary.org/\#">#','#<span typeof="v:Breadcrumb"><a href="(.*?)" .*?'.'>(.*?)</a></span>#','#<span typeof="v:Breadcrumb">(.*?)</span>#','# property=".*?"#','#</span>$#'), array('','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="$1" itemprop="url"><span itemprop="title">$2</span></a></span>','<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">$1</span></span>','',''), $link_output);
  return $link_output;
}

add_filter( 'disable_wpseo_json_ld_search', '__return_true' );


require_once('json-ld/person.php');


// Change Breadcrumbs

add_filter( 'wpseo_breadcrumb_links', 'yoast_seo_breadcrumb_append_link' );
function yoast_seo_breadcrumb_append_link( $links ) {
    global $post;

    $is_video = is_singular('video') || is_tax('video-cat');
    $is_faq = is_singular('faq') || is_tax('faq-cat');
    $is_case = is_singular('case-result') || is_tax('case-cat');
    $is_report = is_singular('report');


    if($is_video) {
      array_splice($links, 1, 0, [
        [
          'url' => '/videos/',
          'text' => 'Videos'
        ]
      ]);
    }

    if($is_faq) {
      array_splice($links, 1, 0, [
        [
          'url' => '/faqs/',
          'text' => 'FAQs'
        ]
      ]);
    }

    if($is_case) {
      array_splice($links, 1, 0, [
        [
          'url' => '/case-results/',
          'text' => 'Case Results'
        ]
      ]);
    }

    if($is_report) {
      array_splice($links, 1, 0, [
        [
          'url' => '/free-books/',
          'text' => 'Free Books'
        ]
      ]);
    }

    return $links;
}

/*--------------------------------------------
Adding Page Number to Title
---------------------------------------------*/
if (!function_exists('title_add_page_number')) {
    function title_add_page_number($s)
    {
        global $page;
        global $numpages;

        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
        !empty ($page) && 1 < $page && $paged = $page;

        if($paged > 1) {
            $s = str_replace('- Sevenish Law', '- '.sprintf(__('Page %s'), $paged).' - Sevenish Law', $s);
        }

        return $s;
    }

    //add_filter('wpseo_title', 'title_add_page_number', 100, 1);
}


/*--------------------------------------------
Improved Page Navigation
---------------------------------------------*/

function numeric_posts_nav() {

  if( is_singular() )
    return;

  global $wp_query;

  /** Stop execution if there's only 1 page */
  if( $wp_query->max_num_pages <= 1 )
    return;

  $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
  $max   = intval( $wp_query->max_num_pages );

  /** Add current page to the array */
  if ( $paged >= 1 )
    $links[] = $paged;

  /** Add the pages around the current page to the array */
  if ( $paged >= 3 ) {
    $links[] = $paged - 1;
    $links[] = $paged - 2;
  }

  if ( ( $paged + 2 ) <= $max ) {
    $links[] = $paged + 2;
    $links[] = $paged + 1;
  }

  echo '<div class="pagination"><ul>' . "\n";

  /** Previous Post Link */
  if ( get_previous_posts_link() )
    printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

  /** Link to first page, plus ellipses if necessary */
  if ( ! in_array( 1, $links ) ) {
    $class = 1 == $paged ? ' class="active"' : '';

    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

    if ( ! in_array( 2, $links ) )
      echo '<li><span>...</span></li>';
  }

  /** Link to current page, plus 2 pages in either direction if necessary */
  sort( $links );
  foreach ( (array) $links as $link ) {
    $class = $paged == $link ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
  }

  /** Link to last page, plus ellipses if necessary */
  if ( ! in_array( $max, $links ) ) {
    if ( ! in_array( $max - 1, $links ) )
      echo '<li><span>...</span></li>' . "\n";

    $class = $paged == $max ? ' class="active"' : '';
    printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
  }

  /** Next Post Link */
  if ( get_next_posts_link() )
    printf( '<li>%s</li>' . "\n", get_next_posts_link() );

  echo '</ul></div>' . "\n";

}
