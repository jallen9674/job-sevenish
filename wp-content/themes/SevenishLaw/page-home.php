<?php
/**
 * Theme Page file used to show Pages
 * @package      WordPress
 * @subpackage   Sevenish Law
 */
get_header();
?>
<div class="fullwidth page-content">

    <?php /*    <div id="page-banner" class="bg-cover">
      <div class="banner-content white-text text-center">
      <div class="container">
      <h2><?php echo get_the_title(362); ?></h2>
      </div>
      </div>
      <?php
      if (function_exists('yoast_breadcrumb')) {
      yoast_breadcrumb('
      <div id="crumbs"><div class="container">', '</div></div>');
      }
      ?>
      </div> */ ?>

    <div id="page-banner" class="bg-cover">
        <div class="banner-content white-text">
            <div class="container">
                <div class="col-xs-12 col-sm-6 text-left">
                    <h2>A Lifetime Of Service</h2>
                    <h3>Fierce Protectors of the Injured<span>©</span></h3>
                    <p>Police Captain, SWAT Team Founder & Commander, Karate Sensei. Injury Lawyer &
                        Fierce Protector of the Injured©</p>
                    <a class="btn btn-lg" href="/contact-us/">Contact Us</a>
                </div>

            </div>
        </div>
        <?php
        if (function_exists('yoast_breadcrumb')) {
            yoast_breadcrumb('
<div id="crumbs"><div class="container">', '</div></div>');
        }
        ?>
    </div>
    <div id="home-banner">
       <!-- <div id="banner-video"> <?php /* Banner video */ ?>
            <video autoplay loop muted>
                <?php if (get_field('banner_video_mp4')): ?>
                    <source src="<?php the_field('banner_video_mp4'); ?>" type="video/mp4">
                    <?php
                endif;
                if (get_field('banner_video_ogg')):
                    ?>
                    <source src="<?php the_field('banner_video_ogg'); ?>" type="video/ogg">
                    <?php
                endif;
                if (get_field('banner_video_webm')):
                    ?>
                    <source src="<?php the_field('banner_video_webm'); ?>" type="video/webm">
                <?php endif;
                ?>
            </video>
            <a class="video-mute"><span class="fa fa-volume-off" aria-hidden="true"></span></a>

        </div>-->
        <div class="banner fullwidth">
            <div class="container">
                <div class="home-banner-text col-sm-7 col-xs-12">
                    <div class="home-banner-title">
                        <h1><?php
                            if (get_field('home_banner_title')) {
                                the_field('home_banner_title');
                            }
                            ?></h1>
                    </div>
                    <div class="home_content">
                        <?php
                        if (get_field('home_content')) {
                            the_field('home_content');
                        }
                        ?>
                    </div>
                    <div class="home-video-thumb">
                        <?php if (get_field('popup_banner_video')): ?>
                            <!--<a href="https://youtu.be/<?php // echo get_field('popup_banner_video'); ?>" class="video-home html5lightbox"></a>-->
                         <!--    <a href="https://youtu.be/<?php echo get_field('popup_banner_video'); ?>" class="video-home">	    
                                <span class="fa fa-play-circle" aria-hidden="true"></span>
                            </a> -->
                              <a download target="_blank" href="javascript:void(0);" imglink="#" videolink="<?php echo get_field('popup_banner_video'); ?>" class="download_popup">        <span class="fa fa-play-circle" aria-hidden="true"></span>
                                             </a>
                        <?php endif;
                        ?>
                    </div>
                </div>
                <div id="home-banner-form" class="col-sm-4 col-sm-offset-1  col-xs-12">
                    <?php
                    echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form"]');
                    ?>
                </div>
            </div>
        </div>
        <div id="awards-sec" class="award-slider-section">
            <div class="container">
                <div class="owl-carousel owl-theme">
                    <?php
                    if (have_rows('awards_section')):
                        while (have_rows('awards_section')) : the_row();
                            $award_img = get_sub_field('add_award');
                            ?>
                            <div class="item text-center">
                                <img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
                            </div>
                            <?php
                            the_sub_field('sub_field_name');
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div id="awards-sec1" class="awards-slider award-slider-section">
        <div class="container">
            <div class="owl-carousel owl-theme">
                <?php
                if (have_rows('awards_section')):
                    while (have_rows('awards_section')) : the_row();
                        $award_img = get_sub_field('add_award');
                        ?>
                        <div class="item text-center">
                            <img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
                        </div>
                        <?php
                        the_sub_field('sub_field_name');
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-12 col-xs-12 page-wrap text-center"><?php /* Page main content section */ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <!--                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                <?php
                /* echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form"]'); */

                //echo do_shortcode('[lexicata-contact-form]');
                ?>
                                </div>  -->
            </div>
        </div>
    </div>

    <div id="practice-areas" class="single-section white-text text-center Practice-Area-Main bg-cover"> 
        <div class="container">
            <?php the_field('practice_areas_content_section'); ?>
        </div>
        <div class="practice-list col-xs-12 nopadding practice-inner">
            <?php
            if (have_rows('practice_areas_sec')):
                $i = 1;
                while (have_rows('practice_areas_sec')): the_row();
                    $postObj = get_sub_field('select_practice_area_page');
                    $postId = $postObj->ID;
                    $PracticeBg = get_field('practice_area_image', $postId);
                    $PracticeIcon = get_field('practice_area_icon', $postId);
                    $pageLink = get_permalink($postId);
                    $postTitle = get_field('practice_area_title', $postId);
                    ?>
                    <a class="practice-<?php echo $i; ?>" href="<?php echo $pageLink ?>" title="<?php echo $postTitle; ?>">
                        <div class="bg-image bg-cover"></div>
                        <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2 area-box">
                            <div class="col-xs-12 nopadding area-inner-box">
                                <div class="col-xs-12 nopadding main-box">
                                    <span class="practice-detail-title"> <?php echo $postTitle; ?></span>
                                </div>
                                <div class="overlay-box">
                                    <div>
                                        <?php if ($PracticeIcon): ?>
                                            <img src="<?php echo $PracticeIcon['url']; ?>" alt="<?php echo $PracticeIcon['alt']; ?>"/>
                                        <?php endif;
                                        ?>
                                        <span class="practice-detail-title"><?php echo $postTitle ?></span>
                                        <img src="<?php bloginfo('template_url'); ?>/img/practice-arrow.png" alt="Practice Link"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <?php
                    $i++;
                endwhile;
            endif;
            wp_reset_query();
            ?>
        </div>
    </div>
    <div id="below-practice-areas" class="single-section text-center">
        <div class="container">
            <?php the_field('below_practice_areas_section'); ?>
            <div class="video-container">
                <?php
                if (get_field('home_video') == 'home_video_youtube'):
                    $video_id = get_field('home_video_youtube');
                    ?>
                    <div class="hidden-sm hidden-xs">
                        <a href="https://youtu.be/<?php echo $video_id; ?>" class="video-home fancybox-youtube">	    
                            <?php
                            if (get_field('home_video_thumb')):
                                $video_thumb = get_field('home_video_thumb');
                            else:
                                $video_thumb = 'https://i.ytimg.com/vi/' . $video_id . '/maxresdefault.jpg';
                            endif;
                            ?>
                            <img class="home-video-thumb" src="<?php echo $video_thumb; ?>" alt="video-thumb"/>
                        </a>
                    </div>
                    <div class="hidden-md hidden-lg embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" allowfullscreen></iframe>

                        <?php
                    elseif (get_field('home_video') == 'home_video_mp4'):
                        ?>
                        <div class="hidden-sm hidden-xs">
                            <a href="<?php echo get_field('home_video_mp4'); ?>" class="video-home html5lightbox">	    
                                <?php
                                if (get_field('home_video_thumb')):
                                    $video_thumb = get_field('home_video_thumb');
                                else:
                                    $video_thumb = 'https://i.ytimg.com/vi/' . $video_id . '/maxresdefault.jpg';
                                endif;
                                ?>
                                <img class="home-video-thumb" src="<?php echo $video_thumb; ?>"/>
                            </a>
                        </div>
                        <div class="hidden-md hidden-lg video-mp4-mobile">
                            <video controls  poster="<?php echo $video_thumb; ?>">
                                <source src="<?php echo get_field('home_video_mp4'); ?>" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="tabs-sec" class="single-section white-text text-center">
            <div class="container">
                <h2>Personal Injury Claims Process</h2>
            </div>
            <div class="tabs-block text-center">
                <ul class="nav nav-tabs">
                    <?php
                    if (have_rows('tabs_section')):
                        $i = 1;
                        while (have_rows('tabs_section')) : the_row();
                            ?>
                            <li class="<?php
                            if ($i == 1) {
                                echo 'active';
                            }
                            ?>">
                                <a data-toggle="tab" href="#tab-content-<?php echo $i; ?>">
                                    <span class="tab-number"><?php echo $i; ?></span>
                                    <span class="tab-title"><?php the_sub_field('tab_title'); ?></span>
                                </a>
                            </li>
                            <?php
                            $i++;
                        endwhile;
                    else :
                    endif;
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                    if (have_rows('tabs_section')):
                        $i = 1;
                        while (have_rows('tabs_section')) : the_row();
                            ?>
                            <div id="tab-content-<?php echo $i; ?>" class="single-tab bg-cover tab-pane fade <?php
                            if ($i == 1) {
                                echo 'in active';
                            }
                            ?>">
                                <div class="col-sm-6 col-sm-offset-6 text-justify tab-main">
                                    <div class="tab-main-content">
                                        <?php the_sub_field('tab_content'); ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>

                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div id="below-tabs" class="single-section text-justify">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <?php
                        $content_col = get_field('below_tabs_section_left_col_content');
                        echo str_replace('rel="noopener"', ' ', $content_col);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <?php
                        $content_col = get_field('below_tabs_section_right_col_content');
                        echo str_replace('rel="noopener"', ' ', $content_col);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="attorneys-sec" class="single-section text-center white-text bg-cover">
            <div class="container">
                <div class="row">
                    <?php the_field('attorneys_section_content'); ?>
                    <?php
                    if (have_rows('attorneys_section')):
                        while (have_rows('attorneys_section')) : the_row();
                            $postObj = get_sub_field('attorney_page_link');
                            $pageLink = get_permalink($postObj);
                            ?>
                            <div class="single-attorney">
                                <?php $attor_img = get_sub_field('add_attorney_image'); ?>
                                <div class="attorney-img-block">
                                    <a href="<?php echo $pageLink ?>">
                                        <img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>
                                    </a>
                                </div>
                                <span class="attor-designation"><?php the_sub_field('add_attorney_designation'); ?></span>
                                <span class="attor-title"><a href="<?php echo $pageLink ?>"><?php the_sub_field('add_attorney_name'); ?></a></span>
                            </div>
                            <?php
                        endwhile;
                    else :
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <div id="free-books-sec" class="single-section text-center bg-cover">
            <div class="container">
                <?php the_field('free_books_content_section'); ?>
                <?php
                if (have_rows('add_books')):
                    while (have_rows('add_books')) : the_row();
                        $BookObj = get_sub_field('add_book_link');
                        $BookLink = get_permalink($BookObj);
                        $book_img = get_sub_field('add_book_image');
                        ?>
                        <div class="single-book">
                            <a href="<?php echo $BookLink ?>">
                                <?php if ($book_img): ?>
                                    <img src="<?php echo $book_img['url']; ?>" alt="<?php echo $book_img['alt']; ?>"/> 
                                <?php else:
                                    ?>
                                    <span class="fa fa-book"></span>
                                <?php endif;
                                ?>
                            </a>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
        <div id="testimonials-sec" class="single-section text-center white-text bg-cover">
            <div class="container">
                <?php the_field('client_testimonials_section_content'); ?>
                <div class="owl-carousel owl-theme">
                    <?php
                    $i = 1;
                    $parent = new WP_Query(array(
                        'post_type' => 'client-review',
                        'showposts' => -1
                    ));
                    $count = $parent->post_count;
                    if ($parent->have_posts()) :
                        ?>
                        <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                            <div class="item active">
                                <div class="testimonial-main-content">
                                    <span class="testimonial_author"><?php the_title(); ?></span><?php the_field('client_designation'); ?>
                                    <div class="video_testimonial">
                                        <?php if(get_field('testimonial_video')){ ?>
                                        <iframe width="400" height="250" src="https://www.youtube.com/embed/<?php echo get_field('testimonial_video'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <?php } ?>
                                    </div>
                                    <div class="testimonail-content scroll-inner">
                                        <?php echo wp_trim_words(get_the_content(), 30); ?>
                                    </div>
                                      <div class="testimonail-content scroll-inner">
                                        <?php echo wp_trim_words(the_field('below_video_quote'), 30); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endwhile;
                        ?>
                    </div> 
                    <?php
                    unset($parent);
                endif;
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

