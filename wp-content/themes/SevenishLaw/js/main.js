jQuery(document).ready(function ($) {

    equalheight = function () {

        var maxHeight = null;

        $('.single-video-block-wrap').each(function () {
            maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
        });
        $('.single-video-block-wrap').each(function () {
            $(this).find('.single-video-block').css('min-height', maxHeight);
        });
        $('.single-written-testimonial').each(function () {
            maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
        });
        $('.single-written-testimonial').each(function () {
            $(this).find('.maintestimonials').css('min-height', maxHeight);
        });
    }
    equalheight();
    //Small Header on Scroll
    $(window).scroll(function () {
        var hdrHeight = $('#site-header').outerHeight();
        if ($(this).scrollTop() >= 10) {
            $('#site-header').addClass('small-header');
        } else {
            $('#site-header').removeClass('small-header');
        }

    });
    /*************** Margin Top to Main Content *****************/
    function headerHeight() {
        var headerHeight = $('#site-header').height();
        $("#main-wrapper").css('margin-top', headerHeight);
    }
    $(window).resize(function () {
        headerHeight();
        setTimeout(function () {
            headerHeight();
        }, 1000);
        equalheight();
    });

    setTimeout(function () {
        headerHeight();
    }, 1000);
    $('.award-slider-section .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        slideSpeed: 1000,
        autoplaySpeed: 1000,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        autoplay: true,
        dots: true,
        dotsSpeed: 500,
        navigation: true,
        navigationspeed: 1000,
        responsive: {
            0: {
                items: 2,
            },
            600: {
                items: 3
            },
            768: {
                items: 4
            },
            1000: {
                items: 5,
                navigation: false
            }
        }
    });
    $(".owl-prev").html('<span class="fa fa-chevron-left"></span>');
    $(".owl-next").html('<span class="fa fa-chevron-right"></span>');
    $('#testimonials-sec .owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        responsiveClass: true,
        autoplaySpeed: 1000,
        dots: true,
        navigation: false,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        autoplay: true,
        navSpeed: 1000,
        dotsSpeed: 1000,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 2,
                navigation: false
            }
        }
    });
//    $('.testimonialvideo').owlCarousel({
//        loop: false,
//        margin: 30,
//        autoHeight: false,
//        slideSpeed: 1000,
//        autoplayTimeout: 6000,
//        autoplaySpeed: 1000,
//        paginationSpeed: 1000,
//        rewindSpeed: 1000,
//        autoplay: true,
//        dotsSpeed: 1000,
//        nav: true,
//        dots: false,
//        navText: ["<em class='fa fa-caret-left'></em>", "<em class='fa fa-caret-right'></em>"],
//        responsive: {
//            0: {
//                items: 1,
//                nav: false
//            },
//            480: {
//                items: 1,
//                nav: false
//            },
//            800: {
//                items: 3,
//                nav: false
//            },
//            1000: {
//                items: 3,
//                nav: false
//            }
//        }
//    });
    $('.btnNext').click(function () {
        if ($('.tabs-block .nav.nav-tabs li:last-of-type').hasClass('active')) {
            $('.tabs-block .nav.nav-tabs li:first-of-type a').trigger('click');
        } else {
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        }

    });

    $('.btnPrevious').click(function () {
        if ($('.tabs-block .nav.nav-tabs li:first-of-type').hasClass('active')) {
            $('.tabs-block .nav.nav-tabs li:last-of-type a').trigger('click');
        } else {
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        }
    });
    /*
     * Equal Height JS 
     * @type	function
     * @date	12/07/2017
     */
    equalheight = function (container) {
        var currentTallest = 0,
                currentRowStart = 0,
                rowDivs = new Array(),
                $el,
                topPosition = 0;
        $(container).each(function () {
            $el = $(this);
            $($el).height('auto')
            topPostion = $el.position().top;
            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    }
    if (window.matchMedia("(min-width: 768px) and (max-width: 1400px)").matches) {
        jQuery('li#menu-item-358 ul.sub-menu').scrollbar();
    }
    $(document).on('click', '.js-videoPoster', function (ev) {
        ev.preventDefault();
        var $poster = $(this);
        var $wrapper = $poster.closest('.js-videoWrapper');
        videoPlay($wrapper);
        var video = $("#home-content-video").get(0);
        video.play();

        $(this).css("visibility", "hidden");
        return false;
    });

    /********** play the targeted video (and hide the poster frame) **********/
    function videoPlay($wrapper) {
        var $iframe = $wrapper.find('.js-videoIframe');
        var src = $iframe.data('src');
        $wrapper.addClass('videoWrapperActive');
        $iframe.attr('src', src);
    }

    /********** stop the targeted/all videos (and re-instate the poster frames) **********/
    function videoStop($wrapper) {
        if (!$wrapper) {
            var $wrapper = $('.js-videoWrapper');
            var $iframe = $('.js-videoIframe');
        } else {
            var $iframe = $wrapper.find('.js-videoIframe');
        }
        $wrapper.removeClass('videoWrapperActive');
        $iframe.attr('src', '');
    }
    /********** Video Transcript Toggle **********/
    $("#btn-transcript").click(function () {
        $('#video-transcript').toggle('slow');

    });
    $('#banner-video .video-mute').click(function () {
        if ($("#banner-video video").prop('muted'))
        {
            $("#banner-video video").prop('muted', false);
            $('.video-mute span').removeClass('fa-volume-off');
            $('.video-mute span').addClass('fa-volume-up');
        } else {
            $("#banner-video video").prop('muted', true);
            $('.video-mute span').removeClass('fa-volume-up');
            $('.video-mute span').addClass('fa-volume-off');

        }

    });
    var $rateYo = $("#rateYo").rateYo({
        rating: 0,
        fullStar: true
    });
    $("#rateYo").rateYo().on("rateyo.set", function (e, data) {
        var rating = data.rating;
        $('#rating-star').attr('value', rating);
        //alert(rating);
        if (rating <= 3) {
            $('.row.review-box').slideDown();
            $('.review-google-btn').hide();
            $('.review-submit-btn').show();
        } else {
            $('.row.review-box').slideUp();
            $('.review-google-btn').css('display', 'inline-block');
            $('.review-submit-btn').hide();
        }
    });
    $("#review-form").validate({
        rules: {
            ratingstar: {
                required: true
            },
            fname: {
                required: true
            },
            lname: {
                required: true
            },
            email: {
                required: true
            },
            phone: {
                required: true
            },
        },
        messages: {
            ratingstar: "Please select your rating.",
            fname: "Required",
            lname: "Required",
            email: "Required",
            phone: "Required"
        }
    });

});
