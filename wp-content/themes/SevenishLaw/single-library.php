<?php
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap text-justify"><?php /* Page main content section */ ?>
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <h1><?php the_title(); ?></h1>
                            <?php if (has_post_thumbnail()): ?>
                                <div class="featured-div alignleft">
                                    <?php
                                    the_post_thumbnail();
                                    ?>
                                </div>
                            <?php endif;
                            ?>
                            <?php the_content(); ?>
                            <?php if (get_field('select_newsletter_pdf')): ?>
                                <a class="btn" target="_blank" title="Download" href="<?php the_field('select_newsletter_pdf'); ?>">Download Now</a>
                            <?php endif;
                            ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <div class="widget widget_categories">
                        <h3 class="widgettitle">Categories</h3>
                        <ul>
                            <?php
                            $taxonomy = array(
                                "library-cat",
                            );
                            $args = array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                'hierarchical' => true,
                                'childless' => false,
                            );
                            $terms = get_terms($taxonomy, $args);
                            foreach ($terms as $term) {
                                ?>
                                <li class="cat-item"><a href="/library-cat/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a></li>
                                <?php }
                                ?>
                        </ul>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>