<?php
/**
 * Theme Page file used to show Pages
 * @package      WordPress
 * @subpackage   The Visa Firm
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
<?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section no-small-title error-page">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap"><?php /* Page main content section */ ?>
                    <div class="title-sec">
                        <div class="title-label">
                            <h1>404 Error! Page not found</h1>    
                        </div>
                    </div>
                    <p>The page you are looking for can't be found. Try using the menus to the right, or contact us by filling the form below. We will get in touch with you shortly.</p>
                    <div id="below-contact-sec">
                        <div class="contact-form-col">
                            <h2>Get In Touch</h2>
                            <?php echo do_shortcode('[contact-form-7 id="646" title="Contact Us Form"]'); ?>
                        </div>
                    </div>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                    <?php
                    dynamic_sidebar('404 Sidebar');
                    ?>
                </div>  
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
