<?php
/**
 * The template used for displaying the header
 * @package      WordPress
 * @subpackage   Sevenish Law
 */
head();

?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
  $( function() {
    $( "#dialog" ).dialog({
      autoOpen: false,
      show: {
       // effect: "blind",
        duration: 1000
      },
      hide: {
     //   effect: "explode",
        duration: 1000
      }
    });
 
    $( ".download_popup" ).on( "click", function() {     
        //var imglink = $(this).attr('imglink');
        var videolink = $(this).attr('videolink');
        $('#hiddendownload2').attr('href', videolink);
         var videourl = "https://www.youtube.com/embed/"+videolink;
        $('.content_download_place').html('<iframe width="100%" height="450" src="'+videourl+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
        $( "#dialog" ).dialog( "open");
    });
  } );

  </script>

<div id="dialog" title="" style="width:500px;">
    <div class="content_download_place"></div>    
</div> 

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8LJQZT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header id="site-header" class="header normalHeader fullwidth">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div id="header-left" class="col-xs-6 col-sm-5 col-md-4">
                        <?php theme_logo(); ?>
                    </div>
                    <div id="header-middle" class="col-xs-6 col-sm-5 col-md-4 text-center">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Left')) : endif; ?>
                    </div>
                    <div  id="header-right" class="col-xs-6 col-sm-5 col-md-4 text-right">
			<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Right')) : endif; ?>
                    </div> 

                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <nav id="header-nav" class="navbar fullwidth">
		    <?php theme_menu(); ?>
                </nav><?php /* End Header Nav */ ?>
            </div>
        </div>
    </header>
    <main  id="main-wrapper" class="fullwidth">


