<?php
/**
 * Template Name: Attorney Listing
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div id="page-section" class="page-wrap text-center"><?php /* Page main content section */ ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                
  <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
    <div id="attorneys-sec" class="attorney-listing single-section white-text bg-cover">
        <div class="container">
            <div class="row">
                <?php
                if (have_rows('attorneys_section', 387)):
                    while (have_rows('attorneys_section', 387)) : the_row();
                        $postObj = get_sub_field('attorney_page_link', 387);
                        $pageLink = get_permalink($postObj);
                        ?>
                        <div class="single-attorney attorney-list-block">
                            <?php $attor_img = get_sub_field('add_attorney_image', 387); ?>
                            <div class="attorney-img-block bg-cover" style="background-image: url('<?php echo $attor_img['url']; ?>')">
                                <a href="<?php echo $pageLink ?>">
                                    <!--<img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>-->
                                </a>
                            </div>
                            <div class="attorney-list-content">
                                <span class="attor-title"><a href="<?php echo $pageLink ?>" title="<?php the_sub_field('add_attorney_name', 387); ?>"><?php the_sub_field('add_attorney_name', 387); ?></a></span>
                                <span class="attor-designation"><?php the_sub_field('add_attorney_designation', 387); ?></span>
                                <?php $attor_desc = get_sub_field('attorney_description', 387);?>
                                <p><?php echo wp_trim_words($attor_desc, 32); ?></p>
                                <a class="btn" href="<?php echo $pageLink ?>" title="Read More"> Read More</a>
                            </div>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
    <div id="free-books-sec" class="single-section text-center bg-cover">
        <div class="container">
            <div class="row">
                <?php the_field('free_books_content_section', 387); ?>
                <?php
                if (have_rows('add_books', 387)):
                    while (have_rows('add_books', 387)) : the_row();
                        $BookObj = get_sub_field('add_book_link', 387);
                        $BookLink = get_permalink($BookObj);
                        $book_img = get_sub_field('add_book_image', 387);
                        ?>
                        <div class="single-book">
                            <a href="<?php echo $BookLink ?>">
                                <?php if ($book_img): ?>
                                    <img src="<?php echo $book_img['url']; ?>" alt="<?php echo $book_img['alt']; ?>"/> 
                                <?php else:
                                    ?>
                                    <span class="fa fa-book"></span>
                                <?php endif;
                                ?>
                            </a>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
    <div id="testimonials-sec" class="single-section text-center white-text bg-cover">
        <div class="container">
            <?php the_field('client_testimonials_section_content', 387); ?>
            <div class="owl-carousel owl-theme">
                <?php
                $i = 1;
                $parent = new WP_Query(array(
                    'post_type' => 'client-review',
                    'showposts' => -1
                ));
                $count = $parent->post_count;
                if ($parent->have_posts()) :
                    ?>
                    <?php while ($parent->have_posts()) : $parent->the_post(); ?>
                        <div class="item active">
                            <div class="testimonial-main-content">
                                <span class="testimonial_author"><?php the_title(); ?></span><?php the_field('client_designation'); ?>
                                <div class="testimonail-content scroll-inner">
                                    <?php echo wp_trim_words(get_the_content(), 30); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    endwhile;
                    ?>
                </div> 
                <?php
                unset($parent);
            endif;
            wp_reset_query();
            ?>
        </div>
    </div>
</div>
<?php
get_footer();

