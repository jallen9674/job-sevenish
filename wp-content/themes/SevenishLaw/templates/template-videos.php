<?php
/**
 * Template Name: Videos
 */
add_filter('wpseo_title', 'title_add_page_number', 100, 1);
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap blog-page-wrap"><?php /* Page main content section */ ?>
                     <h1><?php the_title(); ?></h1>
                    <div class="post-listing fullwidth">
                        <?php
                        $i = 1;
                        query_posts(array(
                            'order' => 'Desc',
                            'post_status' => 'publish',
                            'post_type' => 'video',
                            'paged' => get_query_var('paged')
                        ));
                        ?>
                        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                                <div class="blog-listing video-listing col-sm-6 blog-listing-<?php echo $i; ?>">
                                    <?php /*
                                    if (get_field('enter_youtube_id')):
                                        ?>
                                        <div class="hidden-xs">                   
                                        <!--     <a href="https://youtu.be/<?php //the_field('enter_youtube_id'); ?>" class="video-featured fancybox-youtube">
                                                <?php //the_post_thumbnail(); ?>
                                            </a>  -->
                                            <?php if(get_field('enter_youtube_id')) {?>
                                             <a download target="_blank" href="javascript:void(0);" imglink="#" videolink="<?php the_field('enter_youtube_id'); ?>" class="download_popup">        <?php the_post_thumbnail(); ?>
                                             </a>
                                         <?php } ?>
                                        </div>
                                        <div class="visible-xs mobile-videos">
                                            <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                                <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php the_field('enter_youtube_id'); ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                                <button class="videoPoster js-videoPoster" style="background-image:url(<?php the_post_thumbnail_url() ?>);"><span class="fa fa-play-circle" aria-hidden="true"></span></button>
                                            </div>
                                        </div>
                                    <?php else: */
                                        ?>
                                        <a href="<?php the_permalink() ?>" class="video-featured">
                                            <?php the_post_thumbnail(); ?>
                                        </a> 
                                    <?php
                                    //endif;
                                    ?>


                                    <span class="date">
                                        <?php
                                        $terms = get_the_terms(get_the_ID(), 'video-cat');
                                        if ($terms && !is_wp_error($terms)) :
                                            $category_links = array();
                                            foreach ($terms as $term) {
                                                $category_links[] = '<a href="/videos/category/' . $term->slug . '/">' . $term->name . '</a>';
                                            }

                                            $category_link = join(", ", $category_links);
                                            ?>
                                            <?php
                                        endif;
                                        echo $category_link;
                                        ?>
                                    </span>
                                    <h3>
                                        
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title(); ?>
                                            </a> 
                                        
                                    </h3>
                                </div>
                                <?php
                                $i++;
                            endwhile;
                            numeric_posts_nav();
                            wp_reset_query();
                        endif;
                        ?>
                    </div>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                     <?php /*echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form"]'); */		    
		    //echo do_shortcode('[lexicata-contact-form]');
		    ?>

                    <div class="widget widget_categories">
                        <h3 class="widgettitle">Categories</h3>
                        <ul>
                            <?php
                            $taxonomy = array(
                                "video-cat",
                            );
                            $args = array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                // 'parent' => 0,
                                'hierarchical' => true,
                                // 'child_of' => 0,
                                'childless' => false,
                            );
                            $terms = get_terms($taxonomy, $args);
                            foreach ($terms as $term) {
                                ?>
                                <li class="cat-item"><a href="/videos/category/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a></li>
                                <?php }
                                ?>
                        </ul>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

<!-- Youtube video lightbox ends -->
<?php
get_footer();

