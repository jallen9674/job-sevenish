<?php
/*
 * Template name: Articles
 */
get_header();
global $post;
$backgroundimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-8 col-xs-12 page-wrap blog-page-wrap"><?php /* Page main content section */ ?>
                    <h1><?php the_title(); ?></h1>
                    <div class="post-listing fullwidth">
                        <?php
                        $i = 1;
                        
                        $parent_nav = new WP_Query(array(
                            'order' => 'Desc',
                            'post_status' => 'publish',
                            'post_type' => 'library',
                            'paged' => get_query_var('paged')
                        ));
                        ?> 
                        <?php if ($parent_nav->have_posts()) : while ($parent_nav->have_posts()) : $parent_nav->the_post();
                        $post = get_post( $post );        
                                ?>
                                <div class="blog-listing  blog-listing-<?php echo $i; ?>">
                                    <a href="<?php the_permalink(); ?>" class=" <?php echo $post->post_type;?>-listing blog-featured bg-cover <?php
                               if (has_post_thumbnail()) {
                                   echo 'featured-bg';
                               }
                               ?>" style="<?php echo $backgroundimage; ?>">
                                    </a>
                                    <div class="blog-content half">
                                        <h3> <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <span class="date">
                                            <?php
                                            $terms = get_the_terms(get_the_ID(), 'library-cat');
                                            if ($terms && !is_wp_error($terms)) :
                                                $category_links = array();
                                                foreach ($terms as $term) {
                                                    $category_links[] = '<a href="/library-cat/' . $term->slug . '/">' . $term->name . '</a>';
                                                }
                                                $category_link = join(", ", $category_links);
                                                ?>
            <?php
        endif;
        echo $category_link;
        ?>
                                        </span>
                                        <p> 
        <?php
        echo wp_trim_words(get_the_excerpt(), 20, '...');
        ?>
                                        </p>
                                        <a class="read-more" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Read More</a>
                                    </div>
                                    <hr/>
                                </div>
                                <?php
                                $i++;
                            endwhile;
                            wp_pagenavi(array('query' => $parent_nav));
                            wp_reset_query();
                        endif;
                        ?>
                    </div>
                </div>
                <div id="page-sidebar" class="col-sm-4 col-xs-12"> <?php /* Page sidebar */ ?>
                              <?php /*echo do_shortcode('[contact-form-7 id="389" title="Sidebar Contact Form"]'); */
		    
		    //echo do_shortcode('[lexicata-contact-form]');
		    ?>
                    <div class="widget widget_categories">
                        <h3 class="widgettitle">Categories</h3>
                        <ul>
                            <?php
                            $taxonomy = array(
                                "library-cat",
                            );
                            $args = array(
                                'orderby' => 'name',
                                'order' => 'ASC',
                                'hierarchical' => true,
                                'childless' => false,
                            );
                            $terms = get_terms($taxonomy, $args);
                            foreach ($terms as $term) {
                                ?>
                                <li class="cat-item <?php
                                    if ($termname == $term->name) {
                                        echo 'current-cat';
                                    }
                                    ?>"><a href="/library-cat/<?php echo $term->slug; ?>/"><?php echo $term->name; ?></a></li>
<?php }
?>
                        </ul>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

