<?php
/**
 * Template Name: Attorney Detail
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
<?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section attorney-detail">
        <div class="container">
            <div id="page-section" class="page-wrap text-justify attor-content"><?php /* Page main content section */ ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
            <div class="attorney-more-info-block">

                <?php if (have_rows('more_about_attorney_section')): ?>
                    <ul class="nav nav-tabs">
                        <?php
                        $i = 1;
                        while (have_rows('more_about_attorney_section')) : the_row();
                            ?>
                            <li class="<?php
                            if ($i == 1) {
                                echo 'active';
                            }
                            ?>">
                                <a data-toggle="tab" href="#tab-<?php echo $i; ?>">
                                    <?php
                                    if (have_rows('attorney_left_col_titles_block')):
                                        while (have_rows('attorney_left_col_titles_block')) : the_row();
                                            ?>
                                            <div class="single-title-block">
                                                <h3><?php the_sub_field('add_info_title') ?></h3>
                                            </div>
                                            <?php
                                        endwhile;
                                    else :
                                    endif;
                                    ?>
                                </a>
                            </li>
                            <?php
                            $i++;
                        endwhile;
                        ?>
                    </ul>
                    <?php
                endif;
                ?>


                <?php if (have_rows('more_about_attorney_section')): ?>
                    <div class="tab-content">
                        <?php
                        $i = 1;
                        while (have_rows('more_about_attorney_section')) : the_row();
                            ?>
                            <div id="tab-<?php echo $i; ?>" class="tab-pane fade <?php
                            if ($i == 1) {
                                echo 'in active';
                            }
                            ?>">
                                <div class="middle-col text-justify">
                                    <?php the_sub_field('attorney_single_block_content'); ?>
                                </div>
                                <div class="right-col text-center">
                                    <?php
                                    if (have_rows('attorney_right_col_images')):
                                        while (have_rows('attorney_right_col_images')) : the_row();
                                            $award_single_img = get_sub_field('add_right_col_image');
                                            ?>
                                            <img src="<?php echo $award_single_img['url']; ?>" alt="<?php echo $award_single_img['alt']; ?>"/>
                                            <?php
                                        endwhile;
                                    else :
                                    endif;
                                    ?>
                                </div>
                            </div>
                            <?php
                            $i++;
                        endwhile;
                        ?>
                    </div>
                <?php endif;
                ?>
 
            </div>
        </div>
    </div>
    <div class="schema-hide" itemscope itemtype="http://schema.org/Attorney">
        <img itemscope itemprop="image" src="<?php echo get_template_directory_uri(); ?>/img/header-logo.png" alt="logo"/>
        <div itemprop="name"><?php the_title(); ?></div>
        <div itemprop="makesOffer" itemscope itemtype="http://schema.org/Offer">
            <div itemprop="name">Sevenish Law</div>
            <div itemprop="description">Our practice has come a long way since the beginning, but it has only gotten stronger.</div>
            <link itemprop="businessFunction" href="http://purl.org/goodrelations/v1#ProvideService" />

        </div>
        <div itemprop="address" itemscope itemtype="https://schema.org/PostalAddress">              
            <span itemprop="streetAddress">101 W Ohio St, Suite 1540</span>
            <span itemprop="addressLocality">Indianapolis,</span> 
            <span itemprop="addressRegion">IN </span>
            <span itemprop="postalCode">46204</span>                       
        </div>           
        <span itemprop="telephone">(317) 636-7777</span>     
        <span itemprop="priceRange">N/A</span><br>
    </div>
</div>
<?php
get_footer();

