<?php
/**
 * Template Name: Client Review
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div class="row">
                <div id="page-section" class="col-sm-12 col-xs-12 page-wrap text-justify"><?php /* Page main content section */ ?>
                    <h1><?php the_title(); ?></h1>
                    <?php
                    $video_thumbnail = get_field('page_video_thumbnail');
                    $youtube_video_id = get_field('youtube_video_id');
                    if ($video_thumbnail && $youtube_video_id) {
                        ?>
                        <div class="page-video-section alignright">
                            <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $video_thumbnail; ?>);">Play video</button>
                            </div>
                            <img src="<?php echo $video_thumbnail; ?>" itemprop="thumbnailURL" class="hide"/>
                            <a class="hide" target="_blank" itemprop="embedURL" href="https://youtu.be/<?php echo get_field('youtube_video_id'); ?>">Watch the Video</a>
                        </div>
                    <?php }
                    ?>

                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php
                    endif;
                    wp_reset_query();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div id="video-testimonials" class="testimonialblock testimonialtop single-section fullwidth">
        <div class="container">
            <h2>Video Reviews</h2>
            <div class="testimonialvideo nopadding row">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                        )
                );
                ?>
                <?php while ($loop->have_posts()) : $loop->the_post(); ?>
                    <?php
                    if (get_field('testimonial_video')):
                        if (get_field('video_thumbnail')) {
                            $thumb = get_field('video_thumbnail');
                        } else {
                            $thumb = '/wp-content/uploads/2017/11/mqdefault-83.jpg';
                        }
                        ?>
                        <div class="col-sm-6 single-video-block-wrap">
                            <div class="single-video-block ">
                                <div class="video-wrap hidden-xs">
                                    <a class="fancybox-youtube single-video" rel="group" href="https://youtu.be/<?php the_field('testimonial_video'); ?>">
                                        <img src="<?php echo $thumb; ?>">
                                    </a>
                                </div>
                                <div class="visible-xs mobile-videos">
                                    <div class="videoWrapper videoWrapper169 js-videoWrapper">
                                        <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php the_field('testimonial_video'); ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                                        <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $thumb; ?>);"></button>
                                    </div>
                                </div>
                                <div class="below-video-content"><div class="testimonials-title"><?php
                                        if (get_field('video_title')) {
                                            the_field('video_title');
                                        } else {
                                            the_title();
                                        }
                                        ?></div>
                                    <?php if (get_field('below_video_quote')) { ?>
                                        <div class="testimonial-quote">
                                            <?php the_field('below_video_quote'); ?>
                                        </div>
                                    <?php } ?>
                                    <?php if (get_field('video_testimonial_client_info')) { ?>
                                        <div class="testimonial-client-info">
                                            - <?php the_field('video_testimonial_client_info'); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php
                endwhile;
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
    <div id="written-testimonials" class="testimonialblock testimonialbottom fullwidth single-section">
        <div class="container">
            <h3>Written Reviews</h3>
            <div class="row">
                <?php
                $loop = new WP_Query(array(
                    'post_type' => 'client-review',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                        )
                );
                ?>

                <?php while ($loop->have_posts()) : $loop->the_post(); ?>

                    <?php if (get_field('written_testimonial_quote')) : ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote">
                                    <?php the_field('written_testimonial_quote'); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_field('written_testimonial_client_info'); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    elseif (get_the_content()) :
                        ?>
                        <div class="col-12 col-sm-6 col-lg-4 single-written-testimonial">
                            <div class="maintestimonials">
                                <div class="testimonial-quote">
                                    <?php the_content(); ?>
                                </div>
                                <div class="testimonial-client-info">-
                                    <?php the_title(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif;
                    ?>

                    <?php
                endwhile;
                wp_reset_query();
                ?>

            </div>
        </div>
    </div>
    <div id="social-testimonials" class="testimonialblock fullwidth single-section">
        <div class="container">
            <?php if (get_field('social_review_title')) { ?>
                <h3><?php the_field('social_review_title'); ?></h3>
            <?php } ?>
            <?php if (get_field('social_review_subtitle')) { ?>
                <div class="social-review-subtitle"><?php the_field('social_review_subtitle'); ?></div>
            <?php } ?>
            <div class="social-review-list">
                <?php
                if (have_rows('social_reviews')):
                    while (have_rows('social_reviews')) : the_row();
                        ?>
                        <div class="single-social-block">
                            <?php if (get_sub_field('review_content')) { ?>
                                <div class="social-review-quote"><?php the_sub_field('review_content'); ?></div>
                            <?php } ?>
                            <?php if (get_sub_field('social_review_title')) { ?>
                                <div class="social-review-quote"><?php the_sub_field('social_review_title'); ?></div>
                            <?php } ?>
                            <div class="review-profile">
                                <?php
                                if (get_sub_field('profile_picture')) {
                                    $profile = get_sub_field('profile_picture');
                                    ?>
                                    <img class="review-profile-img alignleft" src="<?php echo $profile; ?>" alt="<?php echo $profile['alt']; ?>">
                                <?php } else {
                                    ?>
                                    <img class="review-profile-img alignleft" src="/wp-content/uploads/2018/03/thumb-anonymous.png" alt="sevenish-law">
                                <?php } ?>
                                <?php if (get_sub_field('reviewed_by')) { ?>
                                    <div class="social-reviewed-by">by <?php the_sub_field('reviewed_by'); ?></div>
                                <?php } ?>

                                <?php if (get_sub_field('reviewed_at')) { ?>
                                    <p>REVIEWED AT<a href="<?php the_sub_field('reviewed_at_link'); ?>" class="social-reviewed-at">
                                            <?php the_sub_field('reviewed_at'); ?></a></p>
                                <?php } ?>

                                <?php $star = get_sub_field('review_rating'); ?>
                                <span class="stars">
                                    <?php
                                    if ($star == 1) {
                                        echo"★";
                                    } elseif ($star == 2) {
                                        echo"★★";
                                    } elseif ($star == 3) {
                                        echo"★★★";
                                    } elseif ($star == 4) {
                                        echo"★★★★";
                                    } elseif ($star == 5) {
                                        echo"★★★★★";
                                    } else {
                                        
                                    }
                                    ?>
                                </span>
                                <span class="rating">(<?php echo $star; ?>/5)</span>

                                </p>
                            </div>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

