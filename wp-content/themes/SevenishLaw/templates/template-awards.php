<?php
/**
 * Template Name: Awards
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div id="page-section" class="text-center award-content-main page-wrap"><?php /* Page main content section */ ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
            <div id="awards-sec-block">
                <?php
                if (have_rows('awards_memberships')):
                    $i = 1;
                    while (have_rows('awards_memberships')) : the_row();
                        $award_img = get_sub_field('add_award_image');
                        if ($i % 2 == 0) {
                            ?>
                            <div class="single-award">
                                <div class="award-main right-img-block <?php
                                if ($award_img) {
                                    echo 'half-award';
                                }
                                ?>">
                                         <?php the_sub_field('add_award_description'); ?>
                                </div>
                                <?php if ($award_img): ?>
                                    <div class="award-img-block">
                                        <img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
                                    </div>
                                <?php endif;
                                ?>
                            </div>
                        <?php } else {
                            ?>
                            <div class="single-award">
                                <?php if ($award_img): ?>
                                    <div class="award-img-block">
                                        <img src="<?php echo $award_img['url']; ?>" alt="<?php echo $award_img['alt']; ?>"/>
                                    </div>
                                <?php endif;
                                ?>
                                <div class="award-main <?php
                                     if ($award_img) {
                                         echo 'half-award';
                                     }
                                     ?>">
                                         <?php the_sub_field('add_award_description'); ?>
                                </div>
                            </div>
                        <?php }
                        ?>

                        <?php
                        $i++;
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();

