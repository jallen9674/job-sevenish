<?php
/**
 * Template Name: Client Review New
 */
get_header();
?>
<?php if (isset($_POST['submit'])) {
    get_template_part('includes/review', 'mail');
}
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    
    <div class="main-content single-section text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                <?php
                while (have_posts()) : the_post();
                    the_content();
                endwhile;
                if (isset($_POST['submit'])) {
                    ?>
                    <div class="alert alert-success">
                        <strong>Success!</strong> Review Successfully Submitted
                    </div>
                <?php } ?>

                <div class="review-form-section-1">
                    <h3>Leave a Review</h3>

                    <?php echo do_shortcode('[contact-form-7 id="6639" title="Client Review/Feedback"]'); ?>

                    <?php /* Disabling form due to spam
                    <form action="" method="post" id="review-form">
                        <div class="row">
                            <div class="col-sm-12 rating-section col-xs-12">
                                <label for="ReviewRating">Rating:</label>
                                <div class="input-text">
                                    <div id="rateYo"></div>
                                    <div class="hidden-input hide">
                                        <input type="text" id="rating-star" name="ratingstar" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row review-box">
                            <div class="col-sm-6 col-xs-12"><input type="text" class="form-control" name="fname" placeholder="First Name" /> </div>
                            <div class="col-sm-6  col-xs-12"><input type="text" class="form-control" name="lname" placeholder="Last Name" /> </div>
                        </div>
                        <div class="row review-box">
                            <div class="col-sm-12  col-xs-12"><textarea name="caseinfo" class="form-control" placeholder="Your Feedback"></textarea></div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-12  col-xs-12  honey">
                                <input type="submit" name="submit" value="submit" class="btn review-box review-box-submit btn-primary review-submit-btn"/>
                                <a class="btn btn-primary review-google-btn" title="Review us on Google" href="https://www.google.com/search?source=hp&ei=UC6fWrPNEaq0gge1rZDQBw&q=sevenish+law+firm&oq=sevenish+law+firm&gs_l=psy-ab.3..0j0i22i30k1l2.193.4303.0.4488.28.21.6.0.0.0.181.2062.14j7.21.0..2..0...1.1.64.psy-ab..1.27.2246.0..35i39k1j0i131k1j0i67k1j0i20i264k1j0i20i264i46k1j46i20i264k1j0i20i263k1j0i10k1j0i30k1j0i10i30k1j0i13k1j0i13i30k1j0i5i10i30k1j0i22i10i30k1.0.w-gfu7akr5Y#lrd=0x886b5095c580a4b9:0x1dd4b8f6c60e0b04,3,5" target="_blank">Review us on Google</a>
                            </div>
                        </div>
                    </form> */ ?>
                </div>
                <?php
                wp_reset_query();
                // Schema template 
                get_template_part('includes/schema-template');
                ?>
            </div>   
        </div>
    </div>
</div>
</div>
<?php
get_footer();

