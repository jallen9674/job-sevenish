<?php
/**
 * Template Name: Contact Us
 */
get_header();
?>
<div id="inner-pages" class="fullwidth page-content">
    <?php get_template_part('includes/site', 'banner') ?>
    <div class="main-content single-section">
        <div class="container">
            <div id="page-section" class="page-wrap text-center"><?php /* Page main content section */ ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                           <?php the_content(); ?>
                    <?php endwhile; ?>
                <?php else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php
                endif;
                wp_reset_query();
                ?>
            </div>
        </div>
        <div id="below-contact-sec">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <h3>Get In Touch</h3>

                        <?php
                        echo do_shortcode('[contact-form-7 id="646" title="Contact Us Form"]');
                        $contact_form = get_field('add_form_shortcode');
                        /* echo do_shortcode($contact_form); */
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <h3>Address</h3>
                        <div class="contact-info">
                            <h4><?php the_field('address_title'); ?></h4>
                            <ul>
                                <li class="address-block">
                                    <img src="<?php bloginfo('template_url') ?>/img/location-golden-icon.png" alt="Location">
                                    <?php
                                    if (get_field('address_block')):
                                        the_field('address_block');
                                    endif;
                                    ?>
                                </li>
                                <li class="phone-block">
                                    <img src="<?php bloginfo('template_url') ?>/img/phone-golden-icon.png" alt="Phone">
                                    <?php
                                    if (get_field('main_phone_number_block')):
                                        the_field('main_phone_number_block');
                                    endif;
                                    ?>
                                </li>
                                <li class="fax-block">
                                    <img src="<?php bloginfo('template_url') ?>/img/fax-golden-icon.png" alt="Fax">
                                    <?php
                                    $phonestr = preg_replace('/[^a-zA-Z0-9\']/', '', get_field('add_fax_number'));
                                    $phonestr = str_replace("'", '', $phonestr);
                                    if (get_field('add_fax_number')):
                                        ?>
                                        <?php the_field('add_fax_number'); ?>  
                                    <?php endif;
                                    ?>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="attorneys-sec" class="single-section text-center white-text bg-cover">
        <div class="container">
            <div class="row">
                <?php the_field('attorneys_section_content', 387); ?>
                <?php
                if (have_rows('attorneys_section', 387)):
                    while (have_rows('attorneys_section', 387)) : the_row();
                        $postObj = get_sub_field('attorney_page_link', 387);
                        $pageLink = get_permalink($postObj);
                        ?>
                        <div class="single-attorney">
                            <?php $attor_img = get_sub_field('add_attorney_image', 387); ?>
                            <div class="attorney-img-block">
                                <a href="<?php echo $pageLink ?>">
                                    <img src="<?php echo $attor_img['url'] ?>" alt="<?php echo $attor_img['alt']; ?>"/>
                                </a>
                            </div>
                            <span class="attor-designation"><?php the_sub_field('add_attorney_designation', 387); ?></span>
                            <span class="attor-title"><a href="<?php echo $pageLink ?>"><?php the_sub_field('add_attorney_name', 387); ?></a></span>
                        </div>
                        <?php
                    endwhile;
                else :
                endif;
                ?>
            </div>
        </div>
    </div>
    <div id="map-sec" class="single-section text-center">
        <h2>Maps & Directions</h2>
        <?php the_field('add_map_iframe_code'); ?>
        <a target="_blank" href="https://www.google.com/maps/dir//39.7696979,-86.1602258/@39.769698,-86.160226,16z?hl=en-GB">Get Directions Here</a>
    </div>
</div>
<?php
get_footer();

