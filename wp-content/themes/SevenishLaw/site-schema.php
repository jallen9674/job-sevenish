<?php
/**
 *  File for Schema Markup of the site
 */
?>
<div class="clearfix"></div>
<?php
$desc = get_field('schema_review_content', get_the_id());
$star = get_field('star_rating', get_the_id());
$Reviewtitle = get_field('schema_review_title', get_the_id());
$reviewerName = get_field('reviewed_by', get_the_id());
if ($desc && $star && $reviewerName && $Reviewtitle) {
    ?>
    <div class="review-schema-wrapper single-section text-center">
        <div class="container">
            <h2>Client Review</h2>
            <div class="page_review" itemscope itemtype="http://schema.org/Review">
                <div class="page_review_by" itemprop="author">
                    <p><strong>Client Name:</strong> <span itemprop="name"><?php echo $reviewerName; ?></span></p>
                </div>
                <div class="page_review_by">
                    <p><strong>Review Title:</strong>
                        <span itemprop="itemReviewed" >
                            <?php echo $Reviewtitle; ?>
                        </span>
                    </p>
                </div>
                <div class="page_review_review">
                    <p><strong>Review Title:</strong>
                        <span itemprop="description">
                            <?php
                            echo $desc;
                            ?>
                        </span>
                    </p>
                </div>
                <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                    <?php $star = get_field('star_rating', get_the_id()); ?>
                    <p><strong>Star Rating:</strong>
                        <span>
                            <?php
                            if ($star == 1) {
                                echo"★";
                            } elseif ($star == 2) {
                                echo"★★";
                            } elseif ($star == 3) {
                                echo"★★★";
                            } elseif ($star == 4) {
                                echo"★★★★";
                            } elseif ($star == 5) {
                                echo"★★★★★";
                            } else {
                                
                            }
                            ?>
                        </span>
                    </p>
                    <div class="schema-hide">
                        <meta itemprop="worstRating" content="3">
                        <span itemprop="ratingValue"><?php echo $star; ?></span> / <span itemprop="bestRating">5</span> stars
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
$video_title = get_field('schema_video_title', get_the_id());
$video_thumbnail = get_field('page_video_thumbnail', get_the_id());
$video_desc = get_field('schema_video_description', get_the_id());
$video_uploaded = get_field('video_uploaded_date', get_the_id());
$youtube_video_id = get_field('youtube_video_id');
if ($video_title && $video_thumbnail && $video_desc && $video_uploaded) {
    ?>
    <div class="single-section video-schema-wrapper text-center" itemscope itemtype="http://schema.org/VideoObject" >
        <div class="container">
            <h2>Client Video</h2>
            <div class="videoWrapper videoWrapper169 js-videoWrapper">
                <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowtransparency="true" allowfullscreen="" data-src="https://www.youtube.com/embed/<?php echo $youtube_video_id; ?>?autoplay=1&amp; modestbranding=1&amp;rel=0&amp;hl=sv"></iframe>
                <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo $video_thumbnail; ?>);">Play video</button>
            </div>
            <img src="<?php echo $video_thumbnail; ?>" itemprop="thumbnailURL" class="hide" alt="John Fisher"/>
            <a class="hide" target="_blank" itemprop="embedURL" href="https://youtu.be/<?php echo get_field('youtube_video_id'); ?>">Watch the Video</a>
            <div class="video-schema-content">
                <p><b>Video Title:</b> <span itemprop="name"><?php echo $video_title; ?></span></p>
                <p><b>Upload Date:</b> <span itemprop="uploadDate"><?php echo $video_uploaded; ?></span></p>
                <p><b>Video Short Description:</b> <span itemprop="description"><?php echo $video_desc; ?></span>
            </div>
        </div>
    </div>
<?php }
?>